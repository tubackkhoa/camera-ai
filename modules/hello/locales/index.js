/* eslint-disable import/prefer-default-export */
// others but default language - en
import { messages as appMessages } from '../../../app/locales';
import en from './langs/en.json';
import vi from './langs/vi.json';

Object.assign(appMessages.en, en);
Object.assign(appMessages.vi, vi);

export const messages = {
  en,
  vi
};
