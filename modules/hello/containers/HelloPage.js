import React from 'react';

import styles from './HelloPage.css';

export default () => <h1 className={styles.red}>Hello</h1>;
