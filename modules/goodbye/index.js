import React from 'react';
import SettingsIcon from '@material-ui/icons/Settings';
import GoodbyePage from './containers/GoodbyePage';

export { messages } from './locales';

export const route = {
  key: 'location.goodbye',
  path: '/module/goodbye',
  component: GoodbyePage,
  icon: <SettingsIcon />
};

export default {
  name: 'Goodbye'
};
