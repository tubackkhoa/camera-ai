import React from 'react';

import styles from './GoodbyePage.css';

export default () => <h1 className={styles.green}>Goodbye</h1>;
