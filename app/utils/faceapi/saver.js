/* eslint-disable import/prefer-default-export */
import fs from 'fs';
import path from 'path';
import { get, post } from '../../api';

const getMediaroot = async () => {
  const ret = await get('/camera/get_save_folder');
  if (ret) return ret.path;
};

const saveImage = async (
  urlStream,
  slot,
  filePath,
  snapshot,
  faces,
  filename
) => {
  // save file so that detectImage will update meta info for it if not startsWith blob:
  const buffer = Buffer.from(await snapshot.arrayBuffer());
  // save as current millisecond for easy search
  fs.writeFileSync(filePath, buffer);
  await post('/camera/save_image_path', {
    urlStream,
    streamName: slot,
    imgPath: filename,
    faces: JSON.stringify(faces)
  });
};

export async function saveFacesResult(
  urlStream,
  slot,
  snapshot,
  facesResult,
  peopleCount
) {
  // not implemented or no result
  const empty =
    (!facesResult || !facesResult.length) &&
    (!peopleCount || !peopleCount.classes.filter(c => c === 0).length);

  if (process.env.TARGET === 'web' || empty) return;

  // get mediaroot realtime
  const mediaroot = await getMediaroot();

  if (!mediaroot) return;

  const slotPath = path.join(mediaroot, slot);
  if (!fs.existsSync(slotPath)) {
    fs.mkdirSync(slotPath);
  }

  const currentMillis = Date.now();
  const filename = `${currentMillis}.jpg`;
  const filePath = path.join(slotPath, filename);

  const metaPath = path.join(slotPath, `${currentMillis}.json`);
  // base on env
  const { promises } = await import('fs');
  try {
    await promises.writeFile(
      metaPath,
      JSON.stringify({ facesResult, peopleCount }, null, 2)
    );
  } catch (ex) {
    console.error(ex);
  }
  const faces = [];
  facesResult.forEach(item => {
    if (!faces.includes(item.label) && item.label !== 'unknown') {
      faces.push(item.label);
    }
  });
  await saveImage(urlStream, slot, filePath, snapshot, faces, filename);
  return `${slot}/${currentMillis}.jpg`;
}
