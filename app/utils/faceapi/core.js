/* eslint-disable no-restricted-globals */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
/* eslint-disable global-require */
/* eslint-disable no-return-await */

import * as tf from '@tensorflow/tfjs-core';
// import * as cocoSsd from '@tensorflow-models/coco-ssd';
import * as faceapi from './lib';

// import { setWasmPath } from '@tensorflow/tfjs-backend-wasm';
// import wasmPath from '../../node_modules/@tensorflow/tfjs-backend-wasm/dist/tfjs-backend-wasm.wasm';

export const getRect = (box, padding = 0.1) => {
  const paddingX = padding * box.width;
  const paddingY = padding * box.height;
  return {
    left: Math.max(0, Math.round(box.x - paddingX)),
    top: Math.max(0, Math.round(box.y - paddingY)),
    width: Math.round(box.width + 2 * paddingX),
    height: Math.round(box.height + 2 * paddingY)
  };
};

// can update this ?
let faceDetectorOptions;

const getRectAndSize = detection => {
  const padding = faceDetectorOptions._name === 'MtcnnOptions' ? 0.1 : 0.2;
  const rect = getRect(detection.box, padding);
  const { imageWidth, imageHeight } = detection;
  return {
    ...rect,
    imageWidth: Math.round(imageWidth),
    imageHeight: Math.round(imageHeight)
  };
};

export const isInitialized = () => modelInitialized;

export const detectImage = (input, blobURL) => {
  // wait until model is initialized
  // do no try await, just return empty

  if (!modelInitialized) return [];

  // // web assembly support only face detection
  // if (tf.getBackend() === 'wasm') {
  //   return faceapi.detectAllFaces(input, faceDetectorOptions).then(result =>
  //     result.map(detection => ({
  //       rect: getRectAndSize(detection),
  //       blobURL
  //     }))
  //   );
  // }
  // const start = Date.now();
  return (
    faceapi
      .detectAllFaces(input, faceDetectorOptions)
      // tiny model
      .withFaceLandmarks(faceDetectorOptions._name === 'MtcnnOptions')
      .withFaceDescriptors()
      .then(result =>
        result.map(({ detection, descriptor }) =>
          // may extract at main thread
          ({
            rect: getRectAndSize(detection),
            // descriptor,
            time: Date.now(),
            ...findLabel(descriptor),
            blobURL
          })
        )
      )
  );
};

export const getLabeledFaceDescriptors = async people => {
  const labeledDescriptors = await Promise.all(
    people.map(async (person, i) => {
      // later add only
      let descriptors =
        person.descriptors ||
        (await Promise.all(
          person.files
            .filter(file => file && file.path)
            .map(async file => {
              const input = new Image();
              input.src = file.path;
              const result = await faceapi
                .detectSingleFace(input)
                .withFaceLandmarks()
                .withFaceDescriptor();

              return result ? result.descriptor : null;
            })
        ));
      // filter null descriptor
      descriptors = descriptors.filter(descriptor => descriptor);
      return new faceapi.LabeledFaceDescriptors(
        person.label || `People #${i}`,
        descriptors
      );
    })
  );

  return labeledDescriptors;
};

export const updateFaceMatcher = labeledDescriptor => {
  // if delete then remove, else update or add
  const findIndex = faceMatcher.labeledDescriptors.findIndex(
    l => l.label === labeledDescriptor.label
  );

  if (findIndex > -1)
    faceMatcher.labeledDescriptors[findIndex] = labeledDescriptor;
  else faceMatcher.labeledDescriptors.push(labeledDescriptor);
};

export const sanitizeFaceMatcher = labels => {
  // console.log(labels);
  faceMatcher._labeledDescriptors = faceMatcher._labeledDescriptors.filter(l =>
    labels.includes(l.label)
  );
};

// patch it
const patchFaceApiWeb = () => {
  // patch
  faceapi.env.monkeyPatch({
    Canvas: HTMLCanvasElement,
    Image: HTMLImageElement,
    ImageData,
    Video: HTMLVideoElement,
    createCanvasElement: () => document.createElement('canvas'),
    createImageElement: () => document.createElement('img')
  });
};

let faceMatcher = null;
const findLabel = descriptor => {
  const option = { label: 'unknown' };
  if (faceMatcher) {
    const matched = faceMatcher.findBestMatch(descriptor);
    option.label = matched.label;
    option.confidence = Math.round(100 * (1 - matched.distance));
  }

  return option;
};

let modelInitialized = false;

const initFaceMatcher = async API_BASE => {
  // init faceMatcher the firstTime
  const labels = await fetch(`${API_BASE}/model/descriptors`).then(res =>
    res.json()
  );

  // get from server
  let labeledDescriptors;
  if (Array.isArray(labels)) {
    const people = await Promise.all(
      labels.map(async label => {
        const descriptors = [];

        // convert to descriptors from raw value
        const rawDescriptors = await fetch(
          `${API_BASE}/model/descriptor/${label}`
        ).then(res => res.arrayBuffer());
        if (rawDescriptors) {
          for (let i = 0; i < rawDescriptors.byteLength; i += 512) {
            descriptors.push(
              new Float32Array(rawDescriptors.slice(i, i + 512))
            );
          }
        }
        return { label, descriptors };
      })
    );

    labeledDescriptors = await getLabeledFaceDescriptors(people);
  } else {
    labeledDescriptors = [];
  }

  faceMatcher = new faceapi.FaceMatcher(labeledDescriptors);
};

let cocoModel;

export const predictObjects = input => {
  if (cocoModel) return cocoModel.detect(input);
};

// start running, this is relative to app
export const initModel = async ({ modelPath, patch = true, API_BASE }) => {
  if (modelInitialized) return;
  // check gpu support
  const { gpgpu } = tf.backend();
  if (gpgpu) {
    console.info('running on WebGL backend');
  } else {
    console.info('running on CPU backend, should try Web Assembly fallback');

    // setWasmPath('http://localhost/tfjs-backend-wasm.wasm');
    // await tf.setBackend('wasm');
  }

  if (patch) patchFaceApiWeb();

  // on Web mtcnn is faster and not causing lag, but on app ssdmobilenetv1 is faster
  faceDetectorOptions =
    process.env.TARGET === 'web' // && tf.getBackend() !== 'wasm'
      ? new faceapi.MtcnnOptions({
          minFaceSize: 50,
          // increase accuracy
          scaleFactor: 0.8,
          scoreThresholds: [0.8, 0.8, 0.9]
        })
      : new faceapi.SsdMobilenetv1Options({
          minConfidence: 0.8
        });

  const method = modelPath.startsWith('http') ? 'loadFromUri' : 'loadFromDisk';
  const models =
    faceDetectorOptions._name === 'MtcnnOptions'
      ? ['mtcnn', 'faceLandmark68TinyNet']
      : ['ssdMobilenetv1', 'faceLandmark68Net'];
  models.push('faceRecognitionNet');
  // await load all
  await Promise.all(
    models.map(model => faceapi.nets[model][method](modelPath))
  );

  // yolov3 = gpgpu // await yolo.v3tiny(`${modelPath}/yolo/v3tiny/model.json`);
  //   ? await yolo.v3(`${modelPath}/yolo/v3/model.json`)
  //   : await yolo.v3tiny(`${modelPath}/yolo/v3tiny/model.json`);

  // best model for person
  // const base = 'lite_mobilenet_v2';
  // const base =
  //   process.env.TARGET === 'web' ? 'lite_mobilenet_v2' : 'mobilenet_v2';
  // cocoModel = await cocoSsd.load({
  //   base,
  //   modelUrl: `${modelPath}/${base}/model.json`
  // });

  // cross platform
  await initFaceMatcher(API_BASE);

  modelInitialized = true;

  return faceapi;
};

export default faceapi;
