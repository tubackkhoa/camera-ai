/* eslint-disable no-param-reassign */
/* eslint-disable import/prefer-default-export */
/* eslint-disable global-require */

import command from '../electron-command';
import EventEmitter from '../../components/FLV/events';

// single thread, wrap this
export const emitter = new EventEmitter();

export const assignTaskHandler = task => {
  emitter.addListener(`assign-task.${task}`, (canvas, blobURL, slot) => {
    // save this image
    command.run(task, blobURL, slot);
  });

  command.on(task, (e, ret, slot, args) => {
    let doneEvent = `done-task.${task}`;
    // no slot then just fire all
    if (slot) doneEvent += `.${slot}`;
    emitter.emit(doneEvent, ret, slot, args);
  });
};

assignTaskHandler('detectImage');
assignTaskHandler('predictObjects');
