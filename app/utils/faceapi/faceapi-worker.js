/* eslint-disable func-names */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-const-assign */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-return-assign */
/* eslint-disable no-global-assign */
/* eslint-disable no-undef */
/* eslint-disable no-param-reassign */
/* eslint-disable no-case-declarations */
/* eslint-disable compat/compat */
/* eslint-disable global-require */
/* eslint-disable no-return-await */
import faceapi, {
  initModel,
  isInitialized,
  detectImage,
  predictObjects
} from './core';

const createInput = (w, h, buf) => {
  const input = new OffscreenCanvas(w, h);
  const context = input.getContext('2d');
  const imageData = context.createImageData(w, h);
  imageData.data.set(new Uint8ClampedArray(buf));
  context.putImageData(imageData, 0, 0);
  return input;
};

export default function FaceApiWorker(self) {
  self.Canvas = self.HTMLCanvasElement = self.OffscreenCanvas;
  self.CanvasRenderingContext2D = self.OffscreenCanvasRenderingContext2D;
  function HTMLImageElement() {}
  function HTMLVideoElement() {}
  self.Image = HTMLImageElement;
  self.Video = HTMLVideoElement;
  function Storage() {
    let _data = {};
    this.clear = function() {
      return (_data = {});
    };
    this.getItem = function(id) {
      return _data.hasOwnProperty(id) ? _data[id] : undefined;
    };
    this.removeItem = function(id) {
      return delete _data[id];
    };
    this.setItem = function(id, val) {
      return (_data[id] = String(val));
    };
  }
  class Document extends EventTarget {}
  const window = self;
  const document = new Document();
  self.localStorage = new Storage();
  // console.log('*faked* Window object for the worker', window);
  // console.log('*faked* Document object for the worker', document);
  function createElement(element) {
    // console.log('FAKE ELELEMT instance', createElement, 'type', createElement, '(', createElement.arguments, ')');
    switch (element) {
      case 'canvas':
        // console.log('creating canvas');
        const canvas = new Canvas(1, 1);
        canvas.localName = 'canvas';
        canvas.nodeName = 'CANVAS';
        canvas.tagName = 'CANVAS';
        canvas.nodeType = 1;
        canvas.innerHTML = '';
        canvas.remove = () => {
          console.log('nope');
        };
        // console.log('returning canvas', canvas);
        return canvas;
      default:
        console.log('arg', element);
        break;
    }
  }
  document.createElement = createElement;
  document.location = self.location;
  // console.log('*faked* Document object for the worker', document);
  if (typeof window !== 'object') {
    console.warn('Check failed: window');
  }
  if (typeof document === 'undefined') {
    console.warn('Check failed: document');
  }
  if (typeof HTMLImageElement === 'undefined') {
    console.warn('Check failed: HTMLImageElement');
  }
  if (typeof HTMLCanvasElement === 'undefined') {
    console.warn('Check failed: HTMLCanvasElement');
  }
  if (typeof HTMLVideoElement === 'undefined') {
    console.warn('Check failed: HTMLVideoElement');
  }
  if (typeof ImageData === 'undefined') {
    console.warn('Check failed: ImageData');
  }
  if (typeof CanvasRenderingContext2D === 'undefined') {
    console.warn('Check failed: CanvasRenderingContext2D');
  }
  self.window = window;
  self.document = document;
  self.HTMLImageElement = HTMLImageElement;
  self.HTMLVideoElement = HTMLVideoElement;
  const isBrowserCheck =
    typeof window === 'object' &&
    typeof document !== 'undefined' &&
    typeof HTMLImageElement !== 'undefined' &&
    typeof HTMLCanvasElement !== 'undefined' &&
    typeof HTMLVideoElement !== 'undefined' &&
    typeof ImageData !== 'undefined' &&
    typeof CanvasRenderingContext2D !== 'undefined';
  // console.warn("++ Check passed locally:", isBrowserCheck);
  if (!isBrowserCheck) {
    throw new Error('Failed to monkey patch for face-api, face-api will fail');
  }
  faceapi.env.monkeyPatch({
    createCanvasElement: () => {
      // default canvas from document.createElement('canvas')
      return new OffscreenCanvas(300, 150);
    }
  });

  // const canvas = new OffscreenCanvas(w, h);
  self.addEventListener('message', async ({ data: { cmd, slot, args } }) => {
    switch (cmd) {
      case 'initModel':
        initModel(args);
        break;
      case 'detectImage':
      case 'predictObjects':
        if (!isInitialized()) return;
        // alredy patched
        const [w, h, buf, blobURL] = args;
        const input = createInput(w, h, buf);
        const ret =
          cmd === 'detectImage'
            ? await detectImage(input, blobURL)
            : await predictObjects(input);
        if (ret.length) {
          // console.log(Date.now() - start, 'ms', ret);
          self.postMessage({ cmd, slot, args: ret });
        }

        break;
      default:
        break;
    }
  });
}
