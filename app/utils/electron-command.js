import { ipcRenderer, remote } from 'electron';

const bgWindowRenderer = remote.getGlobal('bgWindow').webContents;
window.bgWindowRenderer = bgWindowRenderer;
export default {
  // first arg is assign-task
  run: bgWindowRenderer.send.bind(bgWindowRenderer, 'assign-task'),
  on(task, callback) {
    return ipcRenderer.on(`done-task.${task}`, callback);
  },
  off(task) {
    return ipcRenderer.removeAllListeners(`done-task.${task}`);
  }
};
