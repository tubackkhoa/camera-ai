/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import People from '../components/People';

type Props = {};

function mapStateToProps(state) {
  return {
    aiServer: state.settings.aiServer
  };
}

@connect(mapStateToProps)
export default class PeoplePage extends Component<Props> {
  props: Props;

  render() {
    return <People {...this.props} />;
  }
}
