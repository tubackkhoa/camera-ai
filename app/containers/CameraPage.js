/* eslint-disable no-alert */
/* eslint-disable prefer-const */
/* eslint-disable no-param-reassign */
import { connect } from 'react-redux';
import React, { Component } from 'react';
import Camera from '../components/Camera';

import * as devicesActions from '../actions/devices';
import * as settingActions from '../actions/settings';
import type { DevicesActions, SettingsActions } from '../actions/types';
import type { DeviceStates, SettingState } from '../reducers/types';
import { StringURL } from '../utils/common';
import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';
import { get, post } from '../api';

function mapStateToProps(state) {
  return {
    settings: state.settings,
    devices: state.devices
  };
}

export type CameraPageProps = {
  settings: SettingState,
  devices: DeviceStates
} & DevicesActions &
  SettingsActions;

@connect(mapStateToProps, { ...devicesActions, ...settingActions })
export default class CameraPage extends Component<CameraPageProps> {
  processNewDevice = async newDevice => {
    let { url, user, pass, tcp, isIPCam } = newDevice;

    // preprocessing url
    const urlObj = new StringURL(url);
    url = `${urlObj.origin}${urlObj.pathname}`;
    if (!user) user = urlObj.username;
    if (!pass) pass = urlObj.password;

    // update url,user,pass
    Object.assign(newDevice, { url, user, pass });

    const ret = await post('/onvif/device_connect', {
      url,
      user,
      pass,
      tcp,
      isIPCam
    });
    // }
    if (ret.error) {
      // replace all alert with show error ui later
      alert(ret.error);
    } else {
      Object.assign(newDevice, ret);
    }
    // console.log(ret);
    // stream will never has user,pass with basic because it is unsecure, it might be digest
    if (ret.stream) {
      newDevice.stream = ret.stream;
      const streamURL = await this.addServerDevice(url, SUB_STREAM);
      newDevice.flvStream = streamURL;
    } else {
      alert('Can not get stream');
    }

    return newDevice;
  };

  addServerDevice = async (url, stream) => {
    const { streamURL } = await post('/camera/add', {
      url,
      stream
    });
    return streamURL;
  };

  handleDeviceAdd = async newDevice => {
    const { deviceAdd } = this.props;
    newDevice = await this.processNewDevice(newDevice);
    deviceAdd(newDevice);
  };

  handleDeviceUpdate = async (newDevice, oldDevice) => {
    const { deviceUpdate } = this.props;

    newDevice = await this.processNewDevice(newDevice);

    const {
      url,
      // user,
      // pass,
      mainStream,
      // stream,
      record,
      saveAuto,
      startRecordTime,
      scheduler,
      schedulerType
    } = newDevice;
    const streamToRecord = mainStream ? MAIN_STREAM : SUB_STREAM;
    // always record main stream
    if (record !== oldDevice.record && streamToRecord) {
      const streamURL = await this.addServerDevice(url, streamToRecord);

      // update main flv stream
      newDevice.mainFlvStream = streamURL;

      // try to record
      console.log(`save record. auto save ${saveAuto}`);
      console.log(`start time ${startRecordTime}`);
      post('/camera/record', {
        url,
        streamType: streamToRecord,
        save: record,
        saveAuto,
        saveDate: startRecordTime,
        scheduler,
        duration: schedulerType
      });
    }

    // delete old stream when update
    if (url !== oldDevice.url) {
      // disconnect and delete from database
      console.log(oldDevice, newDevice);
      post('/onvif/device_disconnect', {
        url: oldDevice.url
      });
      post('/camera/deleteDevice', oldDevice);
    }

    deviceUpdate(newDevice, oldDevice);
  };

  handleDeviceDelete = async oldDevice => {
    const { deviceDelete } = this.props;

    deviceDelete(oldDevice);

    // disconnect the device
    post('/onvif/device_disconnect', {
      url: oldDevice.url
    });
  };

  render() {
    const { devices, settings, setItem } = this.props;

    return (
      <Camera
        settings={settings}
        devices={devices}
        setItem={setItem}
        handleDeviceAdd={this.handleDeviceAdd}
        handleDeviceUpdate={this.handleDeviceUpdate}
        handleDeviceDelete={this.handleDeviceDelete}
      />
    );
  }
}
