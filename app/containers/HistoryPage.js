/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import formValueSelector from 'redux-form/es/formValueSelector';

import History from '../components/History';

const selector = formValueSelector('People'); // <-- same as form name

function mapStateToProps(state) {
  return {
    devices: state.devices,
    people: selector(state, 'people'),
    aiServer: state.settings.aiServer,
    confidence: state.settings.confidence || 30
  };
}

type Props = {};

@connect(mapStateToProps)
export default class HistoryPage extends Component<Props> {
  props: Props;

  render() {
    return <History {...this.props} />;
  }
}
