import React, { Component } from 'react';
import { connect } from 'react-redux';
import Module from '../components/Module';
import * as SettingActions from '../actions/settings';

function mapStateToProps(state) {
  return {
    modules: state.settings.modules
  };
}

type Props = {};

@connect(mapStateToProps, SettingActions)
export default class ModulePage extends Component<Props> {
  props: Props;

  handleSaveModules = modules => {
    const { setItem } = this.props;
    setItem('modules', modules);
  };

  render() {
    const { modules } = this.props;
    return <Module modules={modules} saveModules={this.handleSaveModules} />;
  }
}
