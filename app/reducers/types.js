import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';
import { initialState as DefaultSettingState } from './settings';

export type Action = {
  +type: string
};

export type GetState = () => counterStateType;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;

export type SettingState = typeof DefaultSettingState;

// any value? detectable: ?boolean
export type DeviceState = {
  url: string,
  name: string,
  detectable?: boolean,
  user?: string,
  pass?: string,
  // [key: $Values<typeof detailFields>]: string,
  tableData?: {
    id: number,
    checked: boolean
  }
};

export type DeviceStates = Array<DeviceState>;
