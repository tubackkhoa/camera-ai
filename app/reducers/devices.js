/* eslint-disable no-case-declarations */
import {
  DEVICE_ADD,
  DEVICE_UPDATE,
  DEVICE_DELETE,
  DEVICE_REORDER
} from '../actions/devices';

export const initialState = [];

export default function(state = initialState, action) {
  let devices = state;
  if (!Array.isArray(state)) return initialState;
  switch (action.type) {
    case DEVICE_ADD:
      // url must be unique
      if (state.findIndex(device => device.url === action.payload.url) === -1) {
        devices = [action.payload, ...state];
      }

      return devices;

    case DEVICE_UPDATE:
      const [newDevice, oldDevice] = action.payload;
      if (oldDevice) {
        devices = [...state];
        devices[devices.indexOf(oldDevice)] = newDevice;
        return devices;
      }
      return state;

    case DEVICE_DELETE:
      devices = [...state];
      devices.splice(devices.indexOf(action.payload), 1);
      return devices;

    case DEVICE_REORDER:
      const { startIndex, endIndex } = action.payload;
      devices = [...state];
      const [removed] = devices.splice(startIndex, 1);
      devices.splice(endIndex, 0, removed);
      return devices;

    default:
      return devices;
  }
}
