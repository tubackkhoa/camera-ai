/* eslint-disable react/prop-types */
/* eslint-disable no-unused-expressions */
import React, { Component, forwardRef } from 'react';
import { connect } from 'react-redux';
import formValueSelector from 'redux-form/es/formValueSelector';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { change } from 'redux-form';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { injectIntl, FormattedMessage } from 'react-intl';
import Skeleton from '@material-ui/lab/Skeleton';
import { bindActionCreators } from 'redux';
import { fetchWithOptions } from '../../api';

import { CanvasWithPeople } from '../common';
import styles from './Preview.css';

type Props = {
  people: any,
  change: any,
  aiServer: string,
  intl: any
};

const selector = formValueSelector('People'); // <-- same as form name
// const { formatMessage } = useIntl();

function mapStateToProps(state) {
  return {
    aiServer: state.settings.aiServer,

    devices: state.devices,
    locale: state.settings.locale,
    people: selector(state, 'people')
  };
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ change }, dispatch);
};
const Transition = React.forwardRef((props, ref) => (
  <Slide direction="up" ref={ref} {...props} />
));

@injectIntl
@connect(mapStateToProps, mapDispatchToProps, null)
export default class PreviewDialog extends Component {
  props: Props;

  constructor(props) {
    super(props);
    props.onItemRef && props.onItemRef(this);
    this.state = { item: null, peopleName: '', loading: false, msg: null };
  }

  handleClose = () => {
    this.setState({ item: null });
  };

  show = item => {
    this.setState({ item });
  };

  handleChangePeople = val => {
    this.setState({ peopleName: val });
  };

  setMessage = msg => {
    this.setState({ msg });
  };

  changePeopleForm = () => {
    const { people, change } = this.props;

    const { peopleName, item } = this.state;
    const peopleCopy = people;
    const peopleOptions =
      people && Array.isArray(people)
        ? people.filter(person => person).map(person => person.label)
        : [];
    const index = peopleOptions.findIndex(peopleName);
    if (index !== -1) {
      const person = people[index];
      person.files.push({ name: 'unknown', path: item.blobURL });
      peopleCopy[index] = person;
      change('People', 'people', peopleCopy);
    } else {
      const person = {
        label: peopleName,
        files: [{ name: 'unknown', path: item.blobURL }]
      };
      peopleCopy.push(person);
      change('People', 'people', peopleCopy);
    }
  };

  onUpdatePeople = async () => {
    const { peopleName, item } = this.state;
    const { aiServer } = this.props;

    const formData = new FormData();
    formData.append('name', peopleName);
    formData.append('file', item.blobURL);
    // const snapshots = await Promise.all(
    //   person.files
    //     .filter(file => file)
    //     .map(file => fetch(file.path).then(r => r.blob()))
    // );

    await fetchWithOptions(`${aiServer}/api/update_face`, {
      method: 'POST',
      body: formData
    });

    this.setState({ loading: false }, () => this.changePeopleForm());
    this.setMessage({
      children: `Cập nhật thành công: ${peopleName}`,
      severity: 'success'
    });
  };

  renderUpdatePeople = () => {
    const { item } = this.state;
    const { people, intl } = this.props;
    const { formatMessage } = intl;
    const defaultValue = item && item.id ? [item.id] : [];
    if (item && item.label && item.label === 'unknown') {
      const peopleOptions =
        people && Array.isArray(people)
          ? people.filter(person => person).map(person => person.label)
          : [];
      if (item && item.id) {
        peopleOptions.push(item.id);
      }

      return (
        <Box my={2}>
          <Autocomplete
            onChange={(e, val) => {
              this.handleChangePeople(val);
            }}
            defaultValue={defaultValue}
            options={peopleOptions}
            renderInput={params => {
              return (
                <TextField
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  {...params}
                  variant="standard"
                  placeholder={formatMessage({ id: 'app.choosePeople' })}
                  fullWidth
                />
              );
            }}
          />
          <Button onClick={this.onUpdatePeople}>
            <FormattedMessage id="app.updateModel" />
          </Button>
        </Box>
      );
    }
    return null;
  };

  render() {
    const { item, loading, msg } = this.state;
    return (
      <Dialog
        scroll="paper"
        fullScreen
        open={!!item}
        onClose={this.handleClose}
        TransitionComponent={Transition}
      >
        <IconButton
          edge="start"
          classes={{ root: styles.closeButton }}
          onClick={this.handleClose}
          aria-label="close"
        >
          <CloseIcon />
        </IconButton>
        {item && (
          <CanvasWithPeople
            src={item.blobURL}
            people={[item]}
            className={styles.dialogImage}
          />
        )}
        {/* {this.renderUpdatePeople()}
        {loading && <Skeleton animation="wave" width="100%" />}
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          open={!!msg}
          onClose={() => this.setMessage(null)}
          autoHideDuration={3000}
        >
          <Alert {...msg} />
        </Snackbar> */}
      </Dialog>
    );
  }
}
