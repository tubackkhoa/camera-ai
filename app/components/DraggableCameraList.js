/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import StreamPlayer from './StreamPlayer';
import DraggableList from './DraggableList';
import styles from './DraggableCameraList.css';

const getKey = device => device.url;

const renderContainer = (props, innerRef) => (
  <Grid
    container
    wrap="nowrap"
    ref={innerRef}
    alignContent="flex-start"
    {...props}
  />
);

const renderItem = (
  { item, xs, sm, zoom, videoRatio, onReg, aiServer, ...props },
  innerRef
) => (
  <Grid
    item
    xs={xs}
    sm={sm}
    className={styles.dragItem}
    ref={innerRef}
    {...props}
  >
    <StreamPlayer
      zoom={zoom}
      onReg={onReg}
      videoRatio={videoRatio}
      aiServer={aiServer}
      {...item}
    />
    {/* {item.name} */}
  </Grid>
);

// item number => zoom
const zoomLevel = { 1: 1, 2: 1, 3: 0.8, 4: 0.7, 6: 0.65 };
export default function DraggableCameraList({
  devices,
  onDragEnd,
  items,
  ...props
}) {
  const zoom = zoomLevel[items];
  const sm = Math.round(12 / items);
  // on mobile 2 columns
  const xs = 6;

  // only re-render if change maxItems
  return useCallback(
    <DraggableList
      items={devices}
      direction="horizontal"
      getKey={getKey}
      maxItems={items}
      renderContainer={renderContainer}
      itemProps={{ xs, sm, zoom, ...props }}
      renderItem={renderItem}
      onDragEnd={onDragEnd}
    />,
    [items]
  );
}
