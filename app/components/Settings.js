/* eslint-disable promise/always-return */
/* eslint-disable promise/catch-or-return */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Select from '@material-ui/core/Select';
import { FormattedMessage, injectIntl } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import debounce from 'lodash/debounce';
import { remote } from 'electron';

import { PasswordField } from './common';
import { locales } from '../locales';
import { post, get } from '../api';

const ControlledExpansionPanels = ({ title, children, spacing = 3 }) => {
  const [expanded, setExpanded] = React.useState(true);

  const handleChange = (event, isExpanded) => {
    setExpanded(isExpanded);
  };

  return (
    <ExpansionPanel expanded={expanded} onChange={handleChange}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography variant="h6" color="textPrimary">
          {title}
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Grid container spacing={spacing}>
          {children}
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

@injectIntl
class Settings extends React.Component {
  static defaultProps = {
    themes: ['dark', 'light'],
    camGrids: ['2', '3', '4', '6'],
    fpss: ['12', '14', '24', '28', '32', '40'],
    videoLengths: ['1', '15', '30', '60', '120']
  };

  constructor(props) {
    super(props);
    this.state = { dataPath: '' };
  }

  async componentDidMount() {
    const { path } = await get('/camera/get_save_folder');
    this.setState({ dataPath: path });
  }

  handleChangeLocale = event => {
    // console.log(event.target.value);
    this.props.changeLocale(event.target.value);
  };

  handleChangeTheme = event => {
    // console.log(event.target.value);
    this.props.changeTheme(event.target.value);
  };

  handleChangeVideoLength = event => {
    this.props.setItem('videoLength', +event.target.value);
    post('/camera/update_video_length', {
      videoLength: event.target.value
    });
  };

  handleChangeFps = event => {
    this.props.setItem('fps', +event.target.value);
    post('/camera/update_stream', {
      fps: event.target.value
    });
  };

  handleChangeMotionOnly = event => {
    this.props.setItem('vfSave', event.target.checked);
    console.log(`vfsave ${event.target.checked}`);
    post('/camera/update_only_motion', {
      vfSave: event.target.checked
    });
  };

  openSelectFolderDialog = async () => {
    const options = { properties: ['openDirectory'] };
    const folderSelected = await remote.dialog.showOpenDialog(options);
    if (folderSelected.filePaths && folderSelected.filePaths.length > 0) {
      console.log(folderSelected.filePaths);
      this.setState({ dataPath: folderSelected.filePaths });
      post('/camera/change_save_dir', {
        newPath: folderSelected.filePaths
      });
    }
  };

  debouncedUpdate = debounce((name, val) => this.props.setItem(name, val), 500);

  handledUpdate = (name, isNumber = false) => e => {
    e.persist();
    let val = e.target.value;
    if (isNumber) val = +val;
    this.debouncedUpdate(name, val);
  };

  render() {
    const {
      settings,
      themes,
      camGrids,
      videoLengths,
      fpss,
      setItem,
      intl
    } = this.props;
    const { dataPath } = this.state;

    return (
      <>
        <ControlledExpansionPanels
          spacing={6}
          title={intl.formatMessage({ id: 'setting.general' })}
        >
          <Grid item sm={3} xs={6}>
            <Select
              value={settings.locale}
              onChange={this.handleChangeLocale}
              fullWidth
            >
              {Object.keys(locales).map(key => (
                <MenuItem key={key} value={key}>
                  {locales[key]}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>
              <FormattedMessage id="setting.changeLanguage" />
            </FormHelperText>
          </Grid>
          <Grid item sm={3} xs={6}>
            <Select
              value={settings.theme}
              onChange={this.handleChangeTheme}
              fullWidth
            >
              {themes.map(key => (
                <MenuItem key={key} value={key}>
                  {key}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>
              <FormattedMessage id="setting.changeTheme" />
            </FormHelperText>
          </Grid>

          <Grid item sm={3} xs={6}>
            <TextField
              fullWidth
              disabled
              value={dataPath}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={this.openSelectFolderDialog}>
                      <FolderIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
          </Grid>
          <Grid item sm={3} xs={6}>
            <TextField
              defaultValue={settings.videoRatio}
              onChange={this.handledUpdate('videoRatio', true)}
              type="number"
            />
            <FormHelperText>
              <FormattedMessage id="setting.videoRatio" />
            </FormHelperText>
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              fullWidth
              defaultValue={settings.mailAddress}
              placeholder={intl.formatMessage({ id: 'setting.mailAddress' })}
              onChange={this.handledUpdate('mailAddress')}
            />
            <PasswordField
              fullWidth
              defaultValue={settings.mailPass}
              placeholder={intl.formatMessage({ id: 'setting.mailPass' })}
              onChange={this.handledUpdate('mailPass')}
            />
            <FormHelperText>
              <FormattedMessage id="setting.mailAccount" />
            </FormHelperText>
          </Grid>

          <Grid item sm={3} xs={6}>
            <TextField
              fullWidth
              defaultValue={settings.aiServer}
              onChange={this.handledUpdate('aiServer')}
            />
            <FormHelperText>AI Server URL</FormHelperText>
          </Grid>
          <Grid item sm={3} xs={6}>
            <TextField
              fullWidth
              defaultValue={settings.confidence}
              onChange={this.handledUpdate('confidence')}
            />
            <FormHelperText>Confidence ({settings.confidence}%)</FormHelperText>
          </Grid>
        </ControlledExpansionPanels>
        <ControlledExpansionPanels
          spacing={6}
          title={intl.formatMessage({ id: 'setting.cam' })}
        >
          <Grid item sm={3} xs={6}>
            <Select
              value={settings.items}
              onChange={e => setItem('items', +e.target.value)}
              fullWidth
            >
              {camGrids.map(key => (
                <MenuItem key={key} value={key}>
                  {key}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>
              <FormattedMessage id="setting.camGrids" />
            </FormHelperText>
          </Grid>
          <Grid item sm={3} xs={6}>
            <Select
              value={settings.videoLength}
              onChange={this.handleChangeVideoLength}
              fullWidth
            >
              {videoLengths.map(key => (
                <MenuItem key={key} value={key}>
                  {key}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>
              <FormattedMessage id="setting.videoLength" />
            </FormHelperText>
          </Grid>
          <Grid item sm={3} xs={6}>
            <Select
              value={settings.fps}
              onChange={this.handleChangeFps}
              fullWidth
            >
              {fpss.map(key => (
                <MenuItem key={key} value={key}>
                  {key}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>
              <FormattedMessage id="setting.fps" />
            </FormHelperText>
          </Grid>
          <Grid item sm={3} xs={6}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={settings.vfSave}
                  disableRipple
                  onChange={this.handleChangeMotionOnly}
                />
              }
              label={intl.formatMessage({ id: 'setting.motion' })}
            />
          </Grid>
        </ControlledExpansionPanels>
      </>
    );
  }
}

export default Settings;
