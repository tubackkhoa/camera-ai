/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useRef, useEffect, forwardRef } from 'react';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import { FormattedMessage } from 'react-intl';
import Skeleton from '@material-ui/lab/Skeleton';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import VideocamIcon from '@material-ui/icons/Videocam';
import Avatar from '@material-ui/core/Avatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { DateTimePicker } from '@material-ui/pickers';
import List from '@material-ui/core/List';
import makeStyles from '@material-ui/core/styles/makeStyles';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import CheckIcon from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import ClearIcon from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import ViewColumn from '@material-ui/icons/ViewColumn';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import dayjs from 'dayjs';
// import WebSocket from './WebSocket';
import { post } from '../api';
import { format } from '../constants/date.json';

export const tableIcon = Component =>
  forwardRef((props, ref) => <Component {...props} ref={ref} />);

export const tableIcons = {
  Add: tableIcon(AddBoxIcon),
  Check: tableIcon(CheckIcon),
  Clear: tableIcon(ClearIcon),
  Delete: tableIcon(DeleteOutline),
  DetailPanel: tableIcon(ChevronRight),
  Edit: tableIcon(Edit),
  Export: tableIcon(SaveAlt),
  Filter: tableIcon(FilterList),
  FirstPage: tableIcon(FirstPage),
  LastPage: tableIcon(LastPage),
  NextPage: tableIcon(ChevronRight),
  PreviousPage: tableIcon(ChevronLeft),
  ResetSearch: tableIcon(ClearIcon),
  Search: tableIcon(SearchIcon),
  SortArrow: tableIcon(ArrowDownward),
  ThirdStateCheck: tableIcon(Remove),
  ViewColumn: tableIcon(ViewColumn)
};

export const useDidUpdate = (callback, deps) => {
  const hasMount = useRef(false);

  useEffect(() => {
    if (hasMount.current) {
      callback();
    } else {
      hasMount.current = true;
    }
  }, deps);
};

export const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 225
  }
}));

export const LabelCheckbox = ({ value, onChange, label }) => {
  const classes = useStyles();

  return (
    <FormControlLabel
      className={classes.formControl}
      label={label}
      labelPlacement="top"
      control={
        <Checkbox
          edge="start"
          checked={!!value}
          tabIndex={-1}
          disableRipple
          onChange={e => onChange(e.target.checked)}
        />
      }
    />
  );
};

export const LabelCheckboxWithKey = ({ value, onChange, field, label }) => {
  return (
    <LabelCheckbox
      label={label}
      value={!!value[field]}
      onChange={checkValue => onChange(value, field, checkValue)}
    />
  );
};

export const MotionToggleButton = ({ device, onChange }) => {
  const handleClick = () => {
    const newDevice = { ...device, recordMotion: !device.recordMotion };
    onChange(newDevice, device);
  };

  return (
    <Checkbox
      edge="start"
      checked={!!device.recordMotion}
      tabIndex={-1}
      disableRipple
      onClick={handleClick}
    />
  );
};

export const DateAndTimePickers = ({ field, label, value, onChange }) => {
  const classes = useStyles();

  const dateSelect = dateValue => {
    // console.log(dateValue);
    if (dateValue) {
      onChange(value, field, dateValue.format(format));
    }
  };

  return (
    <FormControl className={classes.formControl}>
      <DateTimePicker
        ampm={false}
        label={label}
        className={classes.textField}
        value={value[field] || dayjs().format(format)}
        onChange={dateSelect}
        renderInput={props => (
          <TextField size="small" variant="standard" {...props} />
        )}
        format={format}
      />
    </FormControl>
  );
};

export const SchedulerType = ({ schedulerTypes, device, onChange }) => {
  const handleSchedulerType = event => {
    // let newDevice = device;
    // newDevice.schedulerType = event.target.value;
    onChange(device, 'schedulerType', event.target.value);
  };
  const classes = useStyles();

  return (
    <FormControl className={classes.formControl}>
      <FormHelperText>
        <FormattedMessage id="setting.recordDuration" />
      </FormHelperText>
      <Select
        label="Duration"
        value={device.schedulerType ? device.schedulerType : '1'}
        onChange={handleSchedulerType}
        autoWidth
      >
        {schedulerTypes.map(key => (
          <MenuItem key={key} value={key}>
            {key}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export const PasswordField = props => {
  const [showPassword, setShow] = useState(false);
  return (
    <TextField
      {...props}
      type={showPassword ? 'text' : 'password'}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              onMouseUp={() => setShow(false)}
              onMouseDown={() => setShow(true)}
            >
              {showPassword ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        )
      }}
    />
  );
};

export const InputChangeSaveDir = ({ device, onChange }) => {
  // const [selected, setSelected] = React.useState(false);

  const handleOnChange = event => {
    if (event.target.files[0]) {
      const newDevice = { ...device, newFolder: event.target.files[0].path };
      onChange(newDevice, device);
    }
  };

  return (
    <Input
      // folderRef={folderRef}
      type="file"
      webkitdirectory="true"
      directory="true"
      msdirectory="true"
      mozdirectory="true"
      odirectory="true"
      multiple
      onChange={handleOnChange}
    />
  );
};

// scan with ip mask support
export const ScanCamForm = ({ onDeviceAdd }) => {
  const [loading, setLoading] = useState(false);
  const [scanDevices, setDevices] = useState([]);
  const inputRef = useRef();

  const scanCam = async () => {
    const [target, port = '80'] = inputRef.current.value.trim().split(/\s+/);

    setLoading(true);

    const newScanDevices = await post('/onvif/device_discover', {
      target,
      port
    });
    // may be abort, when leave component, ref.current will be null after async
    if (!inputRef.current) return;
    if (newScanDevices && Array.isArray(newScanDevices)) {
      // append old device before
      newScanDevices.push(
        ...scanDevices.filter(
          oldDevice =>
            newScanDevices.findIndex(device => device.url === oldDevice.url) ===
            -1
        )
      );

      setDevices(newScanDevices);
    }

    // done
    setLoading(false);
  };

  return (
    <>
      <Input
        fullWidth
        inputRef={inputRef}
        placeholder="IP and Port seperate by space"
        endAdornment={
          <InputAdornment position="end">
            <IconButton onClick={scanCam} aria-label="Quét camera">
              <SearchIcon />
            </IconButton>
          </InputAdornment>
        }
      />

      {loading && (
        <Box>
          <Skeleton variant="text" />
          <Skeleton variant="circle" width={40} height={40} />
          <Skeleton variant="rect" height={118} />
        </Box>
      )}

      <List dense>
        {scanDevices.map(device => (
          <ListItem key={device.url}>
            <ListItemAvatar>
              <Avatar>
                <VideocamIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={device.name}
              secondary={
                <Typography
                  color="textSecondary"
                  variant="caption"
                  component="div"
                  noWrap
                >
                  {device.url}
                </Typography>
              }
            />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="add"
                onClick={() => onDeviceAdd(device)}
              >
                <AddIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </>
  );
};

export class PeopleCount extends React.Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };
  }

  render() {
    const { count } = this.state;
    if (count === undefined) return null;
    const { variant, ...props } = this.props;
    return (
      <Box position="absolute" {...props}>
        <Chip
          icon={<FaceIcon />}
          label={count}
          size="small"
          variant={variant}
          color={count ? 'primary' : 'default'}
        />
      </Box>
    );
  }
}

export const CanvasWithPeople = ({
  people,
  minConfidence = 0,
  src,
  ctxConfig = {
    lineWidth: '2',
    strokeStyle: 'red',
    font: '20px Comic Sans MS',
    fillStyle: 'red',
    textAlign: 'center'
  },
  ...props
}) => {
  const canvasRef = useRef();
  useEffect(() => {
    const img = new Image();
    img.onload = () => {
      const canvas = canvasRef.current;
      if (canvas) {
        canvas.width = img.naturalWidth;
        canvas.height = img.naturalHeight;
        const ctx = canvas.getContext('2d');
        Object.assign(ctx, ctxConfig);
        ctx.drawImage(img, 0, 0);
        ctx.beginPath();
        if (Array.isArray(people))
          people.forEach(({ rect, label, confidence }) => {
            ctx.rect(rect.left, rect.top, rect.width, rect.height);

            const text =
              confidence > minConfidence
                ? `${label} (${confidence})%`
                : 'unknown';
            if (!label.match(/unknown/i)) {
              ctx.fillText(text, rect.left + rect.width / 2, rect.top + 20);
            }
          });
        ctx.stroke();
      }
    };
    img.src = src;
  }, [people]);

  return <canvas ref={canvasRef} {...props} />;
};
