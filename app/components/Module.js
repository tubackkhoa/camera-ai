import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Link from '@material-ui/core/Link';
import Switch from '@material-ui/core/Switch';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AttachmentIcon from '@material-ui/icons/Attachment';
import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useDropzone } from 'react-dropzone';
import commonStyles from './common.css';

import { API_BASE, get, post } from '../api';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  avatar: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    marginRight: theme.spacing(1)
  }
}));

function Accept({ onUpload }) {
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: '.gz'
  });

  const upload = () => {
    onUpload(acceptedFiles);
  };

  return (
    <Box>
      <Typography {...getRootProps({ className: commonStyles.dropzone })}>
        <input {...getInputProps()} />
        Drag & drop files here, or click to select files.
        <br />
        (Only *.zip files will be accepted)
      </Typography>

      <List>
        {acceptedFiles.map(file => (
          <ListItem key={file.path}>
            <ListItemIcon>
              <AttachmentIcon />
            </ListItemIcon>
            <ListItemText
              secondary={`${file.size} bytes`}
              primary={file.path}
            />
          </ListItem>
        ))}
      </List>
      <Button variant="outlined" type="button" onClick={upload}>
        Upload
      </Button>
    </Box>
  );
}

const filterModule = (modules, availModules) =>
  modules.filter(name => availModules.some(avail => avail.name === name));

export default function Module({ modules = [], saveModules }) {
  const classes = useStyles();
  const [availModules, setAvailModules] = useState([]);

  const loadModules = async () => {
    const data = await get(`/module/list/${process.env.TARGET}`);
    setAvailModules(data);
    const newChecked = filterModule(modules, data);
    if (newChecked.length !== modules.length) saveModules(newChecked);
  };

  useEffect(() => {
    loadModules(process.env.TARGET);
  }, []);

  const handleUpload = async files => {
    await Promise.all(files.map(file => post('/module/upload', file, true)));
    loadModules();
  };

  const deleteModule = async moduleName => {
    await get(`/module/delete/${process.env.TARGET}/${moduleName}`);
    loadModules();
  };

  const handleToggle = value => () => {
    // remove delete module
    const newChecked = filterModule(modules, availModules);

    const currentIndex = newChecked.indexOf(value);

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    saveModules(newChecked);
  };

  return (
    <Grid container spacing={2}>
      <Grid item sm={6} xs={12}>
        <Accept onUpload={handleUpload} />
      </Grid>
      <Grid item sm={6} xs={12}>
        <List
          subheader={<ListSubheader>Modules</ListSubheader>}
          className={classes.root}
        >
          {availModules.map(({ name, image, description }) => (
            <ListItem key={name}>
              <ListItemIcon>
                <Avatar alt={name} src={image} className={classes.avatar} />
              </ListItemIcon>
              {process.env.NODE_ENV === 'production' ? (
                <ListItemText primary={description} />
              ) : (
                <Link
                  color="inherit"
                  href={`${API_BASE}/module/download/${process.env.TARGET}/${name}`}
                >
                  <ListItemText primary={description} />
                </Link>
              )}
              <ListItemSecondaryAction>
                <Switch
                  edge="end"
                  onChange={handleToggle(name)}
                  checked={modules.includes(name)}
                />
                <IconButton edge="end" onClick={() => deleteModule(name)}>
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </Grid>
    </Grid>
  );
}
