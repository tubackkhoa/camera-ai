/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import md from 'machine-digest';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { FAILURE } from '../server/status';
import { get, post } from '../api';

const machineId = md.digest('hex');

const checkLicense = async () => {
  // if (!licenseKey) return false;
  const { status } = await get(`/license/authorize/${machineId}`);
  return status !== FAILURE;
};

const LicenseChecker = ({ interval = 1000, successInterval = 60000 }) => {
  const [open, setOpen] = useState(false);

  // only reset when interval changed
  useEffect(() => {
    let timer;
    const checker = async () => {
      const success = await checkLicense();
      if (success) {
        setOpen(false);
      } else {
        setOpen(true);
      }

      // if success should check longer
      timer = setTimeout(checker, success ? successInterval : interval);
    };

    // start check
    checker();

    return () => clearTimeout(timer);
  }, [interval, successInterval]);

  // if empty license or not valid, open dialog
  const handleUpdateLicense = e => {
    // store.dispatch(setLicense(e.target.value.trim()));
    const licenseKey = e.target.value.trim();
    post('/license/authorize', {
      licenseKey,
      machineId
    });
  };

  return (
    <Dialog open={open} fullWidth>
      <DialogTitle id="simple-dialog-title">Enter license key</DialogTitle>
      <DialogContent>
        <TextField
          fullWidth
          label="Machine ID"
          disabled
          defaultValue={machineId}
        />
        <TextField
          fullWidth
          label="Paste License"
          multiline
          onChange={handleUpdateLicense}
        />
      </DialogContent>
    </Dialog>
  );
};

export default LicenseChecker;
