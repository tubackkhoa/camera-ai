import DragAndDropWrapper from './DragAndDropWrapper';
import withMaxItems from './withMaxItems';

const ComponentWithMaxItems = withMaxItems(DragAndDropWrapper);

export default ComponentWithMaxItems;
