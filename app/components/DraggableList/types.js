export type Location = {
  id: string,
  index: number
};

export type Omit<P, O> = Pick<P, Exclude<$Keys<P>, $Keys<O>>>;

export type DragAndDropResult = {
  source: Location,
  destination: Location
};

export type Chunk = {
  id: string,
  items: any[]
};

export type WithMaxItemsProps = {
  items: any[],
  maxItems?: number,
  onDragEnd(sourceIndex: number, destinationIndex: number): void
};

export type WithMaxItemsState = {
  maxItems: number,
  items: any[],
  chunks: Chunk[]
};
