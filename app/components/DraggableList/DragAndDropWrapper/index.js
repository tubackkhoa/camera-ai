import React, { ReactElement } from 'react';
import {
  DragDropContext,
  Droppable,
  DroppableProvided,
  DropResult
} from 'react-beautiful-dnd';
import ListManagerItem from './ListManagerItem';
import type { DragAndDropResult, Chunk } from '../types';

const DragDropContainer = ({
  items,
  getKey,
  renderItem,
  itemProps,
  placeholder
}) => {
  return (
    <>
      {items.map((item: any, index: number) => {
        const key = getKey(item);
        return (
          <ListManagerItem
            key={key}
            draggableId={key}
            item={item}
            index={index}
            renderProps={itemProps}
            render={renderItem}
          />
        );
      })}
      {placeholder}
    </>
  );
};

export type Props = {
  chunks: Chunk[],
  direction: 'horizontal' | 'vertical',
  renderItem(item: any): ReactElement<{}>,
  renderContainer(item: any): ReactElement<{}>,
  getKey(item: any): string,
  onDragEnd(result: DragAndDropResult): void
};

const DragAndDropWrapper: React.StatelessComponent<Props> = ({
  onDragEnd,
  chunks,
  direction,
  getKey,
  renderContainer,
  containerProps,
  renderItem,
  itemProps
}: Props) => {
  return (
    <DragDropContext onDragEnd={mapAndInvoke(onDragEnd)}>
      {chunks.map(({ id: droppableId, items }: Chunk) => (
        <Droppable
          key={droppableId}
          droppableId={droppableId}
          direction={direction}
        >
          {(provided: DroppableProvided) =>
            renderContainer(
              {
                ...provided.droppableProps,
                ...containerProps,
                children: (
                  <DragDropContainer
                    items={items}
                    itemProps={itemProps}
                    getKey={getKey}
                    renderItem={renderItem}
                    placeholder={provided.placeholder}
                  />
                )
              },
              provided.innerRef
            )
          }
        </Droppable>
      ))}
    </DragDropContext>
  );
};

function mapAndInvoke(onDragEnd: (result: DragAndDropResult) => void) {
  return ({ source, destination }: DropResult): void => {
    if (destination !== undefined && destination !== null) {
      const result: DragAndDropResult = {
        source: {
          id: source.droppableId,
          index: source.index
        },
        destination: {
          id: destination.droppableId,
          index: destination.index
        }
      };

      onDragEnd(result);
    }
  };
}

export default DragAndDropWrapper;
