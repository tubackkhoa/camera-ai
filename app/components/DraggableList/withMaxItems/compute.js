import chunk from 'lodash/chunk';
import type { Chunk } from '../types';

function mapToChunk(items: any[], index: number): Chunk {
  return {
    id: index.toString(),
    items
  };
}

export function splitItems(maxItems: number, items: any[]): Chunk[] {
  const slicedItems: any[][] = chunk(items, maxItems);
  return slicedItems.map(mapToChunk);
}

export function computeOriginalIndex(
  maxItems: number,
  chunkIndex: number,
  indexInChunk: number
): number {
  return chunkIndex * maxItems + indexInChunk;
}

export function computeOriginalIndexAfterDrop(
  maxItems: number,
  sourceChunkIndex: number,
  destinationChunkIndex: number,
  indexInChunk: number
): number {
  return (
    destinationChunkIndex * maxItems +
    indexInChunk +
    (sourceChunkIndex < destinationChunkIndex ? -1 : 0)
  );
}
