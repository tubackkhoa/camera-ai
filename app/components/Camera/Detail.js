/* eslint-disable react/no-unused-prop-types */
import React, { useCallback } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import type { DeviceState } from '../../reducers/types';

import {
  LabelCheckboxWithKey,
  DateAndTimePickers,
  SchedulerType
} from '../common';

const detailFields = [
  'FirmwareVersion',
  'HardwareId',
  'Manufacturer',
  'Model',
  'SerialNumber'
];

// show more info when development
if (process.env.NODE_ENV === 'development') {
  detailFields.push(
    ...[
      'snapshot',
      'stream',
      'flvStream',
      'mainSnapshot',
      'mainStream',
      'mainFlvStream'
    ]
  );
}

const schedulerTypes = [1, 2, 3, 4];

type Props = {
  device: DeviceState,
  onFieldChange: (value: DeviceState, field: string, itemValue: any) => void
};

export default function CameraDetail({ device, onFieldChange }: Props) {
  return useCallback(
    <List>
      <ListItem>
        <LabelCheckboxWithKey
          value={device}
          field="isIPCam"
          label="IP Cam"
          onChange={onFieldChange}
        />
        <LabelCheckboxWithKey
          value={device}
          field="record"
          label="Record"
          onChange={onFieldChange}
        />

        <LabelCheckboxWithKey
          value={device}
          field="saveAuto"
          label="Auto"
          onChange={onFieldChange}
        />
      </ListItem>

      <ListItem alignItems="center">
        {device.saveAuto && (
          <>
            <DateAndTimePickers
              value={device}
              field="startRecordTime"
              label="Start Record time"
              onChange={onFieldChange}
            />
            <SchedulerType
              schedulerTypes={schedulerTypes}
              device={device}
              onChange={onFieldChange}
            />
            <LabelCheckboxWithKey
              value={device}
              field="scheduler"
              label="Repeat"
              onChange={onFieldChange}
            />
          </>
        )}
      </ListItem>

      {detailFields.map(field => (
        <ListItem key={field}>
          <ListItemText primary={field} secondary={device[field]} />
        </ListItem>
      ))}
    </List>,
    [device]
  );
}
