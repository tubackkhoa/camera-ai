import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import Typography from '@material-ui/core/Typography';
import MaterialTable from 'material-table';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';

import { ScanCamForm, tableIcons, tableIcon } from '../common';
import Detail from './Detail';
import { StringURL } from '../../utils/common';
import { tableLocalization } from '../../locales';
import { initialState } from '../../reducers/settings';
import type { CameraPageProps } from '../../containers/CameraPage';
import type { DeviceState } from '../../reducers/types';

const columnMap = {
  detectable: {
    title: 'Detect',

    type: 'boolean',
    sorting: false,
    hidden: process.env.DISABLED_AI === 'true'
  },
  tcp: {
    title: 'TCP',
    type: 'boolean',
    sorting: false
  },
  name: { title: 'Name' },
  url: {
    title: 'URL',

    sorting: false,
    cellStyle: { maxWidth: 200 },
    render: rowData => {
      if (!rowData) return null;
      const { url } = rowData;
      const { hostname } = new StringURL(url);
      return (
        <Typography color="secondary" noWrap>
          {hostname}
        </Typography>
      );
    }
  },
  user: { title: 'User', sorting: false },
  pass: {
    title: 'Pass',

    sorting: false,
    render: () => <VisibilityOffIcon />
  }
};

const defaultColumns = [
  { field: 'detectable' },
  { field: 'tcp' },
  { field: 'name', defaultSort: 'asc' },
  { field: 'url' },
  { field: 'user' },
  { field: 'pass' }
];

const getColumns = config => {
  let columnsConfig = config || defaultColumns;
  const saveFields = new Set(
    columnsConfig.concat(defaultColumns).map(c => c.field)
  );
  // reset if changed
  if (saveFields.size !== columnsConfig.length) {
    columnsConfig = defaultColumns;
  }
  return columnsConfig.map(column => ({
    ...column,
    ...columnMap[column.field]
  }));
};

const UncheckTableIcon = tableIcon(CheckBoxOutlineBlankIcon);

export default class Camera extends Component<CameraPageProps> {
  handleFieldChange = (rowData: DeviceState, field, value) => {
    const newDevice = { ...rowData, [field]: value };
    const { handleDeviceUpdate } = this.props;
    return handleDeviceUpdate(newDevice, rowData);
  };

  handlePageSize = (page: number) => {
    const { setItem } = this.props;
    setItem('pageSize', page);
  };

  handleColumnDragged = (srcInd, destInd) => {
    const { settings, setItem } = this.props;

    const columnsConfig = settings.columns || defaultColumns;
    const newColumns = [...columnsConfig];
    const [removed] = newColumns.splice(srcInd, 1);
    newColumns.splice(destInd, 0, removed);

    setItem('columns', newColumns);
  };

  handleOrderChange = (colId, direction) => {
    if (colId === -1) return;
    const { settings, setItem } = this.props;

    const columnsConfig = settings.columns || defaultColumns;
    const newColumns = [...columnsConfig];
    newColumns[colId].defaultSort = direction;
    setItem('columns', newColumns);
  };

  renderDetail = rowData => (
    <Detail device={rowData} onFieldChange={this.handleFieldChange} />
  );

  render() {
    const {
      settings,
      devices,
      handleDeviceAdd,
      handleDeviceUpdate,
      handleDeviceDelete
    } = this.props;

    const columns = getColumns(settings.columns);
    const pageSize = settings.pageSize || initialState.pageSize;
    return (
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <ScanCamForm onDeviceAdd={handleDeviceAdd} />
        </Grid>
        <Grid item xs={8}>
          <MaterialTable
            icons={tableIcons}
            onChangeRowsPerPage={this.handlePageSize}
            localization={tableLocalization[settings.locale]}
            title="Camera (Drag & drop, sort auto-save)"
            columns={columns}
            data={devices}
            options={{
              addRowPosition: 'first',
              detailPanelType: 'single',
              pageSize,
              rowStyle: ({ running }) => {
                if (running) {
                  return {
                    backgroundColor: 'rgba(245, 0, 87, 0.16)'
                  };
                }
              }
            }}
            actions={[
              ({ running }) => ({
                icon: running ? tableIcons.Check : UncheckTableIcon,
                tooltip: 'Choose Cam',
                onClick: (event, rowData) =>
                  handleDeviceUpdate(
                    { ...rowData, running: !rowData.running },
                    rowData
                  )
              })
            ]}
            onColumnDragged={this.handleColumnDragged}
            onOrderChange={this.handleOrderChange}
            detailPanel={this.renderDetail}
            editable={{
              onRowAdd: handleDeviceAdd,
              onRowUpdate: handleDeviceUpdate,
              onRowDelete: handleDeviceDelete
            }}
          />
        </Grid>
      </Grid>
    );
  }
}
