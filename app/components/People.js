/* eslint-disable no-plusplus */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react/jsx-props-no-spreading */
import React, { Component, useEffect, useState, useCallback } from 'react';
import uniqBy from 'lodash/uniqBy';
import debounce from 'lodash/debounce';
import { useDropzone } from 'react-dropzone';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Skeleton from '@material-ui/lab/Skeleton';
import { FormattedMessage, useIntl } from 'react-intl';
import Field from 'redux-form/es/Field';
import FieldArray from 'redux-form/es/FieldArray';
import reduxForm from 'redux-form/es/reduxForm';
import AddIcon from '@material-ui/icons/Add';
import type { FieldProps } from 'redux-form';

// import command from '../utils/electron-command';
import styles from './People.css';
import commonStyles from './common.css';
import { post, fetchWithOptions } from '../api';

const ImageList = ({ files, onRemove }) => {
  if (!Array.isArray(files)) return null;

  return (
    <GridList cellHeight={100} className={styles.gridList} cols={3}>
      {files.map((file, index) => (
        <GridListTile key={index}>
          <img src={file.path} alt={file.name} />
          <GridListTileBar
            title={file.name}
            actionIcon={
              <IconButton
                size="small"
                onClick={() => onRemove(index)}
                aria-label={file.path}
              >
                <DeleteIcon />
              </IconButton>
            }
          />
        </GridListTile>
      ))}
    </GridList>
  );
};

function PeopleField({ input, label, onUpdate, filesLimit = 10 }) {
  const { formatMessage } = useIntl();

  const { getRootProps, getInputProps } = useDropzone({
    accept: ['image/jpg', 'image/jpeg', 'image/png', 'image/bmp'],
    maxSize: 5000000,
    onDrop: useCallback(acceptedFiles => {
      // update
      input.onChange(value => ({
        ...value,
        files: uniqBy(
          acceptedFiles
            .map(file => ({ name: file.name, path: file.path }))
            .filter(file => file && file.path)
            .slice(0, filesLimit)
            .concat(value.files),
          'name'
        )
      }));
    }, [])
  });

  const debouncedUpdate = useCallback(
    debounce(
      val =>
        input.onChange(value => ({
          ...value,
          label: val
        })),
      500
    ),
    []
  );

  const handleRemoveFile = index => {
    const files = input.value.files.filter(file => file && file.path);
    files.splice(index, 1);
    input.onChange(value => ({
      ...value,
      files
    }));
  };

  const handleUpdateLabel = e => {
    e.persist();
    debouncedUpdate(e.target.value);
  };

  const nameNotEmpty = input.value.label && input.value.label.trim().length;
  return (
    <>
      <TextField
        label={label}
        onChange={handleUpdateLabel}
        defaultValue={input.value.label}
        fullWidth
      />

      {nameNotEmpty && (
        <>
          <Typography {...getRootProps({ className: commonStyles.dropzone })}>
            <input {...getInputProps()} />
            {formatMessage({ id: 'people.dragFiles' })}
          </Typography>
          <ImageList
            files={
              input.value.files
                ? input.value.files.filter(file => file && file.path)
                : []
            }
            onRemove={handleRemoveFile}
          />
          <Button onClick={onUpdate}>
            <FormattedMessage id="app.updateModel" />
          </Button>
        </>
      )}
    </>
  );
}

const RenderPeople = ({ fields, meta: { error }, aiServer }: FieldProps) => {
  const [msg, setMessage] = useState(null);
  const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   command.on('getDescriptors', (e, ret) => {
  //     if (ret)
  //       ret.forEach(({ label, descriptors }) => {
  //         post(`/model/descriptor/${label}`, Buffer.concat(descriptors), true);
  //         setMessage({ children: 'Cập nhật thành công', severity: 'success' });
  //       });

  //     setLoading(false);
  //   });

  //   return () => command.off('getDescriptors');
  // }, []);

  const sanitizeFaceMatcher = people => {
    const faces = people.filter(p => p).map(p => p.label);
    post(`${aiServer}/api/sanitize_faces`, { faces });

    // send labels to update models
    // command.run('sanitizeFaceMatcher', labels);
  };

  const extractFields = () => {
    const ret = [];
    for (let i = 0; i < fields.length; ++i) {
      ret.push(fields.get(i));
    }

    return ret;
  };

  const updatePeople = async index => {
    // re-update all people labels
    const people = extractFields();
    const person = fields.get(index);
    sanitizeFaceMatcher(people);
    setLoading(true);
    // serialized object is ok because not often call
    // console.log(people, person);
    if (person && person.label && Array.isArray(person.files)) {
      // wait for signal
      // command.run('getDescriptors', [person]);
      // update for this person

      const formData = new FormData();
      formData.append('name', person.label);

      const snapshots = await Promise.all(
        person.files
          .filter(file => file)
          .map(file => fetch(file.path).then(r => r.blob()))
      );

      snapshots.forEach(snapshot => {
        formData.append('file', snapshot);
      });

      await fetchWithOptions(`${aiServer}/api/save_face`, {
        method: 'POST',
        body: formData
      });

      setLoading(false);
      setMessage({
        children: `Cập nhật thành công: ${person.label}`,
        severity: 'success'
      });
    }
  };

  const removePeople = index => {
    // re-update all people labels
    // const person = fields.get(index);
    const people = extractFields();
    people.splice(index, 1);
    fields.remove(index);
    // console.log(people, fields.getAll());
    sanitizeFaceMatcher(people);
    // if (person && person.label) {
    //   get(`/model/descriptor/${person.label}`, 'DELETE');
    // }
  };

  return (
    <>
      <Fab
        color="secondary"
        onClick={() => fields.push()}
        className={styles.floatBtn}
        variant="extended"
      >
        <FormattedMessage id="people.add" />
        <AddIcon />
      </Fab>

      <Grid item xs={4}>
        {loading && <Skeleton animation="wave" width="100%" />}

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          open={!!msg}
          onClose={() => setMessage(null)}
          autoHideDuration={3000}
        >
          <Alert {...msg} />
        </Snackbar>
      </Grid>

      <Grid container spacing={3}>
        {fields.map((people, index) => (
          <Grid item xs={3} key={people}>
            <Field
              name={people}
              type="text"
              component={PeopleField}
              onUpdate={() => updatePeople(index)}
              label={
                <FormattedMessage
                  id="people.no"
                  values={{ index: index + 1 }}
                />
              }
            />

            <Button onClick={() => removePeople(index)}>
              <FormattedMessage id="people.remove" />
            </Button>
          </Grid>
        ))}
        <Typography variant="h1" component="h2">
          {error}
        </Typography>
      </Grid>
    </>
  );
};

@reduxForm({
  form: 'People', // a unique identifier for this form
  destroyOnUnmount: false
})
export default class PeopleForm extends Component {
  render() {
    return (
      <FieldArray name="people" component={RenderPeople} {...this.props} />
    );
  }
}
