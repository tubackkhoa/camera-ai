/* eslint-disable compat/compat */
/* eslint-disable prefer-destructuring */
/* eslint-disable max-classes-per-file */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable react/destructuring-assignment */

/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ReplayIcon from '@material-ui/icons/Replay';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import ZoomOutMapIcon from '@material-ui/icons/ZoomOutMap';
import Typography from '@material-ui/core/Typography';
import PauseIcon from '@material-ui/icons/Pause';
import Fade from '@material-ui/core/Fade';
import debounce from 'lodash/debounce';
import screenfull from 'screenfull';

import { injectIntl } from 'react-intl';

import { PeopleCount } from './common';
import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';
import { post, fetchWithOptions, WS_BASE, API_BASE } from '../api';
import { getSnapshot, copyVideo2Canvas } from '../utils';
import { getStreamName } from '../utils/common';
import styles from './StreamPlayer.css';
import flvjs from './FLV';
import type { DeviceState } from '../reducers/types';
// import {
//   assignDetectionTask,
//   addDetectionListener,
//   removeDetectionListener
// } from '../utils/faceapi/emitter';
import { saveFacesResult } from '../utils/faceapi/saver';
import BlinkScreen from './BlinkScreen';

const debouncedUpdate = debounce(tool => {
  tool.style.opacity = 0;
  tool.style.visibility = 'hidden';
}, 1000);

type Props = {
  autoPlay?: boolean,
  detectInterval?: number,
  maxDelay?: number,
  detectQuality?: number,
  maxDelayRetry?: number,
  countConfidence?: number,
  warningConfig?: any,
  media?: any,
  config?: any
} & DeviceState;

@injectIntl
class StreamPlayer extends Component<Props> {
  static defaultProps = {
    autoPlay: true,
    detectInterval: 1000,
    maxDelay: 2500,
    maxDelayRetry: 10000,
    countConfidence: 0.7,
    detectQuality: 1.0,
    warningConfig: {},
    media: {
      type: 'flv',
      isLive: true,
      cors: true
    },
    config: {
      enableWorker: true,
      enableStashBuffer: false,
      stashInitialSize: 128
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      playing: props.autoPlay,
      showControls: false,
      playWarningAudio: false,
      blinkScreen: false
    };
    this.videoElement = React.createRef();
    this.canvas = React.createRef();
    this.control = React.createRef();
    this.peopleCount = React.createRef();
    this.blinkRef = React.createRef();

    // use mainStream as slot
    // console.log(props.url);
    // console.log('here ', MAIN_STREAM);
    this.slot = getStreamName(props.url, MAIN_STREAM);
    if (
      props.warningConfig &&
      props.warningConfig.sound &&
      props.warningConfig.soundPath
    ) {
      this.audio = new Audio(props.warningConfig.soundPath);
    }

    // this.slot = props.mainFlvStream.match(/(\w+)\.flv/)[1];

    // addDetectionListener(`detectImage.${this.slot}`, this.onDetectImage);
    // addDetectionListener(`predictObjects.${this.slot}`, this.onPredictObjects);

    // console.log(props);
  }

  componentDidMount() {
    const { detectable, autoPlay } = this.props;
    // guard code in asynchronous context
    if (!this.videoElement.current) return;

    this.videoElement.current.parentElement.addEventListener(
      'fullscreenchange',
      this.fullscreenChange
    );

    // wait for init
    if (autoPlay) {
      this.replay(true);
    }

    if (detectable) {
      // will reload when prop change
      console.log('start detection');
      // run detect
      this.detect();
    }
    if (this.audio) {
      this.audio.addEventListener('ended', () =>
        this.setState({ playWarningAudio: false })
      );
    }
  }

  componentWillUnmount() {
    // this slot is unique so remove all is best
    // removeDetectionListener(`detectImage.${this.slot}`);
    // removeDetectionListener(`predictObjects.${this.slot}`);

    clearTimeout(this.detectTimer);
    clearTimeout(this.playerTimer);

    if (this.videoElement.current) {
      const { parentElement } = this.videoElement.current;

      parentElement.removeEventListener(
        'fullscreenchange',
        this.fullscreenChange
      );
    }

    if (this.flvPlayer) {
      this.destroyFlv();
    }
    if (this.audio) {
      this.audio.removeEventListener('ended', () =>
        this.setState({ playWarningAudio: false })
      );
    }
  }

  ensureReloadPlayer() {
    clearTimeout(this.playerTimer);
    // if no data received in maxDelayRetry, will retry connect
    this.playerTimer = setTimeout(this.replay, this.props.maxDelayRetry);
  }

  onRecognition = data => {
    const { warningConfig } = this.props;
    // if (!warningConfig || warningConfig.type !== 0) return;

    const { people } = warningConfig;

    let check = false;
    data.forEach(item => {
      if (item.label && !item.label.match(/unknown/i)) {
        // if item.label in  people => send warning
        if (people && people.includes(item.label)) {
          check = true;
          // send mail
          // send mail
        }
      }
    });

    return check;
  };

  onPredictObjects = (classes, scores) => {
    const { warningConfig, countConfidence } = this.props;
    if (!classes) return false;
    const count = classes.filter(
      (c, i) => c === 0 && scores[i] >= countConfidence
    ).length;

    // const count = Array.isArray(data)
    //   ? data.filter(item => item.class === 'person').length
    //   : 0;

    if (this.peopleCount.current) this.peopleCount.current.setState({ count });

    // counting
    // if (!warningConfig || warningConfig.type !== 1) return;

    let check = false;

    const { peopleNumber } = warningConfig;

    if (peopleNumber && count > peopleNumber) {
      check = true;
    }
    return check;
  };

  // send mail
  alertPredictCountCheck = imagePath => {
    const { warningConfig, intl, name } = this.props;
    const { mail, mailReceipents, sound, blink } = warningConfig;
    let count = 0;
    if (this.peopleCount.current) {
      count = this.peopleCount.current.state.count;
    }
    if (mail) {
      const subject = intl.formatMessage(
        {
          id: 'app.warningSubject'
        },
        { name }
      );
      const message = intl.formatMessage(
        {
          id: 'app.warningPeopleCount'
        },
        { total: count }
      );
      const bbc = mailReceipents.replace(/\s*\n+\s*/g, ', ');
      if (bbc) post('/utils/sendMail', { subject, message, bbc, imagePath });
    }
    if (sound && this.audio && !this.state.playWarningAudio) {
      // this.audio.currentTime = 0;
      this.audio.play();
    }
    if (blink && !this.state.blinkScreen) {
      this.setState({ blinkScreen: true });
      if (this.blinkRef && this.blinkRef.current) {
        this.blinkRef.current.starterBlink();
      }
    }
  };

  alertRecognition = (data, imagePath) => {
    const { warningConfig, intl, name } = this.props;

    const { people, mail, mailReceipents, sound, blink } = warningConfig;
    data.forEach(item => {
      if (item.label && !item.label.match(/unknown/i)) {
        // if item.label in  people => send warning
        if (people && people.length > 0 && people.includes(item.label)) {
          // send mail
          if (mail) {
            const subject = intl.formatMessage(
              {
                id: 'app.warningSubject'
              },
              { name }
            );
            const message = intl.formatMessage(
              {
                id: 'app.personFound'
              },
              { person: item.label }
            );
            const bbc = mailReceipents.replace(/\s*\n+\s*/g, ', ');
            post('/utils/sendMail', { subject, message, bbc, imagePath });
          }
          if (sound && this.audio && !this.state.playWarningAudio) {
            // this.audio.currentTime = 0;
            this.audio.play();
          }
          if (blink && !this.state.blinkScreen) {
            this.setState({ blinkScreen: true });
            if (this.blinkRef && this.blinkRef.current) {
              this.blinkRef.current.starterBlink();
            }
          }
        }
      }
    });
  };

  processSnapshot = async snapshot => {
    // after process, call detect again
    const { onReg, aiServer } = this.props;

    const blobURL = URL.createObjectURL(snapshot);
    if (!this.videoElement.current) return;

    const { videoHeight, videoWidth } = this.videoElement.current;
    // console.log(data);

    // send blobdata directly
    // call ai server here
    const formData = new FormData();
    formData.append('detect_face', '');
    formData.append('file', snapshot);
    const controller = new AbortController();
    const signal = controller.signal;
    setTimeout(() => controller.abort(), 10000);
    const ret = await fetchWithOptions(`${aiServer}/api/detect`, {
      method: 'POST',
      body: formData,
      signal
    });

    if (!ret) return;

    const { faces = [], classes, scores } = ret;

    let time = Date.now();
    const data = faces.map(
      ({ sim, left, top, right, bottom, name: label, score = 1 }) => ({
        blobURL,
        confidence: Math.round(100 * sim * score),
        label,
        rect: {
          width: right - left,
          height: bottom - top,
          imageHeight: videoHeight,
          imageWidth: videoWidth,
          left,
          top
        },
        time: time++
      })
    );

    // callback to display
    if (onReg) onReg(data);

    // check noti
    const predictCheck = this.onPredictObjects(classes, scores);

    const regcognitionCheck = this.onRecognition(data);

    // save result if has noti
    if (predictCheck || regcognitionCheck) {
      const imagePath = await saveFacesResult(
        this.props.url,
        this.slot,
        snapshot,
        data,
        {
          classes,
          scores
        }
      );
      if (predictCheck) {
        this.alertPredictCountCheck(imagePath);
      }
      if (regcognitionCheck) {
        this.alertRecognition(data, imagePath);
      }
    } else {
      this.setState({ playWarningAudio: false, blinkScreen: false }, () => {
        if (this.audio) {
          this.audio.pause();
        }
        if (
          this.blinkRef &&
          this.blinkRef.current
          // this.blinkRef.current.state.blink
        ) {
          this.blinkRef.current.hideBlink();
        }
      });
    }
  };

  handleTransmuxer = () => {
    this.ensureReloadPlayer();

    if (this.state.playing) {
      if (this.flvPlayer.buffered.length) {
        const lastTime = this.flvPlayer.buffered.end(
          this.flvPlayer.buffered.length - 1
        );
        if (
          this.flvPlayer._mediaElement.currentTime +
            this.props.maxDelay / 1000 <
          lastTime
        ) {
          this.flvPlayer._msectl._idrList.clear();
          this.flvPlayer._mediaElement.currentTime = lastTime - 0.1;
          // console.log('catchup', this.props.url);
        }
      }
      // should play because it is playing
      if (this.flvPlayer._mediaElement.paused) this.flvPlayer.play();
    }
  };

  updatePoster() {
    const { url, snapshot } = this.props;

    if (snapshot && this.videoElement.current) {
      const poster = `${API_BASE}/onvif/snapshot?url=${encodeURIComponent(
        url
      )}&t=${Date.now()}`;

      this.videoElement.current.setAttribute('poster', poster);
    }
  }

  destroyFlv() {
    // this.flvPlayer.unload();
    // this.flvPlayer.detachMediaElement(this.videoElement.current);
    this.flvPlayer.destroy();
    this.flvPlayer = null;
    // console.log('destroy');
  }

  initFlv(url) {
    if (flvjs.isSupported() && this.videoElement.current) {
      // update poster with new snapshot
      this.updatePoster();
      const { media, config } = this.props;
      this.flvPlayer = flvjs.createPlayer({ ...media, url }, config);
      this.flvPlayer.attachMediaElement(this.videoElement.current);
      this.flvPlayer.load();
      this.flvPlayer.on('error', (type, mediaType) => {
        if (type === 'MediaError' && mediaType === 'MediaMSEError') {
          this.destroyFlv();
          this.initFlv(url);
          // console.log(type, message);
        }
      });

      // if no data received then re-load
      this.flvPlayer.on('video', this.handleTransmuxer);
      this.ensureReloadPlayer();

      // already has poster, and autoplay
      // wait for play then assign flvPlayer
      if (this.state.playing && this.flvPlayer) {
        return this.flvPlayer.play();
      }
    }
  }

  async initPlayer(stream) {
    if (this.flvPlayer) {
      // this.player.destroy();
      this.destroyFlv();
    }
    if (stream) {
      this.currentStream = stream;
      const { url } = this.props;

      // may have new stream
      const { streamURL } = await post('/camera/add', {
        url,
        stream
      });

      if (streamURL) return this.initFlv(`${WS_BASE}${streamURL}`);
    }

    return false;
  }

  detect = async () => {
    // console.log('running', this.flvPlayer);
    // stop running when runout
    const { detectInterval, detectQuality } = this.props;
    if (this.flvPlayer) {
      const canvas = this.canvas.current;
      const video = this.videoElement.current;
      if (video && this.state.playing) {
        copyVideo2Canvas(video, canvas);
        const snapshot = await getSnapshot(canvas, detectQuality);
        if (snapshot) {
          await this.processSnapshot(snapshot);
          // then call it again
          this.detectTimer = setTimeout(this.detect, detectInterval);
          // assignDetectionTask('detectImage', canvas, blobURL, this.slot);
          // assignDetectionTask('predictObjects', canvas, blobURL, this.slot);

          // return and wait for callback to run
          return;
        }
      }
    }
    // otherwise just re-do
    this.detectTimer = setTimeout(this.detect, detectInterval);
  };

  getStreamToPlay() {
    const { detectable, mainStream } = this.props;
    const { fullscreen } = document;
    return (fullscreen || detectable) && mainStream ? MAIN_STREAM : SUB_STREAM;
  }

  fullscreenChange = async () => {
    const streamToPlay = this.getStreamToPlay();
    // do nothing, because already the same stream
    if (this.currentStream === streamToPlay) return true;
    await this.initPlayer(streamToPlay);
  };

  // just refresh
  replay = async (reset = false) => {
    const { url, user, pass, tcp, isIPCam } = this.props;

    // retry re-init cam
    if (reset) {
      await post('/onvif/device_connect', {
        url,
        user,
        pass,
        tcp,
        isIPCam
      });
    }

    const streamToPlay = this.getStreamToPlay();
    await this.initPlayer(streamToPlay);
  };

  handleStream = async () => {
    const streamToPlay = this.getStreamToPlay();
    if (!this.flvPlayer) {
      // wait for initialization
      await this.initPlayer(streamToPlay);
    }
    const { playing } = this.state;
    if (playing) {
      if (this.audio) {
        this.audio.pause();
      }
      this.flvPlayer.pause();
      this.setState({ playing: false });
    } else {
      await this.flvPlayer.play();
      this.setState({ playing: true });
    }
  };

  moveLeft = () => {
    post('/onvif/device_execute', { action: 'left', url: this.props.url });
  };

  moveRight = () => {
    post('/onvif/device_execute', { action: 'right', url: this.props.url });
  };

  moveUp = () => {
    post('/onvif/device_execute', { action: 'up', url: this.props.url });
  };

  moveDown = () => {
    post('/onvif/device_execute', { action: 'down', url: this.props.url });
  };

  zoomIn = () => {
    const { url } = this.props;
    post('/onvif/device_execute', {
      action: 'ptz-zoom-in',
      url
    });
  };

  zoomOut = () => {
    const { url } = this.props;
    post('/onvif/device_execute', {
      action: 'ptz-zoom-out',
      url
    });
  };

  fullscreen = () => {
    if (screenfull.isFullscreen) {
      return screenfull.exit();
    }

    // now switch stream
    // this.fullscreenChange(true);
    return screenfull.request(this.videoElement.current.parentElement);
  };

  hideControl = () => this.setState({ showControls: false });

  showControl = () => this.setState({ showControls: true });

  checkControlVisible = () => {
    if (!screenfull.isFullscreen || !this.control.current) return;
    this.control.current.style.opacity = 1;
    this.control.current.style.visibility = 'visible';
    debouncedUpdate(this.control.current);
  };

  render() {
    const { playing, showControls } = this.state;
    const { name, zoom, videoRatio, detectable } = this.props;

    return (
      <div
        className={styles.videoWrapper}
        style={{ paddingBottom: `${videoRatio * 100}%` }}
        onMouseEnter={this.showControl}
        onMouseLeave={this.hideControl}
        onMouseMove={this.checkControlVisible}
      >
        <video
          muted
          controls={false}
          className={styles.video}
          onDoubleClick={this.fullscreen}
          ref={this.videoElement}
        />
        <canvas ref={this.canvas} style={{ display: 'none' }} />
        {detectable && (
          <PeopleCount
            ref={this.peopleCount}
            className={styles.count}
            right={10}
            top={10}
          />
        )}
        <BlinkScreen
          ref={this.blinkRef}
          className={styles.blink}
          right={60}
          top={10}
        />

        <Fade in={showControls}>
          <Grid
            ref={this.control}
            style={{ zoom }}
            container
            classes={{ root: styles.tools }}
          >
            <Grid item xs={4} sm>
              <Typography
                className={styles.videoTitle}
                variant="caption"
                noWrap
                align="left"
                display="inline"
              >
                {name}
              </Typography>
            </Grid>
            <Grid item>
              <IconButton
                aria-label="left"
                size="small"
                onClick={this.moveLeft}
              >
                <ArrowLeftIcon />
              </IconButton>
              <IconButton
                aria-label="right"
                size="small"
                onClick={this.moveRight}
              >
                <ArrowRightIcon />
              </IconButton>
              <IconButton aria-label="up" size="small" onClick={this.moveUp}>
                <ArrowDropUpIcon />
              </IconButton>
              <IconButton
                aria-label="down"
                size="small"
                onClick={this.moveDown}
              >
                <ArrowDropDownIcon />
              </IconButton>
              <IconButton
                aria-label="zoom in"
                size="small"
                onClick={this.zoomIn}
              >
                <ZoomInIcon />
              </IconButton>
              <IconButton
                aria-label="zoom out"
                size="small"
                onClick={this.zoomOut}
              >
                <ZoomOutIcon />
              </IconButton>
              <IconButton
                aria-label="fullscreen"
                size="small"
                onClick={this.fullscreen}
              >
                <ZoomOutMapIcon />
              </IconButton>

              <IconButton
                variant="extended"
                aria-label="play/pause"
                onClick={this.handleStream}
              >
                {playing ? <PauseIcon /> : <PlayArrowIcon />}
              </IconButton>

              <IconButton
                aria-label="replay"
                size="small"
                onClick={() => this.replay(true)}
              >
                <ReplayIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Fade>
      </div>
    );
  }
}

export default StreamPlayer;
