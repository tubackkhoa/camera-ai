/* eslint-disable compat/compat */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { Component, useCallback } from 'react';
import debounce from 'lodash/debounce';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import FolderIcon from '@material-ui/icons/Folder';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import dayjs from 'dayjs';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import { TimePicker } from '@material-ui/pickers';
import { FormattedMessage, useIntl } from 'react-intl';
import Field from 'redux-form/es/Field';
import FieldArray from 'redux-form/es/FieldArray';
import reduxForm from 'redux-form/es/reduxForm';
import Box from '@material-ui/core/Box';

import type { FieldProps } from 'redux-form';

import { remote } from 'electron';

import classes from './Warning.css';
import { StringURL } from '../utils/common';
import { format } from '../constants/date.json';

const WarningTypes = ['warning.type.0', 'warning.type.1'];
const columns = [
  { id: 'warning.label', style: { width: '40%' } },
  {
    id: 'warning.schedule'
  },
  {
    id: 'app.action',
    style: { width: 50 },
    align: 'right'
  }
];

const WarningField = ({ input, devices, peopleOptions, dayOptions }) => {
  const { formatMessage } = useIntl();
  const { onChange, value = {} } = input;
  const currentDay = dayjs().format(format);
  const {
    name = '',
    camera = [],
    people = [],
    days = [],
    type = 0,
    repeat,
    mail,
    startTime = currentDay,
    endTime = currentDay,
    mailReceipents,
    blink,
    sound,
    soundPath = '',
    peopleNumber = ''
  } = value;

  const updateField = (field, val) =>
    onChange(state => ({
      ...state,
      [field]: val
    }));

  const debouncedUpdate = useCallback(debounce(updateField, 500), []);

  const handleUpdateField = (field, e) => {
    e.persist();
    debouncedUpdate(field, e.target.value);
  };

  const openSelectFolderDialog = async () => {
    const options = {
      properties: ['openFile'],
      filters: [{ name: 'Movies', extensions: ['wav', 'mp3', 'au', 'ogg'] }]
    };
    const { filePaths } = await remote.dialog.showOpenDialog(options);
    if (filePaths && filePaths.length > 0) {
      updateField('soundPath', filePaths[0]);
    }
  };

  const optionLabels = Object.fromEntries(
    devices.map(item => [
      item.url,
      `${item.name} (${new StringURL(item.url).hostname})`
    ])
  );

  return (
    <>
      <TableCell>
        <TextField
          label={formatMessage({ id: 'warning.name' })}
          onChange={e => handleUpdateField('name', e)}
          defaultValue={name}
          fullWidth
        />
        <Box my={2}>
          <Autocomplete
            size="small"
            multiple
            options={devices.map(item => item.url)}
            getOptionLabel={item => optionLabels[item]}
            value={camera}
            autoComplete
            renderInput={params => (
              <TextField
                {...params}
                label={formatMessage({ id: 'app.chooseCam' })}
                fullWidth
              />
            )}
            onChange={(e, val) => updateField('camera', val)}
          />
        </Box>

        <Box my={2}>
          <Select
            label={formatMessage({ id: 'warning.typeLabel' })}
            fullWidth
            value={type}
            onChange={e => updateField('type', e.target.value)}
          >
            {WarningTypes.map((id, index) => (
              <MenuItem key={id} value={index}>
                {formatMessage({ id })}
              </MenuItem>
            ))}
          </Select>
        </Box>

        <Autocomplete
          className={type === 0 ? classes.show : classes.hide}
          size="small"
          multiple
          options={peopleOptions}
          value={people}
          autoComplete
          renderInput={params => {
            return (
              <TextField
                {...params}
                label={formatMessage({ id: 'app.choosePeople' })}
                fullWidth
              />
            );
          }}
          onChange={(e, val) => updateField('people', val)}
        />

        <TextField
          className={type === 1 ? classes.show : classes.hide}
          placeholder={formatMessage({ id: 'warning.peopleNum' })}
          type="number"
          onChange={e => handleUpdateField('peopleNumber', e)}
          defaultValue={peopleNumber}
          fullWidth
        />
      </TableCell>
      <TableCell>
        <FormGroup>
          <Grid container justify="space-between">
            <Grid item>
              <TimePicker
                ampm={false}
                value={startTime}
                label={formatMessage({ id: 'app.startTime' })}
                format={format}
                renderInput={props => (
                  <TextField size="small" variant="standard" {...props} />
                )}
                onChange={dateValue =>
                  updateField('startTime', dateValue.format(format))
                }
              />
            </Grid>
            <Grid item>
              <TimePicker
                ampm={false}
                value={endTime}
                renderInput={props => (
                  <TextField size="small" variant="standard" {...props} />
                )}
                label={formatMessage({ id: 'app.endTime' })}
                format={format}
                onChange={dateValue =>
                  updateField('endTime', dateValue.format(format))
                }
              />
            </Grid>
          </Grid>

          <Box my={2}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={repeat}
                  onChange={e => updateField('repeat', e.target.checked)}
                />
              }
              label={formatMessage({ id: 'app.repeat' })}
            />

            <Select
              className={repeat ? classes.show : classes.hide}
              multiple
              fullWidth
              value={days}
              renderValue={selected => selected.join(', ')}
              onChange={e => updateField('days', e.target.value)}
            >
              {dayOptions.map(day => (
                <MenuItem key={day} value={day}>
                  <Checkbox checked={days.includes(day)} />
                  <ListItemText primary={day} />
                </MenuItem>
              ))}
            </Select>
          </Box>

          <Box my={2}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={mail}
                  onChange={e => updateField('mail', e.target.checked)}
                />
              }
              label={formatMessage({ id: 'app.sendMail' })}
            />

            <TextField
              className={mail ? classes.show : classes.hide}
              multiline
              onChange={e => handleUpdateField('mailReceipents', e)}
              defaultValue={mailReceipents}
              fullWidth
            />
          </Box>

          <FormControlLabel
            control={
              <Checkbox
                checked={blink}
                onChange={e => updateField('blink', e.target.checked)}
              />
            }
            label={formatMessage({ id: 'app.blink' })}
          />

          <Box my={2}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={sound}
                  onChange={e => updateField('sound', e.target.checked)}
                />
              }
              label={formatMessage({ id: 'app.sound' })}
            />

            <TextField
              className={sound ? classes.show : classes.hide}
              disabled
              fullWidth
              value={soundPath}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={openSelectFolderDialog}>
                      <FolderIcon />
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
          </Box>
        </FormGroup>
      </TableCell>
    </>
  );
};

const RenderWarning = ({
  fields,
  devices,
  people,
  locale,
  meta: { error }
}: FieldProps) => {
  const peopleOptions =
    people?.filter(person => person).map(person => person.label) ?? [];

  const dayOptions = dayjs.Ls[locale].weekdays;

  const removeWarning = index => {
    // re-update all people labels
    fields.remove(index);
  };

  return (
    <Paper>
      <Toolbar>
        <Typography variant="subtitle1">
          <FormattedMessage id="warning.add" />
        </Typography>
        <IconButton type="button" onClick={() => fields.push()}>
          <AddIcon />
        </IconButton>
      </Toolbar>

      <TableContainer className={classes.container}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={column.style}
                >
                  <FormattedMessage id={column.id} />
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {fields.map((warning, index) => (
              <TableRow key={warning} className={classes.fieldRow}>
                <Field
                  name={warning}
                  devices={devices}
                  peopleOptions={peopleOptions}
                  dayOptions={dayOptions}
                  component={WarningField}
                />

                <TableCell align="right">
                  <Button onClick={() => removeWarning(index)}>
                    <DeleteIcon />
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Typography variant="h1" component="h2">
        {error}
      </Typography>
    </Paper>
  );
};

@reduxForm({
  form: 'Warning', // a unique identifier for this form
  destroyOnUnmount: false
})
export default class WarningForm extends Component {
  render() {
    const { devices, people, locale } = this.props;
    console.log(people);
    return (
      <Box m={2}>
        <FieldArray
          name="warning"
          devices={devices}
          people={people}
          locale={locale}
          component={RenderWarning}
        />
      </Box>
    );
  }
}
