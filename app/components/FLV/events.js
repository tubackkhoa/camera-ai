type Listener = (...args: any[]) => void;

export default class EventEmitter {
  constructor() {
    this.events = new Map();
  }

  addListener(event: string, listener: Listener): () => void {
    if (!this.events.has(event)) this.events.set(event, []);
    this.events.get(event).push(listener);
  }

  removeListener(event: string, listener: Listener): void {
    const listeners = this.events.get(event);
    if (listeners) {
      const idx: number = listeners.indexOf(listener);
      if (idx > -1) listeners.splice(idx, 1);
    }
  }

  removeAllListeners(event?: string): void {
    if (!event) {
      return this.events.forEach((listeners: Listener[]) =>
        listeners.splice(0)
      );
    }
    // remove all for an event
    const listeners = this.events.get(event);
    if (listeners) {
      listeners.splice(0);
    }
  }

  listenerCount(event) {
    const listeners = this.events.get(event);
    return listeners ? listeners.length : 0;
  }

  emit(event: string, ...args: any[]): void {
    const listeners = this.events.get(event);
    if (listeners) listeners.forEach(listener => listener.apply(this, args));
  }

  once(event: string, listener: Listener): void {
    const handler = (...args: any[]) => {
      this.removeListener(event, handler);
      listener.apply(this, args);
    };
    this.addListener(event, handler);
  }
}

// alias
EventEmitter.prototype.on = EventEmitter.prototype.addListener;
EventEmitter.prototype.off = EventEmitter.prototype.removeListener;
