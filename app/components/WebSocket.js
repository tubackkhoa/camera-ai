import React, { Component } from 'react';

type Props = {
  url: string,
  onMessage: (data: string) => void,
  onOpen: () => void,
  onClose: (code: number, reason: string) => void,
  onError: (e: any) => void,
  debug: boolean,
  reconnect: boolean,
  protocol: ?string,
  reconnectIntervalInMilliSeconds: ?number
};

class WebSocket extends Component<Props> {
  static defaultProps = {
    debug: false,
    reconnect: true
  };

  constructor(props) {
    super(props);
    this.state = {
      ws: window.WebSocket
        ? new window.WebSocket(this.props.url, this.props.protocol)
        : new window.MozWebSocket(this.props.url, this.props.protocol),
      attempts: 1
    };
    this.sendMessage = this.sendMessage.bind(this);
    this.setupWebsocket = this.setupWebsocket.bind(this);
  }

  logging(logline) {
    if (this.props.debug === true) {
      console.log(logline);
    }
  }

  generateInterval(k) {
    if (this.props.reconnectIntervalInMilliSeconds > 0) {
      return this.props.reconnectIntervalInMilliSeconds;
    }
    return Math.min(30, Math.pow(2, k) - 1) * 1000;
  }

  setupWebsocket() {
    const websocket = this.state.ws;

    websocket.onopen = () => {
      this.logging('Websocket connected');
      if (typeof this.props.onOpen === 'function') this.props.onOpen();
    };

    websocket.onerror = e => {
      if (typeof this.props.onError === 'function') this.props.onError(e);
    };

    websocket.onmessage = evt => {
      if (typeof this.props.onMessage === 'function')
        this.props.onMessage(evt.data);
    };

    this.shouldReconnect = this.props.reconnect;
    websocket.onclose = evt => {
      this.logging(
        `Websocket disconnected,the reason: ${evt.reason},the code: ${evt.code}`
      );
      if (typeof this.props.onClose === 'function')
        this.props.onClose(evt.code, evt.reason);
      if (this.shouldReconnect) {
        const time = this.generateInterval(this.state.attempts);
        this.timeoutID = setTimeout(() => {
          this.setState({ attempts: this.state.attempts + 1 });
          this.setState({
            ws: window.WebSocket
              ? new window.WebSocket(this.props.url, this.props.protocol)
              : new window.MozWebSocket(this.props.url, this.props.protocol)
          });
          this.setupWebsocket();
        }, time);
      }
    };
  }

  componentDidMount() {
    this.setupWebsocket();
  }

  componentWillUnmount() {
    this.shouldReconnect = false;
    clearTimeout(this.timeoutID);
    const { ws } = this.state;
    if (ws && (ws.readyState === 0 || ws.readyState === 1)) {
      // CONNECTING || OPEN
      ws.close();
    }
  }

  sendMessage(message) {
    this.state.ws.send(message);
  }

  render() {
    return <></>;
  }
}

export default WebSocket;
