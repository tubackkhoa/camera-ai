/* eslint-disable global-require */

const db = {
  loaded: false,
  tryParse: value => {
    try {
      return JSON.parse(value);
    } catch (ex) {
      console.error('Wrong value', value);
    }

    return null;
  },
  load() {
    db.loaded = true;
  },

  getItem(key, raw) {
    console.log(`Unimplement getItem(${key}, ${raw})`);
  },
  setItem(key, item, raw) {
    console.log(`Unimplement setItem(${key}, ${item}, ${raw})`);
  },
  removeItem(key) {
    console.log(`Unimplement removeItem(${key})`);
  }
};

const updateDb = internalStore => {
  db.getItem = (key, raw = false) => {
    return new Promise(resolve => {
      const value = internalStore.getItem(key);
      resolve(raw ? value : db.tryParse(value));
    });
  };
  db.setItem = (key, item, raw = false) => {
    return new Promise(resolve => {
      resolve(internalStore.setItem(key, raw ? item : JSON.stringify(item)));
    });
  };
  db.removeItem = key => {
    return new Promise(resolve => {
      resolve(internalStore.removeItem(key));
    });
  };
};

if (process.env.TARGET === 'web') {
  updateDb(localStorage);
} else {
  db.load = dbDir => {
    if (db.loaded) return;

    const levelup = require('levelup');
    const leveldown = require('leveldown');
    const internalStore = levelup(leveldown(dbDir));

    db.getItem = (key, raw) => {
      return new Promise(resolve => {
        internalStore.get(key, (err, value) => {
          if (err) resolve(null);
          else resolve(raw ? value : db.tryParse(value));
        });
      });
    };
    db.setItem = (key, item, raw = false) => {
      if (item === undefined) return;
      return internalStore.put(key, raw ? item : JSON.stringify(item));
    };
    db.removeItem = internalStore.del.bind(internalStore);

    db.loaded = true;
  };
}

export default db;
