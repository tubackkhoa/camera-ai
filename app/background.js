/* eslint-disable no-case-declarations */
import { ipcRenderer, remote } from 'electron';
import { API_BASE } from './api/common';

import {
  detectImage,
  predictObjects,
  initModel,
  isInitialized,
  updateFaceMatcher,
  sanitizeFaceMatcher,
  getLabeledFaceDescriptors
} from './utils/faceapi/core';

const parentWindow = remote.getGlobal('mainWindow');

// trigger reload when thing changes
if (module.hot) {
  let timer = null;
  module.hot.accept(err => console.error(err));
  module.hot.dispose(() => {
    // try reload after 2 seconds when there is no more change
    clearTimeout(timer);
    timer = setTimeout(() => window.location.reload(), 2000);
  });
}

// load model
ipcRenderer.once('load-model', (event, modelPath) => {
  // trigger, must pass API_BASE because it is not website
  initModel({ modelPath, API_BASE });
});

// single thread use 1 singleton canvas
const canvas = document.createElement('canvas');
const loadImage = (src, timeout = 15000) =>
  new Promise((resolve, reject) => {
    const img = new Image();
    const timer = setTimeout(
      () => reject(new Error(`Timeout ${timeout}`)),
      timeout
    );
    img.onload = () => {
      clearTimeout(timer);
      canvas.width = img.naturalWidth;
      canvas.height = img.naturalHeight;
      const ctx = canvas.getContext('2d');
      ctx.drawImage(img, 0, 0);
      resolve();
    };
    img.onerror = reject;
    img.src = src;
  });

ipcRenderer.on('assign-task', async (event, task, args, slot) => {
  let ret;

  const start = Date.now();
  // just return null
  if (isInitialized()) {
    try {
      switch (task) {
        case 'detectImage':
        case 'predictObjects':
          await loadImage(args);
          ret =
            task === 'detectImage'
              ? await detectImage(canvas, args)
              : await predictObjects(canvas);
          break;

        case 'sanitizeFaceMatcher':
          // no need to return
          sanitizeFaceMatcher(args);
          break;

        case 'getDescriptors':
          ret = await getLabeledFaceDescriptors(args);
          // update facematcher
          ret.forEach(updateFaceMatcher);
          break;

        default:
          break;
      }
    } catch (ex) {
      console.error(task, 'error', ex);
    }
  }

  // log if open devtools
  if (parentWindow.isDevToolsOpened())
    console.log(task, 'take', Date.now() - start, 'ms', ret);

  // this is like direct event, so no middle man is better, and with args to differ
  parentWindow.webContents.send(`done-task.${task}`, ret, slot, args);
});
