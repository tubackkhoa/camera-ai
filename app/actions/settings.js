export const CHANGE_LOCALE = 'CHANGE_LOCALE';
export const CHANGE_THEME = 'CHANGE_THEME';
// export const LICENSE_UPDATE = 'LICENSE_UPDATE';
export const SET_ITEM = 'SET_ITEM';

export function changeLocale(locale: string) {
  return {
    type: CHANGE_LOCALE,
    payload: locale
  };
}

// primative type
export function setItem(key: string, item: number | boolean | string) {
  return {
    type: SET_ITEM,
    payload: { key, item }
  };
}

export type SetItem = typeof setItem;

export function changeTheme(theme: string) {
  return {
    type: CHANGE_THEME,
    payload: theme
  };
}

// export function setLicense(license) {
//   return {
//     type: LICENSE_UPDATE,
//     payload: license
//   };
// }
