import { post } from '../api';

export const DEVICE_ADD = 'DEVICE_ADD';
export const DEVICE_UPDATE = 'DEVICE_UPDATE';
export const DEVICE_DELETE = 'DEVICE_DELETE';
export const DEVICE_REORDER = 'DEVICE_REORDER';

export function deviceAdd(newDevice) {
  return dispatch => {
    post('/camera/addDevice', newDevice)
      .then(() => {
        return dispatch({ type: DEVICE_ADD, payload: newDevice });
      })
      .catch(console.error);
  };
}

export function deviceUpdate(newDevice, oldDevice) {
  return dispatch => {
    post('/camera/addDevice', newDevice)
      .then(() => {
        return dispatch({
          type: DEVICE_UPDATE,
          payload: [newDevice, oldDevice]
        });
      })
      .catch(console.error);
  };
}

export function deviceDelete(oldDevice) {
  return dispatch => {
    post('/camera/deleteDevice', oldDevice)
      .then(() => {
        return dispatch({
          type: DEVICE_DELETE,
          payload: oldDevice
        });
      })
      .catch(console.error);
  };
}

export function deviceReorder(startIndex, endIndex) {
  return {
    type: DEVICE_REORDER,
    payload: { startIndex, endIndex }
  };
}
