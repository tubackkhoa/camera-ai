import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import type { Store } from '../reducers/types';

export default function configureStore(initialState?: any): Store {
  const enhancer = applyMiddleware(thunk);
  return createStore(rootReducer, initialState, enhancer);
}
