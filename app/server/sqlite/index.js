const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');

class DatabaseSqlite {
  constructor(dbFilePath) {
    if (!fs.existsSync(dbFilePath)) {
      fs.closeSync(fs.openSync(dbFilePath, 'w'));
    }
    this.db = new sqlite3.Database(dbFilePath, sqlite3.OPEN_READWRITE, err => {
      // cần truyền vào một đường dẫn đến file csdl sqlite để khởi tạo một kết nối đến file để bắt đầu đọc ghi
      if (err) {
        console.log('Could not connect to database', err); // Kết nối chưa thành công, có lỗi
      } else {
        console.log('Connected to database'); // Đã kết nối thành công và sẵn sàng để đọc ghi DB
      }
    });
  }

  runquery(sql, params = []) {
    // Hàm do ta tự đặt tên gồm 2 tham số truyền vào.
    return new Promise((resolve, reject) => {
      // Tạo mới một Promise thực thi câu lệnh sql
      this.db.run(sql, params, err => {
        // this.db sẽ là biến đã kết nối csdl, ta gọi hàm run của this.db chính là gọi hàm run của sqlite3 trong NodeJS hỗ trợ (1 trong 3 hàm như đã nói ở trên)
        if (err) {
          // Trường hợp lỗi
          console.log(`Error running sql ${sql}`);
          reject(err);
        } else {
          // Trường hợp chạy query thành công
          resolve({ id: this.lastID }); // Trả về kết quả là một object có id lấy từ DB.
        }
      });
    });
  }

  get(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.get(sql, params, (err, result) => {
        if (err) {
          console.log(`Error running sql: ${sql}`);
          console.log(err);
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  all(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.all(sql, params, (err, rows) => {
        if (err) {
          console.log(`Error running sql: ${sql}`);
          console.log(err);
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }
}

module.exports = DatabaseSqlite;
