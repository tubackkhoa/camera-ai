const crypto = require('crypto');
const { SUCCESS } = require('../status');
const utils = require('./utils');
const logger = require('../logger');

class LicenseKey {
  constructor(config) {
    this.config = config;

    this.PrivateKey = crypto.createPrivateKey({
      key: config.rsa_private_key,
      passphrase: config.rsa_passphrase
    });

    this.PublicKey = crypto.createPublicKey(this.PrivateKey);
  }

  generateLicense(key, machine) {
    const buf = Buffer.from(
      JSON.stringify({
        identity: this.config.identity,
        machine,
        key,
        meta: this.validate(key)
      }),
      'utf8'
    );
    const license = utils.crypt(this.PrivateKey, buf, true);
    return license.toString('hex');
  }

  validate(key) {
    const buf = Buffer.from(key, 'hex');
    try {
      const bufData = utils.crypt(this.PublicKey, buf, false);
      const data = JSON.parse(bufData.toString('utf8'));
      if (data.identity === this.config.identity) {
        if (data.persist === 1) return data;
        if (data.startDate < Date.now() && data.endDate > Date.now())
          return data;
      }
      logger.log(`Encountered invalid key ${bufData}`);
    } catch (e) {
      logger.error(e.toString());
    }
  }

  issue(options = {}) {
    const meta = {
      identity: options.identity || this.config.identity,
      persist: options.persist ? 1 : 0,
      startDate: options.startDate || Date.now(),
      endDate: options.endDate || Date.now() + this.config.expireAfter,
      issueDate: Date.now()
    };
    const buf = Buffer.from(JSON.stringify(meta), 'utf8');
    const key = utils.crypt(this.PrivateKey, buf, true).toString('hex');

    return { status: SUCCESS, key };
  }
}

module.exports = LicenseKey;
