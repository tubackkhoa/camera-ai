const md = require('machine-digest');
// const path = require('path');
const LicenseKey = require('./model');
const utils = require('./utils');

const { SUCCESS, INVALID_INPUT } = require('../status');

const model = new LicenseKey({
  name: 'license-server',
  identity: 'CameraAI', // client software identity
  expireAfter: 365 * 24 * 60 * 60 * 1000, // 1 year
  rsa_private_key: `-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,5B1EF7CD00F44A63

zGBZtJInb3NiJ2+2lAdLwcp0lcWe57GjeMC9YkIFNTL9HbS/3YtsKbhT63acfkDQ
jvThLIo2p7Gs0xABSlsRZ4S2VOnxQ2FuK1B2RXgQurKYLJdH/jDLnaBYQlvddPLw
jEAsLnsgDBI/W5HF6Dmh5CgCQjt1vuW2J6yuxmZDkxsQ1q0Tdu2I9Cnq8htNTxsl
d2DBm/4XPYvjXyvG8GqOOoNSQdJrtlTVQZfV9PmN0joVDeK010op+aHJcteZ7LuK
IxeyKyRdcDXeCH3YQBZSOHPOIQG3jaj1ieUZ5D02ak4wlHcR3SQHBN3BfC0BoDAh
jYX2TEb400unLNMZIDryJb+mh3QezPQZ38lIxrMWFD+lykkZX7fx8tW16S7dVO88
snejaJzIQji+wFz1OCP7LAHTdhISN/AxYJLno4zBl2YUJuj2ubyJO5O4cekyeU9y
eHxkwq/LVKP5CHON2WoqzRQ8GDdod59epZjw77sok1M2WnVU36CgMP/VLgAjXBid
ZiGA8FFXHfx3sVzObf9MGjpTi3h02JLXwvpVnTAYyoPv54xgE/4C+Y/myYVWM1zm
oa2/HR19xeUqPpY5JG8LP1RIE2WNmi0I1NR7/XOGhWbVHXWtDjkry9LK5Zu5JdcA
rnt07AmOtFQn6FP/5eTGNBj3CpHGEXTMdALuXwe77/ZtgmvOosS4X6zBp08tkp0D
Ap1c2WLy86ysVo1rwXtHlrKRdAqDOYSih6ldMqlUSEfjnsJtjNtPWzHuNsIga+fu
XKYWYWppoI/5mO2AebBO9V9NFyyIBJ2dFx0Kbk1BSRN/Erkw4/Ralw==
-----END RSA PRIVATE KEY-----`,
  rsa_passphrase: '1234'
});

const handler = {
  handleLicense({ key, id: machine }) {
    const data = model.validate(key);
    if (!data) return { status: INVALID_INPUT };

    const license = model.generateLicense(key, machine);
    return { status: SUCCESS, license };
  },

  authorize(licenseKey, machineId) {
    const key = licenseKey.substr(0, 512);
    const buf = Buffer.from(licenseKey.substr(512), 'hex');
    const license = JSON.parse(
      utils.crypt(model.PublicKey, buf, false).toString()
    );
    console.log(license);
    if (
      license.key === key &&
      license.machine === machineId &&
      license.identity === model.config.identity
    ) {
      if (
        license.meta.persist ||
        (license.meta.startDate < Date.now() &&
          license.meta.endDate > Date.now())
      ) {
        return true;
      }
      throw Error('invalid effect date of license');
    } else throw Error('invalid license');
  },

  issue(options = {}) {
    return model.issue(options);
  }
};

const checkLicense = () => {
  // load licenseKey from somewhere
  let ret = handler.issue({
    startDate: 1562040194050,
    endDate: 1908936264015,
    identity: 'CameraAI'
  });
  if (ret.status === SUCCESS) {
    const { key } = ret;
    // get machine id
    const machineId = process.argv[2] || md.get().digest;

    ret = handler.handleLicense({ key, id: machineId });
    if (ret.status === SUCCESS) {
      const licenseKey = key + ret.license;
      console.log('MachineId', machineId, '\nLicence: ', licenseKey);
    }
  }
};

checkLicense();

// const authorizeLicense = (machineId, licenseKey) => {
//   try {
//     handler.authorize(licenseKey, machineId);
//     console.log('Key is valid');
//   } catch (ex) {
//     console.error(ex);
//   }
// };

// authorizeLicense(
//   'b793a25deb1fbb5fb5920919dee4127ec41f8a6d6612af07b3b54ce57d6af0f8',
//   '9d6c7461f4d69661f3a9af0b0a11a68f7a0734565806de8af94d5b4ef3a5774665f616c4e1e110fae9fa224fc38113ec273afddf8049fb1a312fd165c115798a0b957ca4bdf2698a52a9553b307b1ee19edc4f1d8995ffc3094d516c77dab2f4c63fc0afd92c706d9cdaf0fb85a64fc8a739ddd997227f7ea76bf923e974013126a757aa7c6f6e4ae2bdd1e29c60f28ec0ea83c9e8a38d0d32600f901423c2846db204865e35f63f9b0e21890c195be1087394a4802e67d93d91ddaaec4ae42668410c8a59650548e1db305df1aaf9b533c585d46d485ab1c7a02c2f935cd831626c1674d578c9d12db84248ab958249738da69c11c7f3e22dc85a7a8afc5beb514e700367c48ce7e3131cadd7591d33194323e36a2a625cb589a05348e4728fab45e82973a73e712cda373536ecfb4bac04e01ba6b64b35ca52bdbd9d8e652ffedbec7c7079e0e374e68de25752fe52abe2516a5359c96eaa2b9a2899034465376463f7ac6ecae74cd19597d193dbe855262da325b3605edc327f996565d6644f5fb107bf6677d9a3aa5edbd75a363ef72e3a34b0ef0b233df7323ba620eb80f505fa9118bb56808eb8078f0214b5bf1ef60c604f60d1a1ef70f057f0ab27f4272b6476e492484e28d30d1ca1758477650ac88305e4c20d9908d7f70b965f97ce3a7175426014d80fab98e55c2ae606d41fc4a74f52265841e665cfc1107b8e7be1d33c353409b6dda5747b1835849b5f940257e6f4c8dc48ecc8ac0bd1fa25efd6c6c26fb1efa7788ab2aa3b65137030a9353c08f7551ab4d3f5767348a1834ee452852244e072536de0fb778e66433321d51d0e0b46a6f19f33d46b5507922853b8b697a0094caf628627a6ddca5de40a24f5e932beb75bb852637d66e4a52fe659214a49b71a4a2cc234ee13919e26667b5f2732d3e9d814f5139698a95e1e22ade92f102ca2fe08c835279be30550badaecee66bbf19cad17d608889c4f357adfdcae6d07d04f76b8ae8cc8a60688c158c41371cf72b6964a464e75ccfd41f3a3c36587c40b85ceb69c94ad09bccf735ed32bbcc67deb9f75f55c9df06c6b329fe0efad4315c1639dbc41023a622f812abc4bc11fecb764e21c123dee769cd8d5a58e222dcb866a6fcda6abc16663954f48efad3d7a1c85f349311a212d1c39af81ef4ccd60a5bc9c427ef4ecb35d4b70fbbbc1c65948f4fa7cdcc99fb82f6119d67714e7a964a85cac58a983d1dd159304f65a82706090be77e1c7d9a680a992d5f024ff01c0952d1fb88fcadb705bff271546141b55529ced38d65a2c9b88d4fd3a98455de0a1c509fe5531f8c71f930a999fa356d595d9eb54997b4d281b696ae50220bd90cb74e634006b073923c556df330322a19e9e94375a72f289c30e7dabf3713e253149deb3a604eafbf35bcddcf7b17b7401f5fe0a15c2b02d916a6b6115aa3bf6525e5369e8573c6a7297cd2ba7f94c477e2cc929067076c64663e11df063930fb5e810d41e604a7129278ce42ba384e39aed6cd0ba0c698fecf1eebfa99e0c5eacda4f6bd91e7e28890fde20e2e3da1d0706ae276098492ddb4ddfa380db6241d5c63a9f5d6a81f68e8b11672b8939662bc5ddfa5f452e595f7e288121539e03328051a5502ec22547961dec96cfbb080d2e9f27985cedc60933815f50bc8abcab0301a3778522474b48eb10812cb747411356285e2860e1e105ca8d2cf6804b930864398125118ecb6f9deb527e32054e988c3ebf573a81c113d3b9afabf5211883fcdb40c53942340dd8997277f32d03ddafea3dbd3c80d3a543ade25aaaf8a2cabedf91e212e8653ab309b2a5f7e8a3704d6899e0be3b80bde5384541a3c8217dc75101cf7a11314612c93397df09a488866c6c9b719f2c537cbcc93d4af0b6dbc2b663d721cd5681fb5034335f2a6b1c9e8002674c62e2ed8f8c62eb8c1d164c23b49a5ac4b69101815ac10197577617f4f2e297fb'
// );
