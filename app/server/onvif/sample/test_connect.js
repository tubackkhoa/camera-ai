/* eslint-disable no-await-in-loop */
const Onvif = require('../lib/node-onvif');

const onvif = new Onvif();

const getSize = resolution => resolution.width * resolution.height;

// Create an OnvifDevice object
const device = new onvif.OnvifDevice({
  xaddr: 'http://192.168.1.111:8899/onvif/device_service',
  user: 'admin',
  pass: ''
});

const run = async () => {
  // Initialize the OnvifDevice object
  try {
    const info = await device.init();
    // info = device.changeProfile('001');
    console.log(info);

    // const users = await device.services.device.getUsers();
    // console.log(JSON.stringify(users, null, 2));

    // const getSize = resolution => resolution.width * resolution.height;

    const profiles = device
      .getProfileList()
      .map(item => ({
        stream: item.stream.rtsp,
        snapshot: item.snapshot,
        size: getSize(item.video.encoder.resolution)
      }))
      .sort((a, b) => b.size - a.size);
    console.log(profiles);
  } catch (error) {
    console.error(error);
  }

  // await stop();
};

// const stop = async () => {
//   while (global.running) {
//     await new Promise(resolve => setTimeout(resolve, 1000));
//   }

//   return true;
// };

// const createProfile = async () => {
//   try {
//     await device.init();
//     const result = await device.services.media.createProfile({
//       Name: 'SubStream',
//       Token: 'SubStream'
//     });
//     console.log(result);
//   } catch (ex) {
//     console.error(ex);
//   }
// };

run();
