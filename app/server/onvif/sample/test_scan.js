const onvif = require('../index');

// const run = async () => {
//   const device_list = await onvif.startProbe();
//   const ret = device_list.map(device => ({
//     url: new URL(device.xaddrs[0]).toString(),
//     name: device.name
//   }));

//   console.log(ret);
// };

// run();

onvif
  .startScan({
    target: '192.168.1.2-255',
    port: '80,8899'
  })
  .then(data => console.log('data', data));

// setTimeout(() => onvif.stopScan(), 1000);
