/* eslint-disable no-empty */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-cond-assign */
/* eslint-disable func-names */
/* eslint-disable camelcase */
/* eslint-disable no-underscore-dangle */
/* ------------------------------------------------------------------
 * node-onvif - service-device.js
 *
 * Copyright (c) 2016, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2016-10-02
 * ---------------------------------------------------------------- */

const mOnvifSoap = require('./soap.js');
const OnvifServiceBase = require('./service-base');
const { parseGetSystemDateAndTime } = require('./utils');

/* ------------------------------------------------------------------
 * Constructor: OnvifServiceDevice(params)
 * - params:
 *    - xaddr   : URL of the entry point for the device management service
 *                (Required)
 *    - user  : User name (Optional)
 *    - pass  : Password (Optional)
 * ---------------------------------------------------------------- */
class OnvifServiceDevice extends OnvifServiceBase {
  constructor(params) {
    super(params);

    this.name_space_method = 'tds';
    this.time_diff = 0;
    this.name_space_attr_list = [
      'xmlns:tds="http://www.onvif.org/ver10/device/wsdl"',
      'xmlns:tt="http://www.onvif.org/ver10/schema"'
    ];
  }

  /* ------------------------------------------------------------------
   * Method: getCapabilities([callback])
   * ---------------------------------------------------------------- */
  getCapabilities() {
    return this._createMethodRequestSoap(
      'GetCapabilities',
      '<tds:Category>All</tds:Category>'
    );
  }

  /* ------------------------------------------------------------------
   * Method: getWsdlUrl([callback])
   * ---------------------------------------------------------------- */
  getWsdlUrl() {
    return this._createMethodRequestSoap('GetWsdlUrl');
  }

  /* ------------------------------------------------------------------
   * Method: getDiscoveryMode(callback)
   * ---------------------------------------------------------------- */
  getDiscoveryMode() {
    return this._createMethodRequestSoap('GetDiscoveryMode');
  }

  /* ------------------------------------------------------------------
   * Method: getScopes([callback])
   * ---------------------------------------------------------------- */
  getScopes() {
    return this._createMethodRequestSoap('GetScopes');
  }

  /* ------------------------------------------------------------------
   * Method: setScopes(params[, callback])
   * - params:
   *   - Scopes  | Array  | required | a list of URI
   *
   * {
   *   'Scopes': [
   *     'onvif://www.onvif.org/location/town/Nerima',
   *     'onvif://www.onvif.org/location/city/Tokyo'
   *   ]
   * }
   *
   * If you want to delete all Configurable scopes, specify an empty
   * Array object as the `Scope` property.
   *
   * {'Scopes': []}
   * ---------------------------------------------------------------- */
  setScopes(params) {
    let err_msg = '';

    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    const scope_list = params.Scopes;
    if ((err_msg = mOnvifSoap.isInvalidValue(scope_list, 'array', true))) {
      return Promise.reject(
        new Error(`The "Scopes" property was invalid: ${err_msg}`)
      );
    }

    const body = scope_list.map(s => `<tds:Scopes>${s}</tds:Scopes>`).join('');

    return this._createMethodRequestSoap('SetScopes', body);
  }

  /* ------------------------------------------------------------------
   * Method: addScopes(params[, callback])
   * - params:
   *   - Scopes  | Array  | required | a list of URI
   *
   * {
   *   'Scopes': [
   *     'onvif://www.onvif.org/location/town/Nerima',
   *     'onvif://www.onvif.org/location/city/Tokyo'
   *   ]
   * }
   * ---------------------------------------------------------------- */
  addScopes(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    const scope_list = params.Scopes;
    if ((err_msg = mOnvifSoap.isInvalidValue(scope_list, 'array', true))) {
      return Promise.reject(
        new Error(`The "Scopes" property was invalid: ${err_msg}`)
      );
    }

    const body = scope_list
      .map(s => `<tds:ScopeItem>${s}</tds:ScopeItem>`)
      .join('');

    return this._createMethodRequestSoap('AddScopes', body);
  }

  /* ------------------------------------------------------------------
   * Method: removeScopes(params[, callback])
   * - params:
   *   - Scopes  | Array  | required | a list of URI
   *
   * {
   *   'Scopes': [
   *     'onvif://www.onvif.org/location/town/Nerima',
   *     'onvif://www.onvif.org/location/city/Tokyo'
   *   ]
   * }
   * ---------------------------------------------------------------- */
  removeScopes(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    const scope_list = params.Scopes;
    if ((err_msg = mOnvifSoap.isInvalidValue(scope_list, 'array'))) {
      return Promise.reject(
        new Error(`The "Scopes" property was invalid: ${err_msg}`)
      );
    }

    const body = scope_list
      .map(s => `<tds:ScopeItem>${s}</tds:ScopeItem>`)
      .join('');

    return this._createMethodRequestSoap('RemoveScopes', body);
  }

  /* ------------------------------------------------------------------
   * Method: getHostname([callback])
   * ---------------------------------------------------------------- */
  getHostname() {
    return this._createMethodRequestSoap('GetHostname');
  }

  /* ------------------------------------------------------------------
   * Method: setHostname(params[, callback])
   * - params:
   *   - Name  | string  | required | a host name
   *
   * {'Name': 'test'}
   * ---------------------------------------------------------------- */
  setHostname(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    const hostname = params.Name;
    if ((err_msg = mOnvifSoap.isInvalidValue(hostname, 'string'))) {
      return Promise.reject(
        new Error(`The "Name" property was invalid: ${err_msg}`)
      );
    }

    const body = `<tds:Name>${hostname}</tds:Name>`;
    return this._createMethodRequestSoap('SetHostname', body);
  }

  /* ------------------------------------------------------------------
   * Method: getDNS([callback])
   * ---------------------------------------------------------------- */
  getDNS() {
    return this._createMethodRequestSoap('GetDNS').then(result => {
      try {
        const di = result.data.DNSInformation;
        if (!di.SearchDomain) {
          di.SearchDomain = [];
        } else if (!Array.isArray(di.SearchDomain)) {
          di.SearchDomain = [di.SearchDomain];
        }
        if (!di.DNSManual) {
          di.DNSManual = [];
        } else if (!Array.isArray(di.DNSManual)) {
          di.DNSManual = [di.DNSManual];
        }
        result.data = di;
      } catch {}
      return result;
    });
  }

  /* ------------------------------------------------------------------
   * Method: setDNS(params[, callback])
   * - params:
   *   - FromDHCP      | boolean | required | true or false
   *   - SearchDomain  | Array   | optional | a list of search domains
   *   - DNSManual     | Array   | optional | a list of DNS addresses
   *     - Type        | String  | required | "IPv4" or "IPv6"
   *     - IPv4Address | String  | optional | IPv4 address
   *     - IPv6Address | String  | optional | IPv6 address
   *
   * {
   *   'FromDHCP'    : false,
   *   'SearchDomain': ['futomi.gr.jp', 'hatano.gr.jp'],
   *   'DNSManual'   : [
   *     {'Type': 'IPv4', 'IPv4Address': '192.168.10.1'},
   *     {'Type': 'IPv4', 'IPv4Address': '192.168.10.2'}
   *   ]
   * }
   * ---------------------------------------------------------------- */
  setDNS(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.FromDHCP, 'boolean'))) {
      return Promise.reject(
        new Error(`The "FromDHCP" property was invalid: ${err_msg}`)
      );
    }

    if ('SearchDomain' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(
          params.SearchDomain,
          'array',
          true
        ))
      ) {
        return Promise.reject(
          new Error(`The "SearchDomain" property was invalid: ${err_msg}`)
        );
      }
      for (let i = 0; i < params.SearchDomain.length; i++) {
        if (
          (err_msg = mOnvifSoap.isInvalidValue(
            params.SearchDomain[i],
            'string'
          ))
        ) {
          return Promise.reject(
            new Error(`The "SearchDomain" property was invalid: ${err_msg}`)
          );
        }
      }
    }

    if ('DNSManual' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(params.DNSManual, 'array', true))
      ) {
        return Promise.reject(
          new Error(`The "DNSManual" property was invalid: ${err_msg}`)
        );
      }

      for (let i = 0; i < params.DNSManual.length; i++) {
        const o = params.DNSManual[i];
        if ((err_msg = mOnvifSoap.isInvalidValue(o, 'object'))) {
          return Promise.reject(
            new Error(`The "DNSManual" property was invalid: ${err_msg}`)
          );
        }

        const type = o.Type;
        if ((err_msg = mOnvifSoap.isInvalidValue(type, 'string'))) {
          return Promise.reject(
            new Error(`The "Type" property was invalid: ${err_msg}`)
          );
        }
        if (!type.match(/^(IPv4|IPv6)$/)) {
          return Promise.reject(
            new Error(
              'The "Type" property was invalid: The value must be either "IPv4" or "IPv6".'
            )
          );
        }

        if (type === 'IPv4') {
          if ((err_msg = mOnvifSoap.isInvalidValue(o.IPv4Address, 'string'))) {
            return Promise.reject(
              new Error(`The "IPv4Address" property was invalid: ${err_msg}`)
            );
          }
        } else if (type === 'IPv6') {
          if ((err_msg = mOnvifSoap.isInvalidValue(o.IPv6Address, 'string'))) {
            return Promise.reject(
              new Error(`The "IPv6Address" property was invalid: ${err_msg}`)
            );
          }
        }
      }
    }

    let body = '';

    if ('FromDHCP' in params) {
      body += `<tds:FromDHCP>${params.FromDHCP}</tds:FromDHCP>`;
    }
    if ('SearchDomain' in params) {
      params.SearchDomain.forEach(s => {
        body += `<tds:SearchDomain>${s}</tds:SearchDomain>`;
      });
    }
    if ('DNSManual' in params) {
      if (params.DNSManual.length === 0) {
        body += '<tds:DNSManual></tds:DNSManual>';
      } else {
        params.DNSManual.forEach(o => {
          body += '<tds:DNSManual>';
          body += `<tt:Type>${o.Type}</tt:Type>`;
          if (o.Type === 'IPv4') {
            body += `<tt:IPv4Address>${o.IPv4Address}</tt:IPv4Address>`;
          } else {
            body += `<tt:IPv6Address>${o.IPv6Address}</tt:IPv6Address>`;
          }
          body += '</tds:DNSManual>';
        });
      }
    }

    return this._createMethodRequestSoap('SetDNS', body);
  }

  /* ------------------------------------------------------------------
   * Method: getNetworkInterfaces([callback])
   * ---------------------------------------------------------------- */
  getNetworkInterfaces() {
    return this._createMethodRequestSoap('GetNetworkInterfaces');
  }

  /* ------------------------------------------------------------------
   * Method: getNetworkProtocols([callback])
   * ---------------------------------------------------------------- */
  getNetworkProtocols() {
    return this._createMethodRequestSoap('GetNetworkProtocols');
  }

  /* ------------------------------------------------------------------
   * Method: setNetworkProtocols(params[, callback])
   * - params:
   *   - NetworkProtocols | Array   | required | a list of protocols
   *     - Name           | String  | required |
   *     - Enabled        | Boolean | optional |
   *     - Port           | Integer | optional |
   *
   * {
   *   'NetworkProtocols': [
   *     {'Name': 'HTTP', 'Enabled': true, 'Port': 80},
   *     {'Name': 'RTSP', 'Enabled': false, 'Port': 554},
   *   ]
   * }
   *
   * ---------------------------------------------------------------- */
  setNetworkProtocols(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(
        params.NetworkProtocols,
        'array',
        true
      ))
    ) {
      return Promise.reject(
        new Error(`The "NetworkProtocols" property was invalid: ${err_msg}`)
      );
    }

    for (let i = 0; i < params.NetworkProtocols.length; i++) {
      const o = params.NetworkProtocols[i];
      if ((err_msg = mOnvifSoap.isInvalidValue(o, 'object'))) {
        return Promise.reject(
          new Error(`The "NetworkProtocols" property was invalid: ${err_msg}`)
        );
      }

      if ((err_msg = mOnvifSoap.isInvalidValue(o.Name, 'string'))) {
        return Promise.reject(
          new Error(`The "Name" property was invalid: ${err_msg}`)
        );
      }

      if (!o.Name.match(/^(HTTP|HTTPS|RTSP)$/)) {
        return Promise.reject(
          new Error(
            'The "Name" property was invalid: It must be "HTTP", "HTTPS", or "RTSP".'
          )
        );
      }

      let flag = false;

      if ('Enabled' in o) {
        if ((err_msg = mOnvifSoap.isInvalidValue(o.Enabled, 'boolean'))) {
          return Promise.reject(
            new Error(`The "Enabled" property was invalid: ${err_msg}`)
          );
        }
        flag = true;
      }

      if ('Port' in o) {
        if ((err_msg = mOnvifSoap.isInvalidValue(o.Port, 'integer'))) {
          return Promise.reject(
            new Error(`The "Port" property was invalid: ${err_msg}`)
          );
        }
        flag = true;
      }

      if (flag === false) {
        return Promise.reject(
          new Error('Either "Enabled" or "Port" property is required.')
        );
      }
    }

    let body = '';

    params.NetworkProtocols.forEach(o => {
      body += '<tds:NetworkProtocols>';
      for (const k in o) {
        if (k.match(/^(Name|Enabled|Port)$/)) {
          body += `<tt:${k}>${o[k]}</tt:${k}>`;
        }
      }
      body += '</tds:NetworkProtocols>';
    });

    return this._createMethodRequestSoap('SetNetworkProtocols', body);
  }

  /* ------------------------------------------------------------------
   * Method: getNetworkDefaultGateway([callback])
   * ---------------------------------------------------------------- */
  getNetworkDefaultGateway() {
    return this._createMethodRequestSoap('GetNetworkDefaultGateway');
  }

  /* ------------------------------------------------------------------
   * Method: setNetworkDefaultGateway(params[, callback])
   * - params:
   *   - NetworkGateway | Array | required | a list of IP addresses of gateways
   *
   * {
   *   'NetworkGateway': [
   *     {'IPv4Address': '192.168.10.1'},
   *     {'IPv4Address': '192.168.10.2'}
   *   ]
   * }
   *
   * ---------------------------------------------------------------- */
  setNetworkDefaultGateway(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(
        params.NetworkGateway,
        'array',
        true
      ))
    ) {
      return Promise.reject(
        new Error(`The "NetworkGateway" property was invalid: ${err_msg}`)
      );
    }

    for (let i = 0; i < params.NetworkGateway.length; i++) {
      const o = params.NetworkGateway[i];
      if ('IPv4Address' in o) {
        if ((err_msg = mOnvifSoap.isInvalidValue(o.IPv4Address, 'string'))) {
          return Promise.reject(
            new Error(`The "IPv4Address" property was invalid: ${err_msg}`)
          );
        }
      }
      if ('IPv6Address' in o) {
        if ((err_msg = mOnvifSoap.isInvalidValue(o.IPv6Address, 'string'))) {
          return Promise.reject(
            new Error(`The "IPv6Address" property was invalid: ${err_msg}`)
          );
        }
      }
      if (!('IPv4Address' in o) && !('IPv6Address' in o)) {
        return Promise.reject(
          new Error(
            'Either "IPv4Address" or "IPv6Address" property must be specified.'
          )
        );
      }
    }

    let body = '';

    params.NetworkGateway.forEach(o => {
      for (const k in o) {
        if (k.match(/^(IPv4Address|IPv6Address)$/)) {
          body += `<tds:${k}>${o[k]}</tds:${k}>`;
        }
      }
    });

    return this._createMethodRequestSoap('SetNetworkDefaultGateway', body);
  }

  /* ------------------------------------------------------------------
   * Method: getDeviceInformation([callback])
   * ---------------------------------------------------------------- */
  getDeviceInformation() {
    return this._createMethodRequestSoap('GetDeviceInformation');
  }

  /* ------------------------------------------------------------------
   * Method: getSystemDateAndTime([callback])
   * ---------------------------------------------------------------- */
  getSystemDateAndTime() {
    return this._createMethodRequestSoap('GetSystemDateAndTime').then(
      result => {
        const parsed = parseGetSystemDateAndTime(result.converted);
        if (parsed && parsed.date) {
          const device_time = parsed.date.getTime();
          const my_time = new Date().getTime();
          this.time_diff = device_time - my_time;
        }
        return result;
      }
    );
  }

  /* ------------------------------------------------------------------
   * Method: setSystemDateAndTime(params[, callback])
   * - params:
   *   - DateTimeType    | string  | required | "NTP" or "Manual".
   *   - DaylightSavings | boolean | required | true or false.
   *   - TimeZone        | string  | optional | e.g., "EST5EDT", "GMT0", "JST-9".
   *   - UTCDateTime     | Date    | optional | A Date object of ECMAScript.
   *
   * Setting the "UTCDateTime" does not work well for now.
   * ---------------------------------------------------------------- */
  setSystemDateAndTime(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.DateTimeType, 'string'))) {
      return Promise.reject(
        new Error(`The "DateTimeType" property was invalid: ${err_msg}`)
      );
    }
    if (!params.DateTimeType.match(/^(NTP|Manual)$/)) {
      return Promise.reject(
        new Error(
          'The "DateTimeType" property must be either "NTP" or "Manual".'
        )
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.DaylightSavings, 'boolean'))
    ) {
      return Promise.reject(
        new Error(`The "DaylightSavings" property was invalid: ${err_msg}`)
      );
    }

    if ('TimeZone' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.TimeZone, 'string'))) {
        return Promise.reject(
          new Error(`The "TimeZone" property was invalid: ${err_msg}`)
        );
      }

      if (!params.TimeZone.match(/^[A-Z]{3}-?\d{1,2}([A-Z]{3,4})?$/)) {
        return Promise.reject(
          new Error(
            'The "TimeZone" property must be a string representing a time zone which is defined in POSIX 1003.1.'
          )
        );
      }
    }

    if ('UTCDateTime' in params) {
      const v = params.UTCDateTime;
      if (!(v instanceof Date)) {
        return Promise.reject(
          new Error(
            'The "UTCDateTime" property must be a Date object of ECMAScript.'
          )
        );
      }
    }

    let body = '';

    body += `<tds:DateTimeType>${params.DateTimeType}</tds:DateTimeType>`;
    body += `<tds:DaylightSavings>${params.DaylightSavings}</tds:DaylightSavings>`;
    if (params.TimeZone) {
      body += '<tds:TimeZone>';
      body += `<tt:TZ>${params.TimeZone}</tt:TZ>`;
      body += '</tds:TimeZone>';
    }
    if (params.UTCDateTime) {
      const dt = params.UTCDateTime;
      body += '<tds:UTCDateTime>';
      body += '<tt:Time>';
      body += `<tt:Hour>${dt.getUTCHours()}</tt:Hour>`;
      body += `<tt:Minute>${dt.getUTCMinutes()}</tt:Minute>`;
      body += `<tt:Second>${dt.getUTCSeconds()}</tt:Second>`;
      body += '</tt:Time>';
      body += '<tt:Date>';
      body += `<tt:Year>${dt.getUTCFullYear()}</tt:Year>`;
      body += `<tt:Month>${dt.getUTCMonth() + 1}</tt:Month>`;
      body += `<tt:Day>${dt.getUTCDate()}</tt:Day>`;
      body += '</tt:Date>';
      body += '</tds:UTCDateTime>';
    }

    return this._createMethodRequestSoap('SetSystemDateAndTime', body);
  }

  /* ------------------------------------------------------------------
   * Method: reboot([callback])
   * ---------------------------------------------------------------- */
  reboot() {
    return this._createMethodRequestSoap('SystemReboot');
  }

  /* ------------------------------------------------------------------
   * Method: getUsers([callback])
   * ---------------------------------------------------------------- */
  getUsers() {
    return this._createMethodRequestSoap('GetUsers').then(result => {
      try {
        const d = result.data.GetUsersResponse.User;
        if (!Array.isArray(d)) {
          result.data.GetUsersResponse.User = [d];
        }
      } catch {}
      return result;
    });
  }

  /* ------------------------------------------------------------------
   * Method: createUsers(params[, callback])
   * - params:
   *   - User        | Array  | required |
   *     - Username  | string | required | Username
   *     - Password  | string | required | Password
   *     - UserLevel | string | required | Either "Administrator", "Operator", "User", or "Anonymous"
   *
   * {
   *   'User' : [
   *     {'Username': 'test1', 'Password' : 'password', 'UserLevel': 'Administrator'},
   *     {'Username': 'test2', 'Password' : 'password', 'UserLevel': 'Operator'},
   *     {'Username': 'test3', 'Password' : 'password', 'UserLevel': 'User'}
   *   ]
   * }
   * ---------------------------------------------------------------- */
  createUsers(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.User, 'array'))) {
      return Promise.reject(
        new Error(`The "User" property was invalid: ${err_msg}`)
      );
    }

    for (let i = 0; i < params.User.length; i++) {
      const u = params.User[i];
      if ((err_msg = mOnvifSoap.isInvalidValue(u, 'object'))) {
        return Promise.reject(
          new Error(`The "User" property was invalid: ${err_msg}`)
        );
      }

      if ((err_msg = mOnvifSoap.isInvalidValue(u.Username, 'string'))) {
        return Promise.reject(
          new Error(`The "Username" property was invalid: ${err_msg}`)
        );
      }

      if ((err_msg = mOnvifSoap.isInvalidValue(u.Password, 'string'))) {
        return Promise.reject(
          new Error(`The "Password" property was invalid: ${err_msg}`)
        );
      }

      if ((err_msg = mOnvifSoap.isInvalidValue(u.UserLevel, 'string'))) {
        return Promise.reject(
          new Error(`The "UserLevel" property was invalid: ${err_msg}`)
        );
      }
      if (!u.UserLevel.match(/^(Administrator|Operator|User|Anonymous)$/)) {
        return Promise.reject(
          new Error(
            'The "UserLevel" property must be either "Administrator", "Operator", "User", or "Anonymous".'
          )
        );
      }
    }

    let body = '';

    params.User.forEach(u => {
      body += '<tds:User>';
      body += `<tt:Username>${u.Username}</tt:Username>`;
      body += `<tt:Password>${u.Password}</tt:Password>`;
      body += `<tt:UserLevel>${u.UserLevel}</tt:UserLevel>`;
      body += '</tds:User>';
    });

    return this._createMethodRequestSoap('CreateUsers', body);
  }

  /* ------------------------------------------------------------------
   * Method: deleteUsers(params[, callback])
   * - params:
   *   - User        | Array  | required |
   *     - Username  | string | required | Username
   *
   * {
   *   'User' : [
   *     {'Username': 'test1'},
   *     {'Username': 'test2'},
   *     {'Username': 'test3'}
   *   ]
   * }
   * ---------------------------------------------------------------- */
  deleteUsers(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.User, 'array'))) {
      return Promise.reject(
        new Error(`The "User" property was invalid: ${err_msg}`)
      );
    }

    for (let i = 0; i < params.User.length; i++) {
      const u = params.User[i];
      if ((err_msg = mOnvifSoap.isInvalidValue(u, 'object'))) {
        return Promise.reject(
          new Error(`The "User" property was invalid: ${err_msg}`)
        );
      }

      if ((err_msg = mOnvifSoap.isInvalidValue(u.Username, 'string'))) {
        return Promise.reject(
          new Error(`The "Username" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    params.User.forEach(u => {
      // soap_body += '<tds:User>';
      body += `<tt:Username>${u.Username}</tt:Username>`;
      // soap_body += '</tds:User>';
    });

    return this._createMethodRequestSoap('DeleteUsers', body);
  }

  /* ------------------------------------------------------------------
   * Method: setUser(params[, callback])
   * - params:
   *   - User        | Array  | required |
   *     - Username  | string | required | Username
   *     - Password  | string | optional | Password
   *     - UserLevel | string | optional | Either "Administrator", "Operator", "User", or "Anonymous"
   *
   * {
   *   'User' : [
   *     {'Username': 'test1', 'Password' : 'password', 'UserLevel': 'Administrator'},
   *     {'Username': 'test2', 'Password' : 'password', 'UserLevel': 'Operator'},
   *     {'Username': 'test3', 'Password' : 'password', 'UserLevel': 'User'}
   *   ]
   * }
   * ---------------------------------------------------------------- */
  setUser(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.User, 'array'))) {
      return Promise.reject(
        new Error(`The "User" property was invalid: ${err_msg}`)
      );
    }

    for (let i = 0; i < params.User.length; i++) {
      const u = params.User[i];
      if ((err_msg = mOnvifSoap.isInvalidValue(u, 'object'))) {
        return Promise.reject(
          new Error(`The "User" property was invalid: ${err_msg}`)
        );
      }

      if ((err_msg = mOnvifSoap.isInvalidValue(u.Username, 'string'))) {
        return Promise.reject(
          new Error(`The "Username" property was invalid: ${err_msg}`)
        );
      }

      if ('Password' in u) {
        if ((err_msg = mOnvifSoap.isInvalidValue(u.Password, 'string'))) {
          return Promise.reject(
            new Error(`The "Password" property was invalid: ${err_msg}`)
          );
        }
      }

      if ('UserLevel' in u) {
        if ((err_msg = mOnvifSoap.isInvalidValue(u.UserLevel, 'string'))) {
          return Promise.reject(
            new Error(`The "UserLevel" property was invalid: ${err_msg}`)
          );
        }
        if (!u.UserLevel.match(/^(Administrator|Operator|User|Anonymous)$/)) {
          return Promise.reject(
            new Error(
              'The "UserLevel" property must be either "Administrator", "Operator", "User", or "Anonymous".'
            )
          );
        }
      }
    }

    let body = '';

    params.User.forEach(u => {
      body += '<tds:User>';
      body += `<tt:Username>${u.Username}</tt:Username>`;
      if ('Password' in u) {
        body += `<tt:Password>${u.Password}</tt:Password>`;
      }
      if ('UserLevel' in u) {
        body += `<tt:UserLevel>${u.UserLevel}</tt:UserLevel>`;
      }
      body += '</tds:User>';
    });

    return this._createMethodRequestSoap('SetUser', body);
  }

  /* ------------------------------------------------------------------
   * Method: getRelayOutputs([callback])
   * ---------------------------------------------------------------- */
  getRelayOutputs() {
    return this._createMethodRequestSoap('GetRelayOutputs');
  }

  /* ------------------------------------------------------------------
   * Method: getNTP([callback])
   * ---------------------------------------------------------------- */
  getNTP() {
    return this._createMethodRequestSoap('GetNTP');
  }

  /* ------------------------------------------------------------------
   * Method: setNTP(params[, callback])
   * - params:
   *   - FromDHCP      | Boolean | required | true or false
   *   - NTPManual     | Object  | optional |
   *     - Type        | String  | required | "IPv4" or "IPv6"
   *     - IPv4Address | String  | required | IP address
   *
   * {
   *    "FromDHCP": "false",
   *    "NTPManual": {"Type": "IPv4", "IPv4Address": "192.168.10.1"}
   * }
   * ---------------------------------------------------------------- */
  setNTP(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.FromDHCP, 'boolean'))) {
      return Promise.reject(
        new Error(`The "FromDHCP" property was invalid: ${err_msg}`)
      );
    }

    if ('NTPManual' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.NTPManual, 'object'))) {
        return Promise.reject(
          new Error(`The "NTPManual" property was invalid: ${err_msg}`)
        );
      }

      const o = params.NTPManual;

      const type = o.Type;
      if ((err_msg = mOnvifSoap.isInvalidValue(type, 'string'))) {
        return Promise.reject(
          new Error(`The "Type" property was invalid: ${err_msg}`)
        );
      }
      if (!type.match(/^(IPv4|IPv6)$/)) {
        return Promise.reject(
          new Error(
            'The "Type" property was invalid: The value must be either "IPv4" or "IPv6".'
          )
        );
      }

      if (type === 'IPv4') {
        if ((err_msg = mOnvifSoap.isInvalidValue(o.IPv4Address, 'string'))) {
          return Promise.reject(
            new Error(`The "IPv4Address" property was invalid: ${err_msg}`)
          );
        }
      } else if (type === 'IPv6') {
        if ((err_msg = mOnvifSoap.isInvalidValue(o.IPv6Address, 'string'))) {
          return Promise.reject(
            new Error(`The "IPv6Address" property was invalid: ${err_msg}`)
          );
        }
      }
    }

    let body = '';

    body += `<tds:FromDHCP>${params.FromDHCP}</tds:FromDHCP>`;
    if ('NTPManual' in params) {
      const o = params.NTPManual;
      body += '<tds:NTPManual>';
      body += `<tt:Type>${o.Type}</tt:Type>`;
      if (o.Type === 'IPv4') {
        body += `<tt:IPv4Address>${o.IPv4Address}</tt:IPv4Address>`;
      } else {
        body += `<tt:IPv6Address>${o.IPv6Address}</tt:IPv6Address>`;
      }
      body += '</tds:NTPManual>';
    }

    return this._createMethodRequestSoap('SetNTP', body);
  }

  /* ------------------------------------------------------------------
   * Method: getDynamicDNS([callback])
   * ---------------------------------------------------------------- */
  getDynamicDNS() {
    return this._createMethodRequestSoap('GetDynamicDNS');
  }

  /* ------------------------------------------------------------------
   * Method: getZeroConfiguration([callback])
   * ---------------------------------------------------------------- */
  getZeroConfiguration() {
    return this._createMethodRequestSoap('GetZeroConfiguration');
  }

  /* ------------------------------------------------------------------
   * Method: getIPAddressFilter([callback])
   * No devcie I own supports this method for now.
   * ---------------------------------------------------------------- */
  getIPAddressFilter() {
    return this._createMethodRequestSoap('GetIPAddressFilter');
  }

  /* ------------------------------------------------------------------
   * Method: setIPAddressFilter(params[, callback])
   * - params:
   *   - Type:          | String  | required | 'Allow', 'Deny'
   *   - IPv4Address    | Array   | required |
   *     - Address      | String  | required | IPv4 address
   *     - PrefixLength | Integer | required | Prefix/submask length
   *
   * {
   *    "Type": "Allow",
   *    "IPv4Address": [
   *      {
   *        "Address": "192.168.10.3",
   *        "PrefixLength": 24
   *      }
   *    ]
   * ]}
   *
   * No devcie I own supports this method for now.
   * ---------------------------------------------------------------- */
  setIPAddressFilter(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.Type, 'string'))) {
      return Promise.reject(
        new Error(`The "Type" property was invalid: ${err_msg}`)
      );
    }
    if (!params.Type.match(/^(Allow|Deny)$/)) {
      return Promise.reject(
        new Error(
          'The "Type" property was invalid: The value must be either "Allow" or "Deny".'
        )
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.IPv4Address, 'array'))) {
      return Promise.reject(
        new Error(`The "IPv4Address" property was invalid: ${err_msg}`)
      );
    }

    for (let i = 0; i < params.IPv4Address.length; i++) {
      const o = params.IPv4Address[i];
      if ((err_msg = mOnvifSoap.isInvalidValue(o, 'object'))) {
        return Promise.reject(
          new Error(`The "IPv4Address" property was invalid: ${err_msg}`)
        );
      }
      if ((err_msg = mOnvifSoap.isInvalidValue(o.Address, 'string'))) {
        return Promise.reject(
          new Error(`The "Address" property was invalid: ${err_msg}`)
        );
      }
      if (!o.Address.match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/)) {
        return Promise.reject(
          new Error('The "Address" property was invalid as a IPv4 address.')
        );
      }
      if ((err_msg = mOnvifSoap.isInvalidValue(o.PrefixLength, 'integer'))) {
        return Promise.reject(
          new Error(`The "PrefixLength" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    body += '<tds:IPAddressFilter>';
    body += `<tt:Type>${params.Type}</tt:Type>`;
    params.IPv4Address.forEach(o => {
      body += '<tt:IPv4Address>';
      body += `<tt:Address>${o.Address}</tt:Address>`;
      body += `<tt:PrefixLength>${o.PrefixLength}</tt:PrefixLength>`;
      body += '</tt:IPv4Address>';
    });
    body += '</tds:IPAddressFilter>';

    return this._createMethodRequestSoap('SetIPAddressFilter', body);
  }

  /* ------------------------------------------------------------------
   * Method: getServices(params[, callback])
   * - params:
   *   - IncludeCapability | boolean | required | true or false
   *
   * {'IncludeCapability': false}
   * ---------------------------------------------------------------- */
  getServices(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.IncludeCapability, 'boolean'))
    ) {
      return Promise.reject(
        new Error(`The "IncludeCapability" property was invalid: ${err_msg}`)
      );
    }

    const body = `<tds:IncludeCapability>${params.IncludeCapability}</tds:IncludeCapability>`;

    return this._createMethodRequestSoap('GetServices', body);
  }

  /* ------------------------------------------------------------------
   * Method: getServiceCapabilities([callback])
   * ---------------------------------------------------------------- */
  getServiceCapabilities() {
    return this._createMethodRequestSoap('GetServiceCapabilities');
  }
}

module.exports = OnvifServiceDevice;
