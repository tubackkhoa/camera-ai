/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-cond-assign */
/* eslint-disable camelcase */
/* ------------------------------------------------------------------
 * node-onvif - service-ptz.js
 *
 * Copyright (c) 2016 - 2017, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2017-08-30
 * ---------------------------------------------------------------- */

const mUrl = require('url');
const mOnvifSoap = require('./soap.js');

/* ------------------------------------------------------------------
 * Constructor: OnvifServicePtz(params)
 * - params:
 *    - xaddr   : URL of the entry point for the media service
 *                (Required)
 *    - user  : User name (Optional)
 *    - pass  : Password (Optional)
 *    - time_diff: ms
 * ---------------------------------------------------------------- */
class OnvifServiceBase {
  constructor(params) {
    this.xaddr = '';
    this.user = '';
    this.pass = '';

    let err_msg = '';

    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      throw new Error(`The value of "params" was invalid: ${err_msg}`);
    }

    if ('xaddr' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.xaddr, 'string'))) {
        throw new Error(`The "xaddr" property was invalid: ${err_msg}`);
      } else {
        this.xaddr = params.xaddr;
      }
    } else {
      throw new Error('The "xaddr" property is required.');
    }

    if ('user' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.user, 'string', true))) {
        throw new Error(`The "user" property was invalid: ${err_msg}`);
      } else {
        this.user = params.user || '';
      }
    }

    if ('pass' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.pass, 'string', true))) {
        throw new Error(`The "pass" property was invalid: ${err_msg}`);
      } else {
        this.pass = params.pass || '';
      }
    }

    this.oxaddr = mUrl.parse(this.xaddr);
    if (this.user) {
      this.oxaddr.auth = `${this.user}:${this.pass}`;
    }

    this.time_diff = params.time_diff;
  }

  _createRequestSoap(body) {
    const soap = mOnvifSoap.createRequestSoap({
      body,
      xmlns: this.name_space_attr_list,
      diff: this.time_diff,
      user: this.user,
      pass: this.pass
    });
    return soap;
  }

  /* ------------------------------------------------------------------
   * Method: setAuth(user, pass)
   * ---------------------------------------------------------------- */
  setAuth(user, pass) {
    this.user = user || '';
    this.pass = pass || '';
    if (this.user) {
      this.oxaddr.auth = `${this.user}:${this.pass}`;
    } else {
      this.oxaddr.auth = '';
    }
  }

  /* ------------------------------------------------------------------
   * Method: getTimeDiff()
   * ---------------------------------------------------------------- */
  getTimeDiff() {
    return this.time_diff;
  }

  _createSoapTag(method, body, tag) {
    tag = tag || this.name_space_method;
    return body
      ? `<${tag}:${method}>${body}</${tag}:${method}>`
      : `<${tag}:${method}/>`;
  }

  _createMethodRequestSoap(method, body) {
    const soap_body = this._createSoapTag(method, body);
    const soap = this._createRequestSoap(soap_body);
    return mOnvifSoap.requestCommand(this.oxaddr, method, soap);
  }
}
module.exports = OnvifServiceBase;
