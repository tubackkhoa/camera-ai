/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable no-empty */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable promise/always-return */
/* eslint-disable camelcase */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable func-names */
/* eslint-disable no-underscore-dangle */
/* eslint-disable new-cap */
/* ------------------------------------------------------------------
 * node-onvif - device.js
 *
 * Copyright (c) 2016-2018, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2018-08-13
 * ---------------------------------------------------------------- */

// const mCrypto = require('crypto');
const mUrl = require('url');

const { EventEmitter } = require('events');

const mOnvifServiceDevice = require('./service-device.js');
const mOnvifServiceMedia = require('./service-media.js');
const mOnvifServicePtz = require('./service-ptz.js');
const mOnvifServiceEvents = require('./service-events.js');
const mOnvifHttpAuth = require('./http-auth.js');

/* ------------------------------------------------------------------
 * Constructor: OnvifDevice(params)
 * - params:
 *    - address : IP address of the targeted device
 *                (Required if the `xaddr` is not specified)
 *    - xaddr   : URL of the entry point for the device management service
 *                (Required if the `address' is not specified)
 *                If the `xaddr` is specified, the `address` is ignored.
 *    - user  : User name (Optional)
 *    - pass  : Password (Optional)
 * ---------------------------------------------------------------- */
class OnvifDevice extends EventEmitter {
  constructor(params) {
    if (!params || typeof params !== 'object') {
      throw new Error('The parameter was invalid.');
    }

    super();

    this.address = '';
    this.xaddr = '';
    this.user = '';
    this.pass = '';
    this.keepAddr = false;

    if ('xaddr' in params && typeof params.xaddr === 'string') {
      this.xaddr = params.xaddr;
      const ourl = mUrl.parse(this.xaddr);
      this.address = ourl.hostname;
    } else if ('address' in params && typeof params.address === 'string') {
      this.keepAddr = true;
      this.address = params.address;
      this.xaddr = `http://${this.address}/onvif/device_service`;
    } else {
      throw new Error('The parameter was invalid.');
    }
    if ('user' in params && typeof params.user === 'string') {
      this.user = params.user || '';
    }
    if ('pass' in params && typeof params.pass === 'string') {
      this.pass = params.pass || '';
    }

    this.oxaddr = mUrl.parse(this.xaddr);
    if (this.user) {
      this.oxaddr.auth = `${this.user}:${this.pass}`;
    }

    this.time_diff = 0;

    this.information = null;
    this.services = {
      device: new mOnvifServiceDevice({
        xaddr: this.xaddr,
        user: this.user,
        pass: this.pass
      }),
      events: null,
      imaging: null,
      media: null,
      ptz: null
    };
    this.profile_list = [];

    this.current_profile = null;
    this.ptz_moving = false;
  }

  /* ------------------------------------------------------------------
   * Method: getInformation()
   * ---------------------------------------------------------------- */
  getInformation() {
    const o = this.information;
    if (o) {
      return JSON.parse(JSON.stringify(o));
    }
    return null;
  }

  /* ------------------------------------------------------------------
   * Method: getCurrentProfile()
   * ---------------------------------------------------------------- */
  getCurrentProfile() {
    const o = this.current_profile;
    if (o) {
      return JSON.parse(JSON.stringify(o));
    }
    return null;
  }

  /* ------------------------------------------------------------------
   * Method: getProfileList()
   * ---------------------------------------------------------------- */
  getProfileList() {
    return JSON.parse(JSON.stringify(this.profile_list));
  }

  /* ------------------------------------------------------------------
   * Method: changeProfile(index|token)
   * ---------------------------------------------------------------- */
  changeProfile(index) {
    if (typeof index === 'number' && index >= 0 && index % 1 === 0) {
      const p = this.profile_list[index];
      if (p) {
        this.current_profile = p;
        return this.getCurrentProfile();
      }
      return null;
    }
    if (typeof index === 'string' && index.length > 0) {
      let new_profile = null;
      for (let i = 0; i < this.profile_list.length; i++) {
        if (this.profile_list[i].token === index) {
          new_profile = this.profile_list[i];
          break;
        }
      }
      if (new_profile) {
        this.current_profile = new_profile;
        return this.getCurrentProfile();
      }
    }
    return null;
  }

  /* ------------------------------------------------------------------
   * Method: getUdpStreamUrl()
   * ---------------------------------------------------------------- */
  getUdpStreamUrl() {
    if (!this.current_profile) {
      return '';
    }
    const url = this.current_profile.stream.udp;
    return url || '';
  }

  /* ------------------------------------------------------------------
   * Method: fetchSnapshot()
   * ---------------------------------------------------------------- */
  fetchSnapshot() {
    const promise = new Promise((resolve, reject) => {
      if (!this.current_profile) {
        reject(new Error('No media profile is selected.'));
        return;
      }
      if (!this.current_profile.snapshot) {
        reject(
          new Error(
            'The device does not support snapshot or you have not authorized by the device.'
          )
        );
        return;
      }
      const ourl = mUrl.parse(this.current_profile.snapshot);
      const options = {
        protocol: ourl.protocol,
        auth: `${this.user}:${this.pass}`,
        hostname: ourl.hostname,
        port: ourl.port || 80,
        path: ourl.path,
        method: 'GET'
      };
      const req = mOnvifHttpAuth.request(options, res => {
        const buffer_list = [];
        res.on('data', buf => {
          buffer_list.push(buf);
        });
        res.on('end', () => {
          if (res.statusCode === 200) {
            const buffer = Buffer.concat(buffer_list);
            let ct = res.headers['content-type'];
            if (!ct) {
              // workaround for DBPOWER
              ct = 'image/jpeg';
            }
            if (ct.match(/image\//)) {
              resolve({ headers: res.headers, body: buffer });
            } else if (ct.match(/^text\//)) {
              reject(new Error(buffer.toString()));
            } else {
              reject(new Error(`Unexpected data: ${ct}`));
            }
          } else {
            reject(new Error(`${res.statusCode} ${res.statusMessage}`));
          }
        });
        req.on('error', error => {
          reject(error);
        });
      });
      req.on('error', error => {
        reject(error);
      });
      req.end();
    });

    return promise;
  }

  /* ------------------------------------------------------------------
   * Method: ptzMove(params[, callback])
   * - params:
   *   - speed:
   *     - x     | Float   | required | speed for pan (in the range of -1.0 to 1.0)
   *     - y     | Float   | required | speed for tilt (in the range of -1.0 to 1.0)
   *     - z     | Float   | required | speed for zoom (in the range of -1.0 to 1.0)
   *   - timeout | Integer | optional | seconds (Default 1)
   * ---------------------------------------------------------------- */
  ptzMove(params) {
    if (!this.current_profile) {
      return Promise.reject(new Error('No media profile is selected.'));
    }
    if (!this.services.ptz) {
      return Promise.reject(new Error('The device does not support PTZ.'));
    }

    let { speed } = params;
    if (!speed) {
      speed = {};
    }
    const x = speed.x || 0;
    const y = speed.y || 0;
    const z = speed.z || 0;

    let { timeout } = params;
    if (!timeout || typeof timeout !== 'number') {
      timeout = 1;
    }
    const p = {
      ProfileToken: this.current_profile.token,
      Velocity: { x, y, z },
      Timeout: timeout
    };
    this.ptz_moving = true;
    return this.services.ptz.continuousMove(p);
  }

  /* ------------------------------------------------------------------
   * Method: ptzStop([callback])
   * ---------------------------------------------------------------- */
  ptzStop() {
    if (!this.current_profile) {
      return Promise.reject(new Error('No media profile is selected.'));
    }
    if (!this.services.ptz) {
      return Promise.reject(new Error('The device does not support PTZ.'));
    }
    this.ptz_moving = false;
    const p = {
      ProfileToken: this.current_profile.token,
      PanTilt: true,
      Zoom: true
    };
    return this.services.ptz.stop(p);
  }

  /* ------------------------------------------------------------------
   * Method: setAuth(user, pass)
   * ---------------------------------------------------------------- */
  setAuth(user, pass) {
    this.user = user || '';
    this.pass = pass || '';
    if (this.user) {
      this.oxaddr.auth = `${this.user}:${this.pass}`;
    }
    for (const k in this.services) {
      const s = this.services[k];
      if (s) {
        this.services[k].setAuth(user, pass);
      }
    }
  }

  /* ------------------------------------------------------------------
   * Method: init([callback])
   * ---------------------------------------------------------------- */
  async init() {
    // get infomation
    await Promise.all([
      this._getSystemDateAndTime(),
      this._getCapabilities(),
      this._getDeviceInformation()
    ]);

    // get profiles
    await this._mediaGetProfiles();

    // get streams
    await Promise.all([this._mediaGetStreamURI(), this._mediaGetSnapshotUri()]);

    return this.getInformation();
  }

  // GetSystemDateAndTime (Access Class: PRE_AUTH)
  async _getSystemDateAndTime() {
    const result = await this.services.device.getSystemDateAndTime();
    // Ignore the error becase some devices do not support
    // the GetSystemDateAndTime command and the error does
    // not cause any trouble.

    this.time_diff = this.services.device.getTimeDiff();
    return result;
  }

  // GetCapabilities (Access Class: PRE_AUTH)
  async _getCapabilities() {
    const result = await this.services.device.getCapabilities();

    const c = result.data.GetCapabilitiesResponse.Capabilities;
    if (!c) {
      throw new Error(
        'Failed to initialize the device: No capabilities were found.'
      );
    }
    const events = c.Events;
    if (events && events.XAddr) {
      this.services.events = new mOnvifServiceEvents({
        xaddr: this._getXaddr(events.XAddr),
        time_diff: this.time_diff,
        user: this.user,
        pass: this.pass
      });
    }
    const imaging = c.Imaging;
    if (imaging && imaging.XAddr) {
      /*
				this.services.imaging = new mOnvifServiceImaging({
					'xaddr'    : imaging['XAddr'],
					'time_diff': this.time_diff,
					'user'     : this.user,
					'pass'     : this.pass
				});
				*/
    }
    const media = c.Media;
    if (media && media.XAddr) {
      this.services.media = new mOnvifServiceMedia({
        xaddr: this._getXaddr(media.XAddr),
        time_diff: this.time_diff,
        user: this.user,
        pass: this.pass
      });
    }
    const ptz = c.PTZ;
    if (ptz && ptz.XAddr) {
      this.services.ptz = new mOnvifServicePtz({
        xaddr: this._getXaddr(ptz.XAddr),
        time_diff: this.time_diff,
        user: this.user,
        pass: this.pass
      });
    }
  }

  // GetDeviceInformation (Access Class: READ_SYSTEM)
  async _getDeviceInformation() {
    const result = await this.services.device.getDeviceInformation();
    this.information = result.data.GetDeviceInformationResponse;
  }

  // Media::GetProfiles (Access Class: READ_MEDIA)
  // can throw error in async like Promise.reject
  async _mediaGetProfiles() {
    const result = await this.services.media.getProfiles();

    let profiles = result.data.GetProfilesResponse.Profiles;
    if (!profiles) {
      throw new Error(
        'Failed to initialize the device: The targeted device does not any media profiles.'
      );
    }
    profiles = [].concat(profiles); // in case profiles is not a list, then forEach below will report an error
    profiles.forEach(p => {
      const profile = {
        token: p.$.token,
        name: p.Name,
        snapshot: '',
        stream: {
          udp: '',
          http: '',
          rtsp: ''
        },
        video: {
          source: null,
          encoder: null
        },
        audio: {
          source: null,
          encoder: null
        },
        ptz: {
          range: {
            x: {
              min: 0,
              max: 0
            },
            y: {
              min: 0,
              max: 0
            },
            z: {
              min: 0,
              max: 0
            }
          }
        }
      };

      if (p.VideoSourceConfiguration) {
        profile.video.source = {
          token: p.VideoSourceConfiguration.$.token,
          name: p.VideoSourceConfiguration.Name,
          bounds: {
            width: parseInt(p.VideoSourceConfiguration.Bounds.$.width, 10),
            height: parseInt(p.VideoSourceConfiguration.Bounds.$.height, 10),
            x: parseInt(p.VideoSourceConfiguration.Bounds.$.x, 10),
            y: parseInt(p.VideoSourceConfiguration.Bounds.$.y, 10)
          }
        };
      }
      if (p.VideoEncoderConfiguration) {
        profile.video.encoder = {
          token: p.VideoEncoderConfiguration.$.token,
          name: p.VideoEncoderConfiguration.Name,
          resolution: {
            width: parseInt(p.VideoEncoderConfiguration.Resolution.Width, 10),
            height: parseInt(p.VideoEncoderConfiguration.Resolution.Height, 10)
          },
          quality: parseInt(p.VideoEncoderConfiguration.Quality, 10),
          framerate: parseInt(
            p.VideoEncoderConfiguration.RateControl.FrameRateLimit,
            10
          ),
          bitrate: parseInt(
            p.VideoEncoderConfiguration.RateControl.BitrateLimit,
            10
          ),
          encoding: p.VideoEncoderConfiguration.Encoding
        };
      }
      if (p.AudioSourceConfiguration) {
        profile.audio.source = {
          token: p.AudioSourceConfiguration.$.token,
          name: p.AudioSourceConfiguration.Name
        };
      }
      if (p.AudioEncoderConfiguration) {
        profile.audio.encoder = {
          token:
            '$' in p.AudioEncoderConfiguration
              ? p.AudioEncoderConfiguration.$.token
              : '',
          name: p.AudioEncoderConfiguration.Name,
          bitrate: parseInt(p.AudioEncoderConfiguration.Bitrate, 10),
          samplerate: parseInt(p.AudioEncoderConfiguration.SampleRate, 10),
          encoding: p.AudioEncoderConfiguration.Encoding
        };
      }

      // update ptz if had
      if (p.PTZConfiguration && p.PTZConfiguration.PanTiltLimits) {
        const {
          XRange,
          YRange,
          ZRange
        } = p.PTZConfiguration.PanTiltLimits.Range;
        const { x, y, z } = profile.ptz.range;
        x.min = parseFloat(XRange.Min);
        x.max = parseFloat(XRange.Max);
        y.min = parseFloat(YRange.Min);
        y.max = parseFloat(YRange.Max);
        // default for ZRange to prevent error
        const zRange = ZRange || XRange;
        z.min = parseFloat(zRange.Min);
        z.max = parseFloat(zRange.Max);
      }

      this.profile_list.push(profile);
      if (!this.current_profile) {
        this.current_profile = profile;
      }
    });
  }

  // Media::GetStreamURI (Access Class: READ_MEDIA)
  async _mediaGetStreamURI() {
    const protocol_list = ['UDP', 'HTTP', 'RTSP'];

    let profile_index = 0;
    let protocol_index = 0;
    const getStreamUri = async () => {
      const profile = this.profile_list[profile_index];
      if (profile) {
        const protocol = protocol_list[protocol_index];
        if (protocol) {
          const { token } = profile;
          const params = {
            ProfileToken: token,
            Protocol: protocol
          };
          try {
            const result = await this.services.media.getStreamUri(params);
            let uri = result.data.GetStreamUriResponse.MediaUri.Uri;
            uri = this._getUri(uri);
            this.profile_list[profile_index].stream[
              protocol.toLowerCase()
            ] = uri;
            protocol_index++;
            await getStreamUri();
          } catch {}
        } else {
          profile_index++;
          protocol_index = 0;
          await getStreamUri();
        }
      }
    };
    await getStreamUri();
  }

  // Media::GetSnapshotUri (Access Class: READ_MEDIA)
  async _mediaGetSnapshotUri() {
    let profile_index = 0;
    const getSnapshotUri = async () => {
      const profile = this.profile_list[profile_index];
      if (profile) {
        const params = { ProfileToken: profile.token };
        try {
          const result = await this.services.media.getSnapshotUri(params);

          const snapshotResponse = result.data.GetSnapshotUriResponse;
          if (snapshotResponse.MediaUri) {
            let snapshotUri = snapshotResponse.MediaUri.Uri;
            snapshotUri = this._getSnapshotUri(snapshotUri);
            profile.snapshot = snapshotUri;
          }
          profile_index++;
          await getSnapshotUri();
        } catch (e) {
          console.log(e);
        }
      }
    };
    await getSnapshotUri();
  }

  _getXaddr(directXaddr) {
    if (!this.keepAddr) return directXaddr;
    const { path } = mUrl.parse(directXaddr);
    return `http://${this.address}${path}`;
  }

  _getUri(directUri) {
    if (typeof directUri === 'object' && directUri._) {
      directUri = directUri._;
    }
    if (!this.keepAddr) return directUri;
    const base = mUrl.parse(`http://${this.address}`);
    const parts = mUrl.parse(directUri);
    const newParts = {
      host: base.host,
      pathname: base.pathname + parts.pathname
    };
    const newUri = mUrl.format(newParts);
    return newUri;
  }

  _getSnapshotUri(directUri) {
    if (typeof directUri === 'object' && directUri._) {
      directUri = directUri._;
    }
    if (!this.keepAddr) return directUri;
    const base = mUrl.parse(`http://${this.address}`);
    const parts = mUrl.parse(directUri);
    const newParts = {
      protocol: parts.protocol,
      host: base.host,
      pathname: base.pathname + parts.pathname
    };
    const newUri = mUrl.format(newParts);
    return newUri;
  }
}

module.exports = OnvifDevice;
