/* eslint-disable no-empty */
/* eslint-disable camelcase */
/* eslint-disable func-names */
/* eslint-disable no-plusplus */
const xml2js = require('xml2js');
const mCrypto = require('crypto');

function createCnonce(digit) {
  const nonce = Buffer.alloc(digit);
  for (let i = 0; i < digit; i++) {
    nonce.writeUInt8(Math.floor(Math.random() * 256), i);
  }
  return nonce.toString('hex');
}

function createHash(algo, data) {
  let hash = null;
  if (algo === 'MD5') {
    hash = mCrypto.createHash('md5');
  } else {
    hash = mCrypto.createHash('sha256');
  }
  hash.update(data, 'utf8');
  return hash.digest('hex');
}

function parseAuthHeader(h) {
  const o = {};
  h.split(/,\s*/).forEach(s => {
    const pair = s.split('=');
    const k = pair[0];
    let v = pair[1];
    if (!k || !v) {
      return;
    }
    v = v.replace(/^\"/, '');
    v = v.replace(/\"$/, '');
    o[k] = v;
  });
  if (!o.algorithm) {
    // workaround for DBPOWER
    o.algorithm = 'MD5';
  }
  return o;
}

function parseSOAP(soap) {
  const opts = {
    explicitRoot: false,
    explicitArray: false,
    ignoreAttrs: false, // Never change to `true`
    tagNameProcessors: [
      function(name) {
        const m = name.match(/^([^:]+):([^:]+)$/);
        return m ? m[2] : name;
      }
    ]
  };

  return new Promise(resolve => {
    xml2js.parseString(soap, opts, (err, result) => {
      resolve(result || []);
    });
  });
}

function getFaultReason(r) {
  let reason = '';
  try {
    const reason_el = r.Body.Fault.Reason;
    if (reason_el.Text) {
      reason = reason_el.Text;
    } else {
      const code_el = r.Body.Fault.Code;
      if (code_el.Value) {
        reason = code_el.Value;
        const subcode_el = code_el.Subcode;
        if (subcode_el.Value) {
          reason += ` ${subcode_el.Value}`;
        }
      }
    }
  } catch {}
  return reason;
}

function parseResponseResult(methodName, res) {
  const s0 = res.Body;
  if (!s0) {
    return null;
  }
  if (`${methodName}Response` in s0) {
    return s0;
  }
  return null;
}

function parseGetSystemDateAndTime(s) {
  const s0 = s.Body;
  if (!s0) {
    return null;
  }
  const s1 = s0.GetSystemDateAndTimeResponse;
  if (!s1) {
    return null;
  }
  const s2 = s1.SystemDateAndTime;
  if (!s2) {
    return null;
  }

  const type = s2.DateTimeType || '';
  let dst = null;
  if (s2.DaylightSavings) {
    dst = s2.DaylightSavings === 'true';
  }
  const tz = s2.TimeZone && s2.TimeZone.TZ ? s2.TimeZone.TZ : '';
  let date = null;
  if (s2.UTCDateTime) {
    const udt = s2.UTCDateTime;
    const t = udt.Time;
    const d = udt.Date;
    if (
      t &&
      d &&
      t.Hour &&
      t.Minute &&
      t.Second &&
      d.Year &&
      d.Month &&
      d.Day
    ) {
      date = new Date();
      date.setUTCFullYear(parseInt(d.Year, 10));
      date.setUTCMonth(parseInt(d.Month, 10) - 1);
      date.setUTCDate(parseInt(d.Day, 10));
      date.setUTCHours(parseInt(t.Hour, 10));
      date.setUTCMinutes(parseInt(t.Minute, 10));
      date.setUTCSeconds(parseInt(t.Second, 10));
    }
  }
  const res = {
    type,
    dst,
    tz,
    date
  };
  return res;
}

function getTypeOfValue(value) {
  if (value === undefined) {
    return 'undefined';
  }
  if (value === null) {
    return 'null';
  }
  if (Array.isArray(value)) {
    return 'array';
  }
  const t = typeof value;
  if (t === 'boolean') {
    return 'boolean';
  }
  if (t === 'string') {
    return 'string';
  }
  if (t === 'number') {
    if (value % 1 === 0) {
      return 'integer';
    }
    return 'float';
  }
  if (
    t === 'object' &&
    Object.prototype.toString.call(value) === '[object Object]'
  ) {
    return 'object';
  }

  return 'unknown';
}

module.exports = {
  parseSOAP,
  parseResponseResult,
  getTypeOfValue,
  getFaultReason,
  createCnonce,
  createHash,
  parseAuthHeader,
  parseGetSystemDateAndTime
};
