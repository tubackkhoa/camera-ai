/* eslint-disable no-bitwise */
/* eslint-disable no-plusplus */
/* eslint-disable promise/always-return */
/* eslint-disable camelcase */
/* eslint-disable no-underscore-dangle */
/* ------------------------------------------------------------------
 * node-onvif - node-onvif.js
 *
 * Copyright (c) 2016 - 2017, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2017-09-30
 * ---------------------------------------------------------------- */

const mDgram = require('dgram');
const mCrypto = require('crypto');
const onvifDevice = require('./modules/device.js');
const { parseSOAP } = require('./modules/utils');

const soap_tmpl = `<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:a="http://schemas.xmlsoap.org/ws/2004/08/addressing">
  <s:Header>
    <a:Action s:mustUnderstand="1">http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</a:Action>
    <a:MessageID>uuid:__uuid__</a:MessageID>
    <a:ReplyTo>
      <a:Address>http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous</a:Address>
    </a:ReplyTo>
    <a:To s:mustUnderstand="1">urn:schemas-xmlsoap-org:ws:2005:04:discovery</a:To>
  </s:Header>
  <s:Body>
    <Probe xmlns="http://schemas.xmlsoap.org/ws/2005/04/discovery">
      <d:Types xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery" xmlns:dp0="http://www.onvif.org/ver10/network/wsdl">dp0:__type__</d:Types>
    </Probe>
  </s:Body>
</s:Envelope>`
  .replace(/>\s+</g, '><')
  .replace(/\s+/, ' ');

/* ------------------------------------------------------------------
 * Constructor: Onvif()
 * ---------------------------------------------------------------- */
class Onvif {
  constructor() {
    // Public
    this.OnvifDevice = onvifDevice;
    // Private

    this._MULTICAST_ADDRESS = '239.255.255.250';
    this._PORT = 3702;
    this._udp = null;
    this._devices = {};
    this._DISCOVERY_INTERVAL = 150; // ms
    this._DISCOVERY_RETRY_MAX = 3;
    this._DISCOVERY_WAIT = 3000; // ms
    this._discovery_interval_timer = null;
    this._discovery_wait_timer = null;
  }

  /* ------------------------------------------------------------------
   * Method: startProbe([callback])
   * ---------------------------------------------------------------- */
  startProbe() {
    return new Promise((resolve, reject) => {
      this._devices = {};
      this._udp = mDgram.createSocket('udp4');

      this._udp.once('error', error => {
        reject(error);
      });

      this._udp.on('message', buf => {
        parseSOAP(buf.toString())
          .then(result => {
            let urn = '';
            let xaddrs = [];
            let scopes = [];
            let types = '';
            try {
              const probe_matches = result.Body.ProbeMatches;

              // make sure the right data exists
              if (probe_matches !== undefined) {
                const probe_match = probe_matches.ProbeMatch;
                urn = probe_match.EndpointReference.Address;
                xaddrs = probe_match.XAddrs.split(/\s+/);
                if (typeof probe_match.Scopes === 'string') {
                  scopes = probe_match.Scopes.split(/\s+/);
                } else if (
                  typeof probe_match.Scopes === 'object' &&
                  typeof probe_match.Scopes._ === 'string'
                ) {
                  scopes = probe_match.Scopes._.split(/\s+/);
                }
                // modified to support Pelco cameras
                if (typeof probe_match.Types === 'string') {
                  types = probe_match.Types.split(/\s+/);
                } else if (
                  typeof probe_match.Types === 'object' &&
                  typeof probe_match.Types._ === 'string'
                ) {
                  types = probe_match.Types._.split(/\s+/);
                }
              }
            } catch (e) {
              return;
            }
            if (urn && xaddrs.length > 0 && scopes.length > 0) {
              if (!this._devices[urn]) {
                let name = '';
                let hardware = '';
                let location = '';
                scopes.forEach(s => {
                  if (s.indexOf('onvif://www.onvif.org/hardware/') === 0) {
                    hardware = s.split('/').pop();
                  } else if (
                    s.indexOf('onvif://www.onvif.org/location/') === 0
                  ) {
                    location = s.split('/').pop();
                  } else if (s.indexOf('onvif://www.onvif.org/name/') === 0) {
                    name = s.split('/').pop();
                    name = name.replace(/_/g, ' ');
                  }
                });
                const probe = {
                  urn,
                  name,
                  hardware,
                  location,
                  types,
                  xaddrs,
                  scopes
                };
                this._devices[urn] = probe;
              }
            }
          })
          .catch(reject);
      });

      this._udp.bind(() => {
        this._udp.removeAllListeners('error');
        this._sendProbe().catch(reject);
        this._discovery_wait_timer = setTimeout(() => {
          this.stopProbe()
            .then(() => Object.values(this._devices))
            .then(resolve)
            .catch(reject);
        }, this._DISCOVERY_WAIT);
      });
    });
  }

  _sendProbe() {
    const soap_set = [];
    ['NetworkVideoTransmitter', 'Device', 'NetworkVideoDisplay'].forEach(
      type => {
        let s = soap_tmpl;
        s = s.replace('__type__', type);
        s = s.replace('__uuid__', this._createUuidV4());
        soap_set.push(s);
      }
    );

    const soap_list = [];
    for (let i = 0; i < this._DISCOVERY_RETRY_MAX; i++) {
      soap_set.forEach(s => {
        soap_list.push(s);
      });
    }

    return new Promise((resolve, reject) => {
      if (!this._udp) {
        reject(
          new Error(
            'No UDP connection is available. The init() method might not be called yet.'
          )
        );
      }
      const send = () => {
        const soap = soap_list.shift();
        if (soap) {
          const buf = Buffer.from(soap, 'utf8');
          this._udp.send(
            buf,
            0,
            buf.length,
            this._PORT,
            this._MULTICAST_ADDRESS,
            () => {
              this._discovery_interval_timer = setTimeout(() => {
                send();
              }, this._DISCOVERY_INTERVAL);
            }
          );
        } else {
          resolve();
        }
      };
      send();
    });
  }

  _createUuidV4() {
    const clist = mCrypto
      .randomBytes(16)
      .toString('hex')
      .toLowerCase()
      .split('');
    clist[12] = '4';
    clist[16] = ((parseInt(clist[16], 16) & 3) | 8).toString(16);
    const m = clist.join('').match(/^(.{8})(.{4})(.{4})(.{4})(.{12})/);
    const uuid = [m[1], m[2], m[3], m[4], m[5]].join('-');
    this._uuid = uuid;
    return uuid;
  }

  /* ------------------------------------------------------------------
   * Method: stopProbe([callback])
   * ---------------------------------------------------------------- */
  stopProbe() {
    if (this._discovery_interval_timer !== null) {
      clearTimeout(this._discovery_interval_timer);
      this._discovery_interval_timer = null;
    }
    if (this._discovery_wait_timer !== null) {
      clearTimeout(this._discovery_wait_timer);
      this._discovery_wait_timer = null;
    }

    return new Promise(resolve => {
      if (this._udp) {
        this._udp.close(() => {
          this._udp.unref();
          this._udp = null;
          resolve();
        });
      } else {
        resolve();
      }
    });
  }
}

module.exports = Onvif;
