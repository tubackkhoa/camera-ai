/* eslint-disable no-underscore-dangle */
const { PassThrough } = require('stream');

const VideopacketPayloads = require('../constants/VideopacketPayloads');
const MessageIds = require('../constants/Messages');

const { VideoPacketParser } = require('../helpers/Parsers');

const DVRIPClient = require('./dvripclient.js');

const HEADER_MESSAGEID_OFFSET = 14;

/**
 * Extension of DVRIPClient to support Audio / Videostreaming
 * @extends DVRIPClient
 */
class DVRIPStreamClient extends DVRIPClient {
  constructor({ StreamType = 'Main', ...options }) {
    super(options);
    this.StreamType = StreamType;
  }

  /**
   * Claim video stream on this connection, thus allowing the parent connection to start it
   *
   * @param {Object} streamInfo
   * @param {string} [streamInfo.Channel=0] Videochannel to claim. Probably only useful for DVR's and not for IP Cams
   * @param {string} [streamInfo.CombinMode="CONNECT_ALL"] Unknown. "CONNECT_ALL" and "NONE" work
   * @returns {Promise} Promise resolves with {@link DVRIPCommandResponse} of called underlying command
   */
  async claimVideoStream(Channel = 0, CombinMode = 'CONNECT_ALL') {
    const claimResult = await this.executeHelper('MONITOR_CLAIM', 'OPMonitor', {
      Action: 'Claim',
      Parameter: {
        Channel,
        CombinMode,
        StreamType: this.StreamType,
        TransMode: this.protocol
      }
    });

    // // if no activity in 5s
    // this._socket.setTimeout(5000);

    this._videoStream = new PassThrough();
    this._audioStream = new PassThrough();

    return claimResult;
  }

  /**
   * Grabs the video stream of the Device
   *
   * @param {Object} streamInfo
   * @param {string} [streamInfo.StreamType="Main"] Substream to grab. Known to work are "Main" and "Extra1"
   * @param {string} [streamInfo.Channel=0] Videochannel to grab. Probably only useful for DVR's and not for IP Cams
   * @param {string} [streamInfo.CombinMode="CONNECT_ALL"] Unknown. "CONNECT_ALL" and "NONE" work
   * @returns {Promise} Promise resolves with a {@link DVRIPStream} object
   */
  async getVideoStream(Channel = 0, CombinMode = 'CONNECT_ALL') {
    // if already claim, so just return
    if (!this.claimResult) {
      try {
        // await _streamClient.connect();
        // re-get stream
        this.claimResult = await this.claimVideoStream(Channel, CombinMode);
        // no need for waiting data
        await this.executeHelper(
          'MONITOR_REQ',
          'OPMonitor',
          {
            Action: 'Start',
            Parameter: {
              Channel,
              CombinMode,
              StreamType: this.StreamType,
              TransMode: this.protocol
            }
          },
          true
        );
      } catch (err) {
        this.close();
        throw err;
      }
    }

    return {
      video: this._videoStream,
      audio: this._audioStream
    };
  }

  /**
   * Ends active video stream. Options have to match the getVideoStream() call
   *
   * @param {Object} streamInfo
   * @param {string} [streamInfo.StreamType="Main"] Substream to end. Known to work are "Main" and "Extra1"
   * @param {string} [streamInfo.Channel=0] Videochannel to end. Probably only useful for DVR's and not for IP Cams
   * @param {string} [streamInfo.CombinMode="CONNECT_ALL"] Unknown. "CONNECT_ALL" and "NONE" work
   * @returns {Promise} Promise resolves with {@link DVRIPCommandResponse} of called underlying command
   */
  async stopVideoStream(Channel = 0, CombinMode = 'NONE') {
    if (!this._videoStream)
      // throw new Error('There no active Videostream instance');
      return;

    try {
      return await this.executeHelper('MONITOR_REQ', 'OPMonitor', {
        Action: 'Stop',
        Parameter: {
          Channel,
          CombinMode,
          StreamType: this.StreamType,
          TransMode: this.protocol
        }
      });
    } finally {
      this.disconnectVideoStream();
    }
  }

  dataParser(responseBuffer) {
    const MessageId = responseBuffer.readUInt16LE(HEADER_MESSAGEID_OFFSET);

    if (!this._videoStream || MessageId !== MessageIds.MONITOR_DATA)
      return super.dataParser(responseBuffer);

    const { RawBody } = VideoPacketParser.parse(responseBuffer);

    // 0x000001 = NAL Unit Seperator
    if (RawBody[0] !== 0 || RawBody[1] !== 0 || RawBody[2] !== 1)
      return this.writeVideo(RawBody);

    const PayloadType = RawBody[3];

    if (PayloadType === VideopacketPayloads.Audio) {
      this.writeAudio(RawBody.slice(8));
      // Failing to remove these 8-16 bytes for these messages for some reason makes FFMPEG unhappy.
      // The output is apparently still a valid video stream.
    } else if (
      PayloadType === VideopacketPayloads.IFrame ||
      PayloadType === VideopacketPayloads.PlusEnc
    ) {
      this.writeVideo(RawBody.slice(16));
    } else {
      this.writeVideo(RawBody.slice(8));
    }
  }

  writeVideo(buffer) {
    if (this._videoStream.writable) this._videoStream.write(buffer);
  }

  writeAudio(buffer) {
    if (this._audioStream.writable) this._audioStream.write(buffer);
  }

  disconnectVideoStream() {
    // force re-claim
    delete this.claimResult;

    if (this._videoStream) {
      this._videoStream.removeAllListeners();
      this._videoStream.end();
      this._videoStream.destroy();
      delete this._videoStream;
    }

    if (this._audioStream) {
      this._audioStream.removeAllListeners();
      this._audioStream.end();
      this._audioStream.destroy();
      delete this._audioStream;
    }
  }

  close() {
    super.close();
    this.disconnectVideoStream();
  }
}

module.exports = DVRIPStreamClient;
