/* eslint-disable no-param-reassign */
/* eslint-disable compat/compat */
import { RingBuffer } from '../utils';
import { API_BASE, rejectErrors } from './common';

export * from './common';

const abortControllers = new RingBuffer(20, controller => controller.abort());

// try invoke callback for refresh token here
export const fetchWithOptions = (url, options = {}, base = API_BASE) => {
  // in the same server, API_BASE is emtpy
  // check convenient way of passing base directly
  if (!options.signal) {
    const abortController = new AbortController();
    abortControllers.push(abortController);
    options.signal = abortController.signal;
  }

  return (
    fetch(/^(?:https?)?:\/\//.test(url) ? url : base + url, options)
      .then(rejectErrors)
      // default return empty json when no content, we always use json, never use plain text
      .then(res => res.json())
      // {
      //   const contentType = res.headers.get('content-type') || '';
      //   // json or pure text
      //   return res.status !== 204 &&
      //     contentType.indexOf('application/json') !== -1
      //     ? res.json()
      //     : res.text();
      // })
      .catch(ex => console.log(ex))
  );
};

export const abortRequests = () => {
  // console.log('abortControllers', abortControllers.length);
  abortControllers.clean();
};

export const fetchJson = (url, options = {}, base) =>
  // in the same server, API_BASE is emtpy
  // check convenient way of passing base directly
  fetchWithOptions(
    url,
    {
      ...options,
      headers: {
        ...options.headers,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    },
    base
  );

export const fetchJsonWithToken = (token, url, options = {}, ...args) =>
  fetchJson(
    url,
    {
      ...options,
      headers: {
        ...options.header,
        Authorization: `Bearer ${token.accessToken || token}`
      }
    },
    ...args
  );

// only post have post data
export const get = (url, method = 'GET') =>
  fetchJson(url, {
    method
  });

export const post = (url, data, raw = false, signal = null) =>
  raw // raw is binary
    ? fetchWithOptions(url, {
        method: 'POST',
        signal,
        body: data,
        headers: {
          'Content-Type': 'application/octet-stream'
        }
      })
    : fetchJson(url, {
        method: 'POST',
        signal,
        body: JSON.stringify(data)
      });
