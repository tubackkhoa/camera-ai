import { app, Menu, shell, BrowserWindow } from 'electron';

// const { dialog } = require('electron')

const subMenuHelp = {
  label: 'Help',
  submenu: [
    {
      label: 'Learn More',
      click() {
        shell.openExternal('http://electron.atom.io');
      }
    },
    {
      label: 'Documentation',
      click() {
        shell.openExternal(
          'https://github.com/atom/electron/tree/master/docs#readme'
        );
      }
    }
  ]
};

const subMenuAbout = {
  label: 'CameraAI',
  submenu: [
    {
      label: 'About CameraAI',
      selector: 'orderFrontStandardAboutPanel:'
    },
    { type: 'separator' },
    { label: 'Services', submenu: [] },
    { type: 'separator' },
    {
      label: 'Hide CameraAI',
      accelerator: 'Command+H',
      selector: 'hide:'
    },
    {
      label: 'Hide Others',
      accelerator: 'Command+Shift+H',
      selector: 'hideOtherApplications:'
    },
    { label: 'Show All', selector: 'unhideAllApplications:' },
    { type: 'separator' },
    {
      label: 'Quit',
      accelerator: 'Command+Q',
      click: () => {
        app.quit();
      }
    }
  ]
};
const subMenuEdit = {
  label: 'Edit',
  submenu: [
    { label: 'Undo', accelerator: 'Command+Z', selector: 'undo:' },
    { label: 'Redo', accelerator: 'Shift+Command+Z', selector: 'redo:' },
    { type: 'separator' },
    { label: 'Cut', accelerator: 'Command+X', selector: 'cut:' },
    { label: 'Copy', accelerator: 'Command+C', selector: 'copy:' },
    { label: 'Paste', accelerator: 'Command+V', selector: 'paste:' },
    {
      label: 'Select All',
      accelerator: 'Command+A',
      selector: 'selectAll:'
    }
  ]
};

const subMenuWindow = {
  label: 'Window',
  submenu: [
    {
      label: 'Minimize',
      accelerator: 'Command+M',
      selector: 'performMiniaturize:'
    },
    { label: 'Close', accelerator: 'Command+W', selector: 'performClose:' },
    { type: 'separator' },
    { label: 'Bring All to Front', selector: 'arrangeInFront:' }
  ]
};

// export default class FolderChooser {
//   buildFolderChooser() {

//   }
// }

export default class MenuBuilder {
  mainWindow: BrowserWindow;

  constructor(mainWindow: BrowserWindow) {
    this.mainWindow = mainWindow;

    const subMenuViewDev = {
      label: 'View',
      submenu: [
        {
          label: 'Reload',
          accelerator: 'Command+R',
          click: () => {
            this.mainWindow.webContents.reload();
          }
        },
        {
          label: 'Toggle Full Screen',
          accelerator: 'Ctrl+Command+F',
          click: () => {
            this.mainWindow.setFullScreen(!this.mainWindow.isFullScreen());
          }
        },
        {
          label: 'Toggle Developer Tools',
          accelerator: 'Alt+Command+I',
          click: () => {
            this.mainWindow.toggleDevTools();
          }
        }
      ]
    };

    const subMenuViewProd = {
      label: 'View',
      submenu: [
        {
          label: 'Toggle Full Screen',
          accelerator: 'Ctrl+Command+F',
          click: () => {
            this.mainWindow.setFullScreen(!this.mainWindow.isFullScreen());
          }
        }
      ]
    };

    this.subMenuView =
      process.env.NODE_ENV === 'development' ? subMenuViewDev : subMenuViewProd;
  }

  buildMenu() {
    // has checked outside
    this.setupDevelopmentEnvironment();

    const template =
      process.platform === 'darwin'
        ? this.buildDarwinTemplate()
        : this.buildDefaultTemplate();

    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    // let options = {properties:["openDirectory"]}

    // console.log('show dialog')
    // const folderSelected = dialog.showOpenDialog(options)
    // console.log('folder select ' + folderSelected)

    return menu;
  }

  setupDevelopmentEnvironment() {
    this.mainWindow.openDevTools();
    this.mainWindow.webContents.on('context-menu', (e, props) => {
      const { x, y } = props;

      Menu.buildFromTemplate([
        {
          label: 'Inspect element',
          click: () => {
            this.mainWindow.inspectElement(x, y);
          }
        }
      ]).popup(this.mainWindow);
    });
  }

  buildDarwinTemplate() {
    return [
      subMenuAbout,
      subMenuEdit,
      this.subMenuView,
      subMenuWindow,
      subMenuHelp
    ];
  }

  buildDefaultTemplate() {
    const templateDefault = [
      {
        label: '&File',
        submenu: [
          {
            label: '&Open',
            accelerator: 'Ctrl+O'
          },
          {
            label: '&Close',
            accelerator: 'Ctrl+W',
            click: () => {
              this.mainWindow.close();
            }
          }
        ]
      },
      this.subMenuView,
      subMenuHelp
    ];

    return templateDefault;
  }
}
