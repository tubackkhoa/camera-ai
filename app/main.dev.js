/* eslint global-require: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */

import { app, BrowserWindow, Menu, Tray, nativeImage } from 'electron';
import path from 'path';
import MenuBuilder from './menu';

const logger = require('./server/logger');

// global variable
global.logger = logger;
global.startHot = process.env.START_HOT !== 'false';
global.isDev = process.env.NODE_ENV === 'development';

// const cpus = require('os').cpus().length;
// logger.info('cpus: ' + cpus);

// let bgWindow = null;
let httpServer;
let mainWindow = null;
let closeHttpServer;

const icon = nativeImage.createFromPath(
  path.resolve(__dirname, '..', 'resources', 'icon.png')
);

const DEBUG =
  process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true';

const devTools = DEBUG;

const installExtensions = async () => {
  const installer = await import('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

  return Promise.all(
    extensions.map(name => installer.default(installer[name], forceDownload))
  ).catch(logger.error);
};

const isDarwin = process.platform === 'darwin';
const modelPath = path.resolve(__dirname, 'models');
const distPath = path.resolve(__dirname, 'dist');

// // Create a hidden background window
// function createBgWindow() {
//   // global access
//   global.bgWindow = bgWindow = new BrowserWindow({
//     width: 1024,
//     height: 400,
//     show: DEBUG,
//     closable: false,

//     // parent: mainWindow,
//     webPreferences: {
//       devTools,
//       nodeIntegration: true,
//       backgroundThrottling: false
//       // experimentalFeatures: true
//     }
//   });
//   bgWindow.loadURL(`file://${__dirname}/background.html`);

//   bgWindow.minimize();

//   bgWindow.webContents.on('did-finish-load', () => {
//     if (!bgWindow) {
//       throw new Error('"bgWindow" is not defined');
//     }

//     bgWindow.webContents.send('load-model', modelPath);
//   });

//   bgWindow.on('closed', () => {
//     logger.info(`background window closed`);
//     // force close
//     if (mainWindow) mainWindow.send('close');
//   });
// }

// app.disableHardwareAcceleration();
app.allowRendererProcessReuse = true;

// incase run at terminal
function quit() {
  // return back log instance for last
  logger.setLogInstance(console);
  if (!httpServer) return app.quit();
  return closeHttpServer(httpServer).then(() => app.quit());
}

function createWindow() {
  // allow remote access
  global.mainWindow = mainWindow = new BrowserWindow({
    show: false,
    icon,
    webPreferences: {
      devTools,
      nodeIntegration: true
      // in case doing renderer or image processing
      // backgroundThrottling: false,
      // experimentalFeatures: true
    }
  });

  mainWindow.setTitle('Quản lý Camera');

  mainWindow.maximize();

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  // delay a little bit for server
  mainWindow.once('ready-to-show', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    // if (process.env.START_MINIMIZED) {
    //   mainWindow.minimize();
    // } else {
    mainWindow.show();
    mainWindow.focus();
    // }
  });

  // mainWindow.on('closed', () => {
  //   mainWindow = null;
  //   // same as this is on top of mainWindow
  //   bgWindow.closable = true;
  //   bgWindow.close();
  // });
}

app.on('quit', () => {
  setTimeout(process.exit, 1000);
});

app.on('window-all-closed', () => {
  logger.info('All windows closed, quitting');
  return quit();
});

app.on('ready', async () => {
  // start express at seperate terminal, if production
  if (
    process.env.NODE_SERVER === 'true' ||
    (process.env.NODE_ENV === 'production' &&
      process.env.NODE_SERVER !== 'false')
  ) {
    const appData = process.env.APP_DATA || `${app.getPath('appData')}/AGT`;
    const availablePort = process.env.SERVER_PORT || 3000;
    logger.info('appData', appData, 'api.port', availablePort);
    // require('require-rebuild')();
    // we need spawn, so do not provide execFile input
    const ffmpegPath = path.resolve(process.resourcesPath, 'bin', 'ffmpeg');
    const server = await import('./server');
    closeHttpServer = server.closeHttpServer;
    // use client model
    httpServer = await server.runHttpServer({
      logType: DEBUG ? logger.LOG_TYPES.NORMAL : logger.LOG_TYPES.NONE,
      port: availablePort,
      appData,
      modelPath,
      distPath,
      ffmpegPath
    });
  } else {
    closeHttpServer = () => Promise.resolve();
  }

  // now get back log after server is ready
  createWindow();

  // if debug in production
  if (DEBUG) {
    await installExtensions();
    if (process.env.NODE_ENV === 'production') {
      const { install } = await import('source-map-support');
      install();
    } else {
      // only turn debug for development
      await import('electron-debug').then(debug => debug.default());
    }
  }

  // // create after show main, only for AI
  // if (process.env.DISABLED_AI !== 'true') {
  //   createBgWindow();
  // }

  // MacOS specific, smaller
  if (isDarwin) {
    app.dock.setIcon(icon);
  }

  const trayMenu = Menu.buildFromTemplate([
    {
      label: 'Quit',
      type: 'normal',
      click: () => {
        return quit();
      }
    }
  ]);

  const tray = new Tray(icon);
  tray.setTitle('Quản lý camera');
  tray.setToolTip('Quản lý Camera');
  tray.setContextMenu(trayMenu);

  // can turn off debugger
  if (process.env.MENU_DEBUG === 'true' || DEBUG) {
    // logger.info('show menu builder');
    const menuBuilder = new MenuBuilder(mainWindow);
    menuBuilder.buildMenu();

    // open for background
    // if (bgWindow) bgWindow.openDevTools();
  }

  // Remove this if your app does not use auto updates
  if (process.env.AUTO_UPDATE === 'true') {
    const { autoUpdater } = await import('electron-updater');
    autoUpdater.checkForUpdatesAndNotify();
  }
});
