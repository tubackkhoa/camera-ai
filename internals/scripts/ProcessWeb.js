/* eslint-disable global-require */
import fs from 'fs';
import path from 'path';

const generateCss = src => `<link rel="stylesheet" href="${src}" />`;
const generateJs = src =>
  `<script type="text/javascript" src="${src}"></script>`;

export default function ProcessWeb(indexFile: string, contentBase: string) {
  // replace {{CSS}} {{JS}}
  let css;
  let js;
  if (process.env.NODE_ENV === 'production') {
    css = generateCss('/style.css');
    js = generateJs('/renderer.web.prod.js');
  } else {
    css = '';
    js = ['/renderer.dev.dll.js', '/dist/renderer.dev.js']
      .map(generateJs)
      .join('');
  }
  const outContent = fs
    .readFileSync(indexFile)
    .toString()
    .replace('{{CSS}}', css)
    .replace('{{JS}}', js);
  // rewrite file
  fs.writeFileSync(path.join(contentBase, `index.html`), outContent);
}

if (require.main === module) {
  // run directly
  const { appBase, contentBase } = require('../../configs/webpack.config.base');
  ProcessWeb(path.join(appBase, 'index.html'), contentBase);
}
