const nodemailer = require('nodemailer');

const { log } = console;

// Step 1
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.EMAIL || 'atautowarning@gmail.com', // TODO: your gmail account
    pass: process.env.PASSWORD || 'Atsend@123' // TODO: your gmail password
  }
});

// Step 2
const mailOptions = {
  from: 'atautowarning@gmail.com', // TODO: email sender
  to: 'cuongnh1807@gmail.com', // TODO: email receiver
  subject: 'Nodemailer - Test',
  text: 'Wooohooo it works!!',
  attachments: [
    {
      filename: 'profile.png',
      path:
        '/Users/apple/Desktop/machine_learning/projects/camera-ai/media/ecbe62770035533af79ab13cf64decfb/1597837744653.jpg'
    } // TODO: replace it with your own image
  ]
};

// Step 3
transporter.sendMail(mailOptions, (err, data) => {
  if (err) {
    return log('Error occurs');
  }
  return log('Email sent!!!', data);
});
