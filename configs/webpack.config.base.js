/**
 * Base webpack config used across other specific configs
 */

import path from 'path';
import webpack from 'webpack';
// import 'aws-sdk/dist/aws-sdk';
import { dependencies as externals } from '../app/package.json';

export const libraryTarget = 'commonjs2';
export const rootBase = path.join(__dirname, '..');
export const appBase = path.join(rootBase, 'app');
export const contentBase = path.join(appBase, 'dist');

export default {
  externals: ['crypto', ...Object.keys(externals || {})],

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          }
        }
      },
      { test: /aws-sdk/, loaders: ['transform-loader?brfs'] }
    ]
  },

  output: {
    path: appBase,
    // https://github.com/webpack/webpack/issues/1114
    libraryTarget
  },

  /**
   * Determine the array of extensions that should be used to resolve modules.
   */
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [appBase, 'node_modules']
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production'
    }),

    new webpack.NamedModulesPlugin()
  ]
};
