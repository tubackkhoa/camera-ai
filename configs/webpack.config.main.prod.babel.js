/**
 * Webpack config for production electron main process
 */

import webpack from 'webpack';
import merge from 'webpack-merge';
import TerserPlugin from 'terser-webpack-plugin';
import path from 'path';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import baseConfig, { appBase } from './webpack.config.base';
import CheckNodeEnv from '../internals/scripts/CheckNodeEnv';

CheckNodeEnv('production');

const target = process.env.TARGET || 'electron-main';

const entry = {
  main: path.join(appBase, target === 'node' ? 'server' : 'main.dev')
};

// add worker entries
if (process.env.WORKER === 'true') {
  entry['cam-worker'] = path.join(appBase, 'server', 'onvif', 'cam-worker');
}

const sourceMap = process.env.SOURCE_MAP
  ? process.env.SOURCE_MAP === 'true'
  : true; // default true

const plugins = [
  // resolve external lib for express
  new webpack.ContextReplacementPlugin(
    /express\/lib/, // replace all searches in
    // express/lib/*
    path.resolve('node_modules') // to look in folder 'node_modules'
  ), // __webpack_require__(...)(mod)

  new BundleAnalyzerPlugin({
    analyzerMode: process.env.OPEN_ANALYZER === 'true' ? 'server' : 'disabled',
    openAnalyzer: process.env.OPEN_ANALYZER === 'true'
  }),

  /**
   * Create global constants which can be configured at compile time.
   *
   * Useful for allowing different behaviour between development builds and
   * release builds
   *
   * NODE_ENV should be production so that modules do not perform certain
   * development checks
   */
  new webpack.EnvironmentPlugin({
    NODE_ENV: 'production',
    WEB: process.env.WEB || 'false',
    SERVER_PORT: process.env.SERVER_PORT || 80,
    DISABLED_AI: process.env.DISABLED_AI || 'false',
    DEBUG_PROD: process.env.DEBUG_PROD || 'false',
    START_HOT: process.env.START_HOT || 'false',
    MENU_DEBUG: process.env.MENU_DEBUG || 'false',
    NODE_SERVER: process.env.NODE_SERVER || 'true'
    // START_MINIMIZED: false
  }),

  // remove locale of moment by default
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
];

if (target === 'node') {
  plugins.push(
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    })
  );
}

export default merge.smart(baseConfig, {
  devtool: sourceMap ? 'source-map' : false,

  // custom modules that can ignore
  externals: ['encoding'],

  mode: 'production',

  target,

  entry,

  output: {
    path: appBase,
    publicPath: './dist/',
    filename: chunkData => {
      let dest = 'dist/';
      let { name } = chunkData.chunk;
      if (name === 'main') {
        if (target === 'node') name = 'server';
        else dest = '';
      }
      return `${dest}${name}.prod.js`;
    },
    // load later at dist so can use the current resource path
    chunkFilename: 'dist/[name].prod.js'
  },

  optimization: {
    minimizer: process.env.E2E_BUILD
      ? []
      : [
          new TerserPlugin({
            parallel: true,
            sourceMap,
            cache: true
          })
        ]
  },

  plugins,

  /**
   * Disables webpack processing of __dirname and __filename.
   * If you run the bundle in node.js it falls back to these values of node.js.
   * https://github.com/webpack/webpack/issues/2010
   */
  node: {
    __dirname: false,
    __filename: false
  }
});
